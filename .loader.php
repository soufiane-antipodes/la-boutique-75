<?php
ini_set('display_errors', 'On');

//Global PATH
define('PATH_WWW', dirname(__FILE__) . '/');
define('PATH_FRAMEWORK', PATH_WWW . 'framework/');
define('PATH_FRAMEWORK_CLASSES', PATH_FRAMEWORK.'class/');

require_once(PATH_FRAMEWORK_CLASSES . 'Globals.php');
require_once(PATH_FRAMEWORK_CLASSES . 'Autoload.php');
require_once(PATH_FRAMEWORK_CLASSES . 'Functions.php');

//include('301.php');

//GET LANG
$lang = Request :: getField('lang', 0);

Session :: initialize();
Cookie :: initialize();
Database :: verifyConnexion();

if(empty($lang)){
	$lang = 'fr';
	Session :: set('lang', $lang);
}else{
	Session :: set('lang', $lang);
}

switch($lang){
	case 'fr' : Session :: set('langToUse', 'fr'); break;
	case 'en' : Session :: set('langToUse', 'en'); break;
	case 'cn' : Session :: set('langToUse', 'cn'); break;
	default : Session :: set('langToUse', 'fr'); break;
}

$globals = array();
Database :: getSimpleArray('SELECT meta_key, value FROM '.BDD.'options WHERE lang = "'.mysql_real_escape_string(Session :: get('langToUse')).'"', $globals);


$globals = array_map('htmlspecialchars_decode', $globals);

// ::::: function called at the beginning of page displaying
function pageStart() {
	require_once(Page :: getHeader());
}

// ::::: function called at the end of page displaying
function pageEnd() {
	require_once(Page :: getFooter());
}

Trigger :: bind('onStartHTML','pageStart');
Trigger :: bind('shutdown','pageEnd');
?>