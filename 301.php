<?php

	#FR
	if(preg_match('#fiche.php\?lng=fr&id=43$#', $_SERVER['REQUEST_URI'])){
		header("Status: 301 Moved Permanently");
		header('Location: ' . PATH_HTTP . 'page/259-toles-inox');
		exit;
	}
	
	#EN
	
	if(preg_match('#fiche.php\?lng=en&id=43$#', $_SERVER['REQUEST_URI'])){
		header("Status: 301 Moved Permanently");
		header('Location: ' . PATH_HTTP . 'en/page/260-plate-stock');
		exit;
	}
		
	
	if(preg_match('#imprimer.php#', $_SERVER['REQUEST_URI'])){
		header("Status: 301 Moved Permanently");
		header('Location: ' . PATH_HTTP);
		exit;
	}
	
	if(preg_match('#^/en/2$#', $_SERVER['REQUEST_URI'])){
		header("Status: 301 Moved Permanently");
		header('Location: ' . PATH_HTTP);
		exit;
	}
	
	if(preg_match('#^/-$#', $_SERVER['REQUEST_URI'])){
		header("Status: 301 Moved Permanently");
		header('Location: ' . PATH_HTTP);
		exit;
	}
	
	if(preg_match('#^/2$#', $_SERVER['REQUEST_URI'])){
		header("Status: 301 Moved Permanently");
		header('Location: ' . PATH_HTTP);
		exit;
	}
	
	if(preg_match('#desabonnement-newsletter.php\?lng=en$#', $_SERVER['REQUEST_URI'])){
		header("Status: 301 Moved Permanently");
		header('Location: ' . PATH_HTTP);
		exit;
	}
	
	if(preg_match('#^/a$#', $_SERVER['REQUEST_URI'])){
		header("Status: 301 Moved Permanently");
		header('Location: ' . PATH_HTTP);
		exit;
	}
?>