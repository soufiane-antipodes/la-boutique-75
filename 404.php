<?php
require_once('.loader.php');

//Definit le titre de la page
Page :: setTitle(TITLE_BY_DEFAULT);

Page :: setTitle('Page introuvable | La Boutique 75');

HTML_Script :: addScript("
 $('.page404 h2').fitText(0.511, {minFontSize: '174px', maxFontSize:'374px'});
 $('.page404 h3').fitText(1.5, {minFontSize: '26px', maxFontSize:'34px'});
");

Trigger :: call('onStartHTML');
?>
<div class="actualpage full-float">
	<h1>Erreur 404</h1>
</div>
<section class="page404">
	<div class="content">
		<h2>404</h2>
		<h3>Page introuvable</h3>
	</div>
</section>	