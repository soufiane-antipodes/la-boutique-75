<?php
require_once('../.loader.php');

//GET LANG
$lang = Request :: getField('langAdmin', 0);

if(empty($lang)){
	if(!Session :: exists('langAdmin')){
		$lang = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
		$lang = $lang{0}.$lang{1};
		Session :: set('lang', $lang);
	}
	else{
		$lang = Session :: get('langAdmin');
	}
}else{
	Session :: set('langAdmin', $lang);
}

switch($lang){
	case 'fr' : Session :: set('langAdminToUse', 'fr'); break;
	case 'en' : Session :: set('langAdminToUse', 'en');break;
	case 'cn' : Session :: set('langAdminToUse', 'cn');break;

	default : Session :: set('langAdminToUse', 'fr'); break;
}

Database :: verifyConnexion();

if(basename($_SERVER['PHP_SELF']) != 'login.php'){
	if(!User :: isAdminLogged()){
		Request :: redirectTo(PATH_ADMIN.'login.php');
	}
}
?>