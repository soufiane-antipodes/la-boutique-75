<?php
require_once('.loader.php');

Database :: verifyConnexion();

$error 		= null;
$success 	= null;

$information = array('id' => 'id', 'relation' => 'id_article', 'table' => 'accueil_diaporama');

if(Request :: getAction() == 'add'){
	
	$fields = array();
	$iCount = 0;
	$iLastID = Database :: get('SELECT ' . $information['id'] . ' FROM ' . BDD . $information['table'] . ' ORDER BY ' . $information['id'] . ' DESC LIMIT 0,1');
	foreach (unserialize(EXISTING_LANGUAGES) as $iKey => $sValue) 
	{
		$fields['id_article'] = array(
								 'name' => 'ID Relation',
								 'value' => $iLastID + 1, 
								 'type' => 'number', 
								 'isMandatory' => true
								 );
		$fields['title'] = array(
								 'name' => 'Titre',
								 'value' => Request :: getField('title'), 
								 'type' => 'string', 
								 'isMandatory' => true
								 );
		$fields['lang'] = array(
								'name' => 'Langue',
								'value' => $sValue, 
								'type' => 'string', 
								'isMandatory' => true
								);

		$file = array();
		$thumbnails = array();

		if ( $iCount > 0 )
		{
			$iLastID_img = Database :: get('SELECT id_item FROM ' . BDD . 'item WHERE extension != "pdf" ORDER BY id_item DESC LIMIT 0,1');
			$iLastID_pdf = Database :: get('SELECT id_item FROM ' . BDD . 'item WHERE extension = "pdf ORDER BY id_item DESC LIMIT 0,1');
			$fields['id_item'] = array(
					'name' => 'Photo',
					'value' => ( $iLastID_img ) ? $iLastID_img : 0, 
					'type' => 'number', 
					'isMandatory' => true
					);
		}
		else
		{
			$file[0] = $_FILES['filePicture'];
			$file[0]['idName'] = 'id_item';
			$file[0]['isMandatory'] = true;
			$file[0]['dest'] = PATH_IMG_SLIDER;
			$file[0]['extension'] = array('.jpg', '.JPG', '.png','.PNG');
			
			$thumbnails = array();
		}

		$position = array('position' => 'end', 'condition' => 'lang="' . $sValue . '"');
		$result = Manage :: add($information['table'], $file, $fields, $thumbnails, $position, $error);
		
		$iCount++;
	}

	if($result == true) $success = 'Ligne ajoutée';

}

if(Request :: getAction() == 'edit'){
	
	$idValue = Request :: getInt('id');
	$fields = array();
	$fields['title'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('title'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$file = array();
	$file[0] = $_FILES['filePicture'];
	$file[0]['idName'] = 'id_item';
	$file[0]['isMandatory'] = true;
	$file[0]['dest'] = PATH_IMG_SLIDER;
	$file[0]['extension'] = array('.jpg', '.JPG', '.png','.PNG');

	$thumbnails = array();
	$id = array('name' => $information['id'], 'value' => $idValue);
	
	
	$result = Manage :: edit($information['table'], $id, $file, $fields, $thumbnails, $error);
	if($result == true) $success = 'Ligne modifiée';
}

if(Request :: getAction() == 'delete')
{	
	$idValue = Request :: getInt('id');
	$aID = array();
	$iResult = 0;
	Database :: getTable('SELECT id FROM ' . BDD . $information['table'] . ' WHERE id_article = ( SELECT id_article FROM ' . BDD . $information['table'] . ' WHERE id = ' . $idValue . ')', $aID);
	$iCount = 0;
	foreach ($aID as $iKey => $sValue) 
	{
		$id = array('name' => $information['id'], 'value' => $sValue['id']);
		$pictureInfos = array('normal' => array('dest' => PATH_IMG_SLIDER));
		$result = Manage :: delete($information['table'], $id, $pictureInfos, $error);
		if ( $iCount === 0 ) $iResult = $result;
		$iCount++;
	}
	echo (int) $iResult;	
	exit;
}

$alLines = array();
Database :: getTable(
			'SELECT S.'.$information['id'].', 
					S.' . $information['relation'] . ', 
					S.id_item, 
					S.title, 
					S.lang, 
					I.value AS img, 
					S.publish, 
					I.extension AS img_extension
				FROM '.BDD.$information['table'].' S 
						LEFT JOIN '.BDD.'item I ON I.id_item = S.id_item 
				WHERE S.lang="'.mysql_real_escape_string(Session :: get('langAdminToUse')).'" order by S.position'
				, $alLines);

HTML_Script :: addFile(PATH_HTTP_PLUGINS_COLORBOX . 'colorbox.js');
HTML_CSS 		:: addFile(PATH_HTTP_PLUGINS_COLORBOX . 'colorbox-v2.css');

HTML_Script :: addFile(PATH_HTTP_PLUGINS_CKEDITOR . 'ckeditor.js');

HTML_Script :: addFile(PATH_HTTP_JS . 'myPublish.js');
HTML_Script :: addFile(PATH_HTTP_JS . 'mySortable.js');

HTML_Script :: addScript("

	//GENERATE POPUP
	$('body').delegate('.bubble', 'click', function(){
		$(this).colorbox({width : '50%',
		
			onClosed : function(){
				location.reload();
			}});
	});
	
	//DELETE BUTTONS
	$('.delete').click(function(){
	
		var idName = $(this).attr('id');
		var idName = idName.replace('delete-', '');
	
	if(confirm('Are you sure to delete this element ?')){
		$.ajax({
				   type: 'GET',
				   url: '',
				   data: 'action=delete&id='+idName,
				   success: function(msg){
				   	//console.log(msg);
				   	if(msg == 1){
				   		$('#myEntry-'+idName).remove();
				   	}
				   	
				   }
				 }); 
	}
	});
	
	$('.myTable tbody').myPublish({table : '" . $information['table'] . "', nameId : '" . $information['relation'] . "'});
	$('.myTable tbody').mySortable({table : '" . $information['table'] . "', nameId : '" . $information['id'] . "'});
	
");

//Definit le titre de la page
Page :: setTitle(TITLE_BY_DEFAULT_ADMIN);
Page :: setAdmin(true);
Page :: setIndexMenu(1);
Trigger :: call('onStartHTML');
?>

<div id="container">
	<div id="headerContainer">Accueil - Diaporama</div><!-- .header -->
	<div id="contentContainer">

<?= ($error !== null) ? '<div class="error">'.$error.'</div>' : null; ?>
<?= ($success !== null) ? '<div class="success">'.$success.'</div>' : null; ?>

<div class="action" style="margin-bottom : 10px;">
	<a href="<?=str_replace( '.php', '', $_SERVER['PHP_SELF'])?>-html.php?action=add" class="bubble">
		<input type="button" class="button" name="add" value="AJOUTER UNE LIGNE" />
	</a>
</div><!-- .action -->

<?php
	if(count($alLines) > 0){
		?>
			<table class="myTable">
				<tr>
					<th></th>
					<th>Photo</th>
					<th>Titre</th>
					<th>Publier</th>
					<th>Editer</th>
					<th>Supprimer</th>
				</tr>
				
				<? foreach($alLines as $line) : ?>
				<tr id="myEntry-<?=$line[$information['id']]?>" class="entry">
					<td class="move">
						<img src="<?=PATH_ADMIN_IMG_ICONS?>move.png" alt="Move" width="16" height="16" />
					</td>
					<td>
						<img src="<?=PATH_HTTP_IMG_SLIDER . Sanitize :: keepValidChars($line['img']) . '-' . $line['id_item'] . '.' . $line['img_extension']?>" alt="" style="width:auto;" height="100" />
					</td>
					<td><?=$line['title']?></td>
					<td style="width:50px;">
						<input type="checkbox" name="publish" class="publish" id="publish-<?= $line[$information['id']] ?>" rel="publish-<?= $line[$information['relation']] ?>" value="1" <?=($line['publish'] == 1) ? 'checked="checked"':null;?>/>
					</td>
					<td style="width:50px;">
						<a href="<?=str_replace( '.php', '', $_SERVER['PHP_SELF'])?>-html.php?action=edit&id=<?=$line[$information['id']]?>" class="bubble">
							<img src="<?=PATH_ADMIN_IMG_ICONS?>edit.png" alt="Edit" title="Edit" width="16" height="16" />
						</a>
					</td>
					<td style="width:50px;">
						<img class="delete" id="delete-<?=$line[$information['id']]?>" src="<?=PATH_ADMIN_IMG_ICONS?>error.png" alt="Delete" title="Delete" width="16" height="16" />
					</td>
				</tr>
				
				<? endforeach; ?>
				
			</table>
		<?php
	}
?>


	</div><!-- .contentContainer -->
</div><!-- #container -->