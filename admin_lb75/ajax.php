<?php
require_once('.loader.php');

Database :: verifyConnexion();

if(Request :: getAction() == 'updatePosition'){
	
	$value = Request :: getField('value');
	$table = explode('*|*', $value);
	$tableName = array_shift($table);
	$idName = array_shift($table);
	
	if(count($table) > 0){
		foreach($table as $cpt => $t){
			
			if($tableName == 'pages'){
				$t = Database :: get('select id_article from '.BDD.'pages where id="'.mysql_real_escape_string($t).'"');
				$idName = 'id_article';
			}
			
			if($tableName == 'questionnaire'){
				$t = Database :: get('select id_article from '.BDD.'questionnaire where id="'.mysql_real_escape_string($t).'"');
				$idName = 'id_article';
			}
			
			if($tableName == 'questionnaire_subjects'){
				$t = Database :: get('select id_article from '.BDD.'questionnaire_subjects where id="'.mysql_real_escape_string($t).'"');
				$idName = 'id_article';
			}
			
			if($tableName == 'actualites'){
				$t = Database :: get('select id_article from '.BDD.'actualites where id="'.mysql_real_escape_string($t).'"');
				$idName = 'id_article';
			}
			
			if($tableName == 'options'){
				$t = Database :: get('select id_article from '.BDD.'options where id_option="'.mysql_real_escape_string($t).'"');
				$idName = 'id_article';
			}
			
			$result = Database :: query('update '.BDD.$tableName.' set position="'.mysql_real_escape_string(($cpt+1)).'" where '.$idName.'="'.mysql_real_escape_string(trim($t)).'"');
		}
	}
	
	echo $result;
	exit;
}

if(Request :: getAction() == 'publish'){
	
	$tableName = Request :: getField('tableName');
	$idName = Request :: getField('idName');
	$id = Request :: getInt('id');
	$status = Request :: getInt('status');

	$result = Database :: query('update '.BDD.$tableName.' set publish="'.$status.'" where '.$idName.'="'.mysql_real_escape_string($id).'"');
	echo $result;
	exit;
}

if(Request :: getAction() == 'isClosed'){
	
	$tableName = Request :: getField('tableName');
	$idName = Request :: getField('idName');
	$id = Request :: getInt('id');
	$status = Request :: getInt('status');

	$result = Database :: query('update '.BDD.$tableName.' set isClosed="'.$status.'" where '.$idName.'="'.mysql_real_escape_string($id).'"');
	echo $result;
	exit;
}

if(Request :: getAction() == 'isRead'){
	
	$id = Request :: getField('id');
	$result = Database :: query('update '.BDD.'t_inbox_conversation IC inner join '.BDD.'t_users U on IC.idUser=U.idUser set isRead="1" where idInbox="'.mysql_real_escape_string($id).'" and U.isAdmin=0');
	
	echo $result;
	exit;
}

if(Request :: getAction() == 'publishHomePage'){
	
	$tableName = Request :: getField('tableName');
	$idName = Request :: getField('idName');
	$id = Request :: getInt('id');
	$status = Request :: getInt('status');

	$result = Database :: query('update '.BDD.$tableName.' set homePage="'.$status.'" where '.$idName.'="'.mysql_real_escape_string($id).'"');
	echo $result;
	exit;
}

if(Request :: getAction() == 'partner'){
	
	$tableName = Request :: getField('tableName');
	$id = Request :: getInt('id');
	$status = Request :: getInt('status');
	$toDelete = Request :: getInt('toDelete');
	
	if($toDelete == 1){
		$result = Database :: query('delete from '.BDD.'t_actualite_by_partner where idActualite="'.mysql_real_escape_string($id).'" and idActualitePartner="'.mysql_real_escape_string($status).'"');
	}
	else{
		$result = Database :: query('insert '.BDD.'t_actualite_by_partner(idActualiteByPartner, idActualitePartner, idActualite) values("", "'.mysql_real_escape_string($status).'", "'.mysql_real_escape_string($id).'")');
	}

	
	echo $result;
	exit;
}

?>