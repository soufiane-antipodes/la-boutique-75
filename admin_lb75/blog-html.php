<?php
require_once('.loader.php');

$information = array('id' => 'id', 'table' => 'actualites');
?>

<div class="addPicture content" style="height:500px;">
	<?php  if(Request :: getAction() == 'add') : ?>
			
	<div class="info">La taille de la photo finale sera de : 1154*353 pixels. Formats autorisés : (.jpg, .png) | Max 4Mo | Max resolution 2500px</div><!-- .info -->
	
	<form action="<?=str_replace('-html', '', $_SERVER['PHP_SELF'])?>" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td><label for="title">Titre :<span class="star">*</span></label></td>
				<td><input type="text" name="title" id="title" value="" size="50" /></td>
			</tr>
			<tr>
				<td><label for="description">Description :<span class="star">*</span></label></td>
				<td><textarea name="description" id="description"></textarea></td>
			</tr>
			<tr>
				<td><label for="filePicture">Charger une miniature :<span class="star">*</span></label></td>
				<td><input type="file" id="filePicture" name="filePicture" /></td>
			</tr>
			<tr>
				<td><label for="filePictureZoom">Charger une photo Agrandie :<span class="star">*</span></label></td>
				<td><input type="file" id="filePictureZoom" name="filePictureZoom" /></td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top : 10px;">
					<input type="hidden" name="action" value="add" />
					<input type="submit" class="button positionButton" name="addCollection" value="AJOUTER UN ARTICLE" />
				</td>
			</tr>
		</table>
	</form>
	
	<?php endif; ?>
	
	
	<?php  if(Request :: getAction() == 'edit') : 
		$allLines = array();
		Database :: getLine(
				'SELECT '.$information['id'].', 
							S.id_item, 
							S.title, 
							S.description, 
							S.lang, 
							I.value AS img, 
							S.publish, 
							I.extension AS img_extension
					FROM '.BDD.$information['table'].' S 
							LEFT JOIN '.BDD.'item I ON I.id_item = S.id_item 
					WHERE S.'.$information['id'].'="'.mysql_real_escape_string(Request :: getInt('id')).'" ORDER BY S.position'
					, $allLines);

		if(!isset($allLines['id']) || $allLines['id'] != Request :: getInt('id') ){ echo 'Cet élément n\'éxiste pas.'; exit; }
		?>
		<div class="info">La taille de la photo finale sera de : 1024*313 pixels. Formats autorisés : (.jpg, .png) | Max 4Mo | Max resolution 2500px</div><!-- .info -->
		<form action="<?=str_replace('-html', '', $_SERVER['PHP_SELF'])?>" method="post" enctype="multipart/form-data">
			<table>
				<tr>
					<td><label for="title">Titre :<span class="star">*</span></label></td>
					<td><input type="text" name="title" id="title" value="<?= $allLines['title'] ?>" size="50" /></td>
				</tr>
				<tr>
					<td><label for="description">Description :<span class="star">*</span></label></td>
					<td><textarea name="description" id="description"><?= $allLines['description'] ?></textarea></td>
				</tr>
				<tr>
					<td><label for="filePicture">Charger une photo :</label></td>
					<td><input type="file" name="filePicture" id="filePicture" /></td>
				</tr>
				<tr>
					<td><label for="filePictureZoom">Charger une photo Agrandie :</label></td>
					<td><input type="file" id="filePictureZoom" name="filePictureZoom" /></td>
				</tr>
				<tr>
					<td colspan="2" style="padding-top : 10px;">
						<input type="hidden" name="action" value="edit" />
						<input type="hidden" name="id" value="<?=Request :: getInt('id')?>" />
						<input type="submit" class="button positionButton" name="addCollection" value="Editer" />
					</td>
				</tr>
			</table>
		</form>
	<?php endif ?>
</div>
<script>
	var instance = CKEDITOR.instances['description'];
	if(instance)
	{
	  CKEDITOR.remove(instance);
	}
	CKEDITOR.replace('description', {
		toolbar : 'FullOption',
		height : '150px'
	});
</script>