<?php
require_once('.loader.php');

Database :: verifyConnexion();

$error 		= null;
$success 	= null;

$information = array('id' => 'id', 'relation' => 'id_article', 'table' => 'actualites');

if(Request :: getAction() == 'add'){
	
	$fields = array();
	$iCount = 0;
	$iLastID = Database :: get('SELECT ' . $information['id'] . ' FROM ' . BDD . $information['table'] . ' ORDER BY ' . $information['id'] . ' DESC LIMIT 0,1');
	foreach (unserialize(EXISTING_LANGUAGES) as $iKey => $sValue) 
	{
		$fields['id_article'] = array(
								 'name' => 'ID Relation',
								 'value' => $iLastID + 1, 
								 'type' => 'number', 
								 'isMandatory' => true
								 );
		$fields['title'] = array(
								 'name' => 'Titre',
								 'value' => Request :: getField('title'), 
								 'type' => 'string', 
								 'isMandatory' => true
								 );
		$fields['description'] = array( 
								'name' => 'Description',
								'value' => Request :: getField('description'), 
								'type' => 'string', 
								'isMandatory' => true
								);
		$fields['date_creation'] = array(
								'name' => 'Date de creation',
								'value' => date('Y-m-d H:i:s'), 
								'type' => 'date-sql', 
								'isMandatory' => true
								);
		$fields['lang'] = array(
								'name' => 'Langue',
								'value' => $sValue, 
								'type' => 'string', 
								'isMandatory' => true
								);

		$file = array();
		$thumbnails = array();

		if ( $iCount > 0 )
		{
			$lastImg = array();
			Database :: getLine('SELECT id_item, id_item_zoom FROM ' . BDD . $information['table'] . ' order by id DESC LIMIT 0,1', $lastImg);

			$fields['id_item'] = array(
					'name' => 'Photo',
					'value' => ( $lastImg['id_item'] ) ? $lastImg['id_item'] : 0, 
					'type' => 'number', 
					'isMandatory' => true
					);
					
			$fields['id_item_zoom'] = array(
					'name' => 'Photo',
					'value' => ( $lastImg['id_item_zoom'] ) ? $lastImg['id_item_zoom'] : 0, 
					'type' => 'number', 
					'isMandatory' => true
					);
		}
		else
		{
			$file = array();
			$file[0] = $_FILES['filePicture'];
			$file[0]['idName'] = 'id_item';
			$file[0]['isMandatory'] = true;
			$file[0]['dest'] = PATH_IMG_ARTICLES;
			$file[0]['extension'] = array('.jpg', '.JPG', '.png','.PNG');
			
			$file[1] = $_FILES['filePictureZoom'];
			$file[1]['idName'] = 'id_item_zoom';
			$file[1]['isMandatory'] = true;
			$file[1]['dest'] = PATH_IMG_ARTICLES;
			$file[1]['extension'] = array('.jpg', '.JPG', '.png','.PNG');

			$thumbnails = array();
		}

		$position = array('position' => 'first', 'condition' => 'lang="' . $sValue . '"');
		
		if($error === null)
			$result = Manage :: add($information['table'], $file, $fields, $thumbnails, $position, $error);
		$iCount++;
	}

	if($result == true) $success = 'Image ajoutée';
}

if(Request :: getAction() == 'edit'){
	
	$idValue = Request :: getInt('id');
	$fields = array();
	$fields['title'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('title'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$fields['description'] = array( 
							'name' => 'Description',
							'value' => Request :: getField('description'), 
							'type' => 'string', 
							'isMandatory' => true
							);
	$fields['lang'] = array(
							'name' => 'Langue',
							'value' => Session :: get('langAdminToUse'), 
							'type' => 'string', 
							'isMandatory' => true
							);
							
	$file = array();
	$file[0] = $_FILES['filePicture'];
	$file[0]['idName'] = 'id_item';
	$file[0]['isMandatory'] = false;
	$file[0]['dest'] = PATH_IMG_ARTICLES;
	$file[0]['extension'] = array('.jpg', '.JPG', '.png','.PNG');
	
	$file[1] = $_FILES['filePictureZoom'];
	$file[1]['idName'] = 'id_item_zoom';
	$file[1]['isMandatory'] = false;
	$file[1]['dest'] = PATH_IMG_ARTICLES;
	$file[1]['extension'] = array('.jpg', '.JPG', '.png','.PNG');

	$thumbnails = array();
	$id = array('name' => $information['id'], 'value' => $idValue);
	
	$result = Manage :: edit($information['table'], $id, $file, $fields, $thumbnails, $error);
	if($result == true) $success = 'Image modifiée';
}

if(Request :: getAction() == 'delete'){
	$idValue = Request :: getInt('id');
	$aID = array();
	$iResult = 0;
	Database :: getTable('SELECT id FROM ' . BDD . $information['table'] . ' WHERE id_article = ( SELECT id_article FROM ' . BDD . $information['table'] . ' WHERE id = ' . $idValue . ')', $aID);
	$iCount = 0;
	foreach ($aID as $iKey => $sValue) 
	{
		$id = array('name' => $information['id'], 'value' => $sValue['id']);
		$pictureInfos = array();
		$result = Manage :: delete($information['table'], $id, $pictureInfos, $error);
		if ( $iCount === 0 ) $iResult = $result;
		$iCount++;
	}
	echo (int) $iResult;	
	exit;
}

$alLines = array();
Database :: getTable(
			'SELECT S.'.$information['id'].', S.' . $information['relation'] . ', 
						S.id_item, 
						S.title, 
						S.description, 
						S.lang, 
						I.value AS img, 
						S.publish, 
						I.extension AS img_extension,
						S.id_item_zoom,
						IZ.value as IZvalue,
						IZ.extension as IZextension
				FROM '.BDD.$information['table'].' S 
						LEFT JOIN '.BDD.'item I ON I.id_item = S.id_item 
						LEFT JOIN '.BDD.'item IZ ON IZ.id_item = S.id_item_zoom 
				WHERE S.lang="'.mysql_real_escape_string(Session :: get('langAdminToUse')).'" ORDER BY S.position'
				, $alLines);

HTML_Script :: addFile(PATH_HTTP_PLUGINS_COLORBOX . 'colorbox.js');
HTML_CSS 		:: addFile(PATH_HTTP_PLUGINS_COLORBOX . 'colorbox-v2.css');

HTML_Script :: addFile(PATH_HTTP_JS . 'myPublish.js');
HTML_Script :: addFile(PATH_HTTP_JS . 'mySortable.js');
HTML_Script :: addFile(PATH_HTTP_PLUGINS_CKEDITOR . 'ckeditor.js');

HTML_Script :: addScript("

	//GENERATE POPUP
	$('body').delegate('.bubble', 'click', function(){
		$(this).colorbox({width : '50%',
		
			onClosed : function(){
				location.reload();
			}});
	});
	
	//DELETE BUTTONS
	$('.delete').click(function(){
	
		var idName = $(this).attr('id');
		var idName = idName.replace('delete-', '');
	
	if(confirm('Are you sure to delete this element ?')){
		$.ajax({
				   type: 'GET',
				   url: '',
				   data: 'action=delete&id='+idName,
				   success: function(msg){
				   	//console.log(msg);
				   	if(msg == 1){
				   		$('#myEntry-'+idName).remove();
				   	}
				   	
				   }
				 }); 
	}
	});
	
	$('.myTable tbody').myPublish({table : '" . $information['table'] . "', nameId : '" . $information['relation'] . "'});
	$('.myTable tbody').mySortable({table : '".$information['table']."', nameId : '".$information['id']."'});
	
");

//Definit le titre de la page
Page :: setTitle(TITLE_BY_DEFAULT_ADMIN);
Page :: setAdmin(true);
Page :: setIndexMenu(2);
Trigger :: call('onStartHTML');
?>

<div id="container">
	<div id="headerContainer">Blog - Contenu</div><!-- .header -->
	<div id="contentContainer">

<?= ($error !== null) ? '<div class="error">'.$error.'</div>' : null; ?>
<?= ($success !== null) ? '<div class="success">'.$success.'</div>' : null; ?>

<div class="action" style="margin-bottom : 10px;">
	<a href="<?=str_replace( '.php', '', $_SERVER['PHP_SELF'])?>-html.php?action=add" class="bubble">
		<input type="button" class="button" name="add" value="AJOUTER UN ARTICLE" />
	</a>
</div><!-- .action -->

<?php if(count($alLines) > 0) : ?>
	<table class="myTable">
		<tr>
			<th></th>
			<th>Miniature</th>
			<th>Photo agrandie</th>
			<th>Titre</th>
			<th>Description</th>
			<th>Publier</th>
			<th>Editer</th>
			<th>Supprimer</th>
		</tr>
		
		<? foreach($alLines as $line) : ?>
			<tr id="myEntry-<?=$line[$information['id']]?>" class="entry">
				<td class="move">
					<img src="<?=PATH_ADMIN_IMG_ICONS?>move.png" alt="Move" width="16" height="16" />
				</td>
				<td>
					<img src="<?=PATH_HTTP . 'timthumb.php?src='.PATH_HTTP_IMG_ARTICLES . Sanitize :: keepValidChars($line['img']) . '-' . $line['id_item'] . '.' . $line['img_extension']?>&w=100&h=100&zc=2" alt="" />
				</td>
				<td>
					<img src="<?=PATH_HTTP . 'timthumb.php?src='.PATH_HTTP_IMG_ARTICLES . Sanitize :: keepValidChars($line['IZvalue']) . '-' . $line['id_item_zoom'] . '.' . $line['IZextension']?>&w=327&h=100&zc=2" alt="" />
				</td>
				<td><?=$line['title']?></td>
				<td><?=Sanitize :: html_excerpt(htmlspecialchars_decode($line['description']), 150);?></td>
				<td style="width:50px;">
					<input type="checkbox" name="publish" class="publish" id="publish-<?=$line[$information['id']]?>" rel="publish-<?= $line[$information['relation']] ?>" value="1" <?=($line['publish'] == 1) ? 'checked="checked"':null;?>/>
				</td>
				<td style="width:50px;">
					<a href="<?=str_replace( '.php', '', $_SERVER['PHP_SELF'])?>-html.php?action=edit&id=<?=$line[$information['id']]?>" class="bubble">
						<img src="<?=PATH_ADMIN_IMG_ICONS?>edit.png" alt="Edit" title="Edit" width="16" height="16" />
					</a>
				</td>
				<td style="width:50px;">
					<img class="delete" id="delete-<?=$line[$information['id']]?>" src="<?=PATH_ADMIN_IMG_ICONS?>error.png" alt="Delete" title="Delete" width="16" height="16" />
				</td>
			</tr>
		<? endforeach; ?>
	</table>
<?php endif ?>


	</div><!-- .contentContainer -->
</div><!-- #container -->