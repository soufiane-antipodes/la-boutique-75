<?php
require_once('.loader.php');

$information = array('id' => 'id', 'relation' => 'id_article', 'table' => 'contact_diaporama');
?>

<div class="addPicture content" style="height : 130px;">
	
	
	
	<!-- ADD -->
	
	
	
	<?php  if(Request :: getAction() == 'add') : ?>
			
	<form action="<?=str_replace('-html', '', $_SERVER['PHP_SELF'])?>" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td><label for="title">Titre :<span class="star">*</span></label></td>
				<td><input type="text" name="title" id="title" value="" size="50" /></td>
			</tr>
			<tr>
				<td><label for="filePicture">Charger une photo :<span class="star">*</span></label></td>
				<td><input type="file" id="filePicture" name="filePicture" /></td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top : 10px;">
					<input type="hidden" name="action" value="add" />
					<input type="submit" class="button positionButton" name="addCollection" value="Ajouter une ligne" />
				</td>
			</tr>
		</table>
	</form>
	
	<?php endif; ?>
	
	
	
	<!-- EDIT -->
	
	
	
	<?php  if(Request :: getAction() == 'edit') : 
	
	$allLines = array();
	Database :: getLine(
				'SELECT *
				FROM '.BDD.$information['table'].' S 
				WHERE S.'.$information['id'].'="'.mysql_real_escape_string(Request :: getInt('id')).'"'
				, $allLines);

	if(!isset($allLines['id']) || $allLines['id'] != Request :: getInt('id') ){ echo 'Cet élément n\'éxiste pas.'; exit; }
	
	?>
			
	<form action="<?=str_replace('-html', '', $_SERVER['PHP_SELF'])?>" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td><label for="title">Titre :<span class="star">*</span></label></td>
				<td><input type="text" name="title" id="title" value="<?=$allLines['title']?>" size="50" /></td>
			</tr>
			<tr>
				<td><label for="filePicture">Charger une photo :</label></td>
				<td><input type="file" name="filePicture" id="filePicture" /></td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top : 10px;">
					<input type="hidden" name="action" value="edit" />
					<input type="hidden" name="id" value="<?=Request :: getInt('id')?>" />
					<input type="submit" class="button positionButton" name="addCollection" value="Editer" />
				</td>
			</tr>
		</table>
	</form>
	
	<?php endif; ?>
	
	
	
</div><!-- .content -->