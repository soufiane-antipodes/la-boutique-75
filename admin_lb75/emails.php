<?php
require_once('.loader.php');

Database :: verifyConnexion();

$error 		= null;
$success 	= null;

$information = array('id' => 'id', 'table' => 'newsletter');


if(Request :: getAction() == 'delete'){
	
	$idValue = Request :: getInt('id');
	$id = array('name' => $information['id'], 'value' => $idValue);
	$result = Manage :: delete($information['table'], $id, array(), $error);
	echo (int) $result;
	exit;
}

$alLines = array();
Database :: getTable('SELECT * FROM '.BDD.$information['table'].' ORDER BY date_creation DESC', $alLines);

HTML_Script :: addFile(PATH_HTTP_PLUGINS_COLORBOX . 'colorbox.js');
HTML_CSS 		:: addFile(PATH_HTTP_PLUGINS_COLORBOX . 'colorbox-v2.css');

HTML_Script :: addFile(PATH_HTTP_JS . 'myPublish.js');
HTML_Script :: addFile(PATH_HTTP_JS . 'mySortable.js');
HTML_Script :: addFile(PATH_HTTP_PLUGINS_CKEDITOR . 'ckeditor.js');

HTML_Script :: addScript("
	$('.delete').click(function()
	{
		var idName = $(this).attr('id');
		var idName = idName.replace('delete-', '');
		if(confirm('Are you sure to delete this element ?'))
		{
			$.ajax(
			{
				type: 'GET',
				url: '',
				data: 'action=delete&id='+idName,
				success: function(msg)
				{
					console.log(msg);
					if(msg == 1){ $('#myEntry-'+idName).remove(); }
				}
			}); 
		}
	});
");

//Definit le titre de la page
Page :: setTitle(TITLE_BY_DEFAULT_ADMIN);
Page :: setAdmin(true);
Page :: setIndexMenu(6);
Trigger :: call('onStartHTML');
?>

<div id="container">
	<div id="headerContainer">Newsletter - Visualisation des emails</div>
	<div id="contentContainer">

<?= ($error !== null) ? '<div class="error">'.$error.'</div>' : null; ?>
<?= ($success !== null) ? '<div class="success">'.$success.'</div>' : null; ?>

<?php if(count($alLines) > 0) : ?>
	<table class="myTable">
		<tr>
			<th>Email</th>
			<th>Date inscription</th>
			<th>Supprimer</th>
		</tr>
		
		<? foreach($alLines as $line) : ?>
			<tr id="myEntry-<?=$line[$information['id']]?>" class="entry">
				<td><?= $line['email'] ?></td>
				<td><?= date('d-m-Y H:i', strtotime($line['date_creation'])) ?></td>
				<td style="width:50px;">
					<img class="delete" id="delete-<?=$line[$information['id']]?>" src="<?=PATH_ADMIN_IMG_ICONS?>error.png" alt="Delete" title="Delete" width="16" height="16" />
				</td>
			</tr>
		<? endforeach; ?>
	</table>
<?php endif ?>


	</div>
</div>