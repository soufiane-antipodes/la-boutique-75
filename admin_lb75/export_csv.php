<?php 
	require_once('.loader.php');

	$information = array('id' => 'id', 'table' => 'newsletter');

	$aDonnees = array();
	Database :: getTable('SELECT email, date_creation FROM '.BDD.$information['table'].' ORDER BY date_creation DESC', $aDonnees);


	$filename = "export__" . date('d_m_Y', time());

	header("Content-type: text/csv");
	header("Content-Disposition: attachment; filename={$filename}.csv");
	header("Pragma: no-cache");
	header("Expires: 0");

	outputCSV($aDonnees, array('email', 'date_creation'));

?>