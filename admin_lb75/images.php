<?php
require_once('.loader.php');

Database :: verifyConnexion();

$error 		= null;
$success 	= null;
$iIdPage    = Request::getInt('idParent');
$iIdMenu    = Request::getInt('idMenu');

$information = array('id' => 'id_image', 'table' => 'images');

if(Request :: getAction() == 'add'){
	
	$fields = array();
	$fields = array();
	$iCount = 0;
	$iLastID = Database :: get('SELECT MAX(' . $information['id'] . ') FROM ' . BDD . $information['table']);
	foreach (unserialize(EXISTING_LANGUAGES) as $iKey => $sValue) 
	{
		$fields['id_article'] = array(
								 'name' => 'ID Relation',
								 'value' => $iLastID + 1, 
								 'type' => 'number', 
								 'isMandatory' => true
								 );
		$fields['title'] = array(
								 'name' => 'Titre de l\'image',
								 'value' => Request :: getField('title'), 
								 'type' => 'string', 
								 'isMandatory' => true
								 );
	    $fields['description'] = array(
	    						 'name' => 'description',
	    						 'value' => Request :: getField('description'), 
	    						 'type' => 'string', 
	    						 'isMandatory' => true
	    						 );
		$fields['id_page'] = array(
								 'name' => 'le numero de la page',
								 'value' => $iIdPage, 
								 'type' => 'string', 
								 'isMandatory' => true);
		$fields['lang'] = array(
								'name' => 'Langue',
								'value' => $sValue, 
								'type' => 'string', 
								'isMandatory' => true
								);

		$file = array();
		$thumbnails = array();

		if ( $iCount > 0 )
		{
			$iLastID_img = Database :: get('SELECT id_item FROM ' . BDD . 'item WHERE extension != "pdf" ORDER BY id_item DESC LIMIT 0,1');
			$fields['id_item'] = array(
					'name' => 'Photo',
					'value' => ( $iLastID_img ) ? $iLastID_img : 0, 
					'type' => 'number', 
					'isMandatory' => true
					);
		}
		else
		{
			$file[0] = $_FILES['filePicture'];
			$file[0]['idName'] = 'id_item';
			$file[0]['isMandatory'] = true;
			$file[0]['dest'] = PATH_IMG_SLIDER;
			$file[0]['extension'] = array('.jpg', '.JPG', '.png','.PNG');
			
			$thumbnails = array('normal' => array('width' => 1024, 'height' => 313, 'source' => PATH_IMG_SLIDER, 'dest' => PATH_IMG_SLIDER));
		}

		$position = array('position' => 'end', 'condition' => 'lang="'.Session :: get('langAdminToUse').'"');
		$result = Manage :: add($information['table'], $file, $fields, $thumbnails, $position, $error);

		$iCount++;

		if($result == true) $success = 'Ligne ajoutée';	
	}

}

if(Request :: getAction() == 'edit')
  {
	
	$idValue = Request :: getInt('id');
	$fields = array();

	$fields['title'] = array('name' => 'Titre de l\'image','value' => Request :: getField('title'), 'type' => 'string', 'isMandatory' => true);
    $fields['description'] = array('name' => 'Description','value' => Request :: getField('description'), 'type' => 'string', 'isMandatory' => true);

	$file = $_FILES['filePicture'];
	$file['isMandatory'] = false;
	$file['dest'] = PATH_IMG_SLIDER;
	$file['extension'] = array('.jpg', '.JPG', '.png');
	$thumbnails = array();

	$id = array('name' => $information['id'], 'value' => $idValue);
	
	$result = Manage :: edit($information['table'], $id, $file, $fields, $thumbnails, $error);
	if($result == true) $success = 'Ligne modifiée';
}

if(Request :: getAction() == 'delete'){
	
	$idValue = Request :: getInt('id');

	$id = array('name' => $information['id'], 'value' => $idValue);
	$pictureInfos = array();
	$result = Manage :: delete($information['table'], $id, $pictureInfos, $error);
	echo (int) $result;
	exit;
}


$alLines = array();
Database :: getTable('select f.*, I.value, I.extension
					 from '.BDD.$information['table'].' f 
					 left join '.BDD.'item I on I.id_item=f.id_item 
					 where lang="'.mysql_real_escape_string(Session :: get('langAdminToUse')).'"
					 and id_page = '.$iIdPage.'
					 order by f.position', $alLines);

HTML_Script :: addFile(PATH_HTTP_PLUGINS_COLORBOX . 'colorbox.js');
HTML_CSS 		:: addFile(PATH_HTTP_PLUGINS_COLORBOX . 'colorbox-v2.css');

HTML_Script :: addFile(PATH_HTTP_PLUGINS_CKEDITOR . 'ckeditor.js');

HTML_Script :: addFile(PATH_HTTP_JS . 'myPublish.js');
HTML_Script :: addFile(PATH_HTTP_JS . 'mySortable.js');

HTML_Script :: addScript("

	//GENERATE POPUP
	$('body').delegate('.bubble', 'click', function(){
		$(this).colorbox({width : '50%'});
	});
	
	//DELETE BUTTONS
	$('.delete').click(function(){
	
		var idName = $(this).attr('id');
		var idName = idName.replace('delete-', '');
	
	if(confirm('Are you sure to delete this element ?')){
		$.ajax({
				   type: 'GET',
				   url: '',
				   data: 'action=delete&id='+idName,
				   success: function(msg){
				   	//console.log(msg);
				   	if(msg == 1){
				   		$('#myEntry-'+idName).remove();
				   	}
				   	
				   }
				 }); 
	}
	});
	
	$('.myTable tbody').myPublish({table : '".$information['table']."', nameId : '".$information['id']."'});
	$('.myTable tbody').mySortable({table : '".$information['table']."', nameId : '".$information['id']."'});
	
	$('.lightbox').colorbox({rel:'image-link',
		photo:true
	});
");

//Definit le titre de la page
Page :: setTitle(TITLE_BY_DEFAULT_ADMIN);
Page :: setAdmin(true);
// Selectionner le menu 
Page :: setIndexMenu( $iIdMenu );
Trigger :: call('onStartHTML');

$sTitleDePage = 'Images';
if( $iIdMenu == 5 ){
	$sTitleDePage = 'Présentation';
}elseif( $iIdMenu == 1 ){
	$sTitleDePage = 'Pub';
}

?>

<div id="container">
	<div id="headerContainer"><?=$sTitleDePage?></div><!-- .header -->
	<div id="contentContainer">

<?= ($error !== null) ? '<div class="error">'.$error.'</div>' : null; ?>
<?= ($success !== null) ? '<div class="success">'.$success.'</div>' : null; ?>

<?php if(User :: isSuperAdmin()) : ?>
<div class="action" style="margin-bottom : 10px;">
	<a href="images-html.php?action=add&idParent=<?=$iIdPage?>&idMenu=<?=$iIdMenu?>" class="bubble">
		<input type="button" class="button" name="add" value="AJOUTER UNE LIGNE" />
	</a>
</div><!-- .action -->
<?php endif; ?>

<?php
	if(count($alLines) > 0){
		?>
		<table class="myTable">
		<tr>
			<?php if(User :: isSuperAdmin()) : ?>
			<th></th>
			<th>ID de la photo</th>
	          <?php endif; ?>
			<th>Photo</th>
			<th>Titre</th>
			<th>Description</th>
			<th>Editer</th>
			<?php if(User :: isSuperAdmin()) : ?><th>Supprimer</th><?php endif; ?>
		  </tr>
		
		<?php foreach($alLines as $line) : ?>
		
		<tr id="myEntry-<?=$line['id_article']?>" class="entry">
		<?php if(User :: isSuperAdmin()) : ?>
			<td class="move">
				<img src="<?=PATH_ADMIN_IMG_ICONS?>move.png" alt="Move" width="16" height="16" />
			</td>
			<td>#<?=$line['id_article']?></td>
				<?php endif; ?>
			<td>
				<a class="lightbox" href="<?= PATH_HTTP_IMG_SLIDER . Sanitize :: keepValidChars($line['value']) . '-' . $line['id_item'] . '.' . $line['extension']?>" class="image-link">
					<img src="<?=PATH_HTTP?>timthumb.php?src=<?= PATH_HTTP_IMG_SLIDER . Sanitize :: keepValidChars($line['value']) . '-' . $line['id_item'] . '.' . $line['extension']?>&amp;w=125&amp;h=50&amp;zc=1" />
				</a>
			</td>
			<td><?=$line['title']?></td>
			<td><?=Sanitize :: html_excerpt(htmlspecialchars_decode($line['description']), 150)?></td>
			<td style="width:50px;"><a href="<?=str_replace( '.php', '', $_SERVER['PHP_SELF'])?>-html.php?action=edit&id=<?=$line[$information['id']]?>&idParent=<?=$iIdPage?>&idMenu=<?=$iIdMenu?>" class="bubble"><img src="<?=PATH_ADMIN_IMG_ICONS?>edit.png" alt="Edit" title="Edit" width="16" height="16" /></td>
			<?php if(User :: isSuperAdmin()) : ?><td style="width:50px;"><img class="delete" id="delete-<?=$line[$information['id']]?>" src="<?=PATH_ADMIN_IMG_ICONS?>error.png" alt="Delete" title="Delete" width="16" height="16" /></td><?php endif; ?>
		</tr>
		<?php endforeach; ?>
		</table>
		<?php
	}
?>


	</div><!-- .contentContainer -->
</div><!-- #container -->