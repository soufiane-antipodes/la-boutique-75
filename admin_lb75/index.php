<?php
require_once('.loader.php');

HTML_CSS :: addFile(PATH_ADMIN_CSS . 'promotion.css');

//Definit le titre de la page
Page :: setTitle(TITLE_BY_DEFAULT_ADMIN);

//Index dans le menu
Page :: setAdmin(true);
Trigger :: call('onStartHTML');
?>

<div id="container">
	<div id="headerContainer">Bienvenue</div><!-- .header -->
	<div id="contentContainer">
		
		<p>Bienvenue sur votre espace d'administration</p>
	
	</div><!-- .contentContainer -->
</div><!-- #container -->