
<!DOCTYPE html>
<html lang="en">
<head>
	<? Trigger::call('onBeforeParseHeader');?>
  <meta charset="utf-8" />
  <title><?=Page::getTitle();?></title>
	<meta name="author" content="agence2web.com">
	<meta name="robots" content="none">
  <link rel="icon" href="<?=PATH_HTTP_IMG_FAVICON?>favicon.ico" />

  <link rel="stylesheet" href="<?=PATH_ADMIN_CSS?>globals.css" />
  <link rel="stylesheet" href="<?=PATH_ADMIN_CSS?>style.css" />

  <script src="<?=PATH_HTTP_PLUGINS_JQUERY?>jquery-1.6.2.min.js" type="text/javascript"></script>
  <script src="<?=PATH_HTTP_PLUGINS_JQUERY?>jquery-ui-1.8.18.custom.min.js" type="text/javascript"></script>

	<? Trigger::call('onAfterParseHeader');?>

</head>
<body>

	<!-- HEADER -->
	<div id="header">
		<div class="logo">
			<a href="<?=PATH_ADMIN?>"><img src="<?=PATH_HTTP_IMG_GLOBAL?>logo_principal.png" alt="Logo" /></a>
		</div><!-- .logo -->

		<div id="language">
				<ul>
					<?=(Session :: get('langAdminToUse') == 'fr') ? '<li class="current">Français</li>' : '<li><a href="' . buildLangURL('fr', true) . '">Français</a></li>'?>
					<li>/</li>
					<?=(Session :: get('langAdminToUse') == 'en') ? '<li class="current">English</li>' : '<li><a href="' . buildLangURL('en', true) . '">English</a></li>'?>
					<li>/</li>
					<?=(Session :: get('langAdminToUse') == 'cn') ? '<li class="current">Chinois</li>' : '<li><a href="' . buildLangURL('cn', true) . '">Chinois</a></li>'?>
				</ul>
			</div>

		<div class="logout">
			<a href="<?=PATH_ADMIN?>logout.php">Déconnexion</a>
		</div><!-- .deconnexion -->

		<div class="clear"></div><!-- .clear -->
	</div><!-- #header -->

	<!-- MENU -->
	<div id="aside">
		<?php include('menu.php'); ?>
	</div>

	<div id="content">