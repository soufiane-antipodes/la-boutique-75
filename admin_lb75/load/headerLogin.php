<!DOCTYPE html> 
<html lang="en">
<head>
	<? Trigger::call('onBeforeParseHeader');?>
  <meta charset="utf-8" />
  <title><?=Page::getTitle();?></title>
  <link rel="icon" href="img/favicon/favicon.ico" />
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN_CSS?>globals.css" />
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN_CSS?>style.css" />
  <link rel="stylesheet" type="text/css" href="<?=PATH_ADMIN_CSS?>login.css" />
	<script type="text/javascript" src="<?=PATH_HTTP_PLUGINS_JQUERY?>jquery-1.6.2.min.js"></script>
	<? Trigger::call('onAfterParseHeader');?>
</head>
<body>
	
