<?php
	HTML_Script :: addScript("
	
		$('.withLink .separator').click(function(){
		
				var underMenu = $(this).parent().next();
				
				
				if(underMenu.is(':hidden')){
					$(this).css('background', 'url(\'img/icons/down.png\') no-repeat 55% 50%');
					underMenu.slideDown();
					
				}
				else{
					$(this).css('background', 'url(\'img/icons/up.png\') no-repeat 55% 50%');
					underMenu.slideUp();
					
				}
			
		});
		
		$('.noLink').click(function(){
			
			var underMenu = $(this).next();
			
			
			if(underMenu.is(':hidden')){
				$(this).find('.separator').css('background', 'url(\'img/icons/down.png\') no-repeat 55% 50%');
				underMenu.slideDown();
				
			}
			else{
				$(this).find('.separator').css('background', 'url(\'img/icons/up.png\') no-repeat 55% 50%');
				underMenu.slideUp();
				
			}
			
		});
	
	");

	$aPresses = array();
	Database :: getTable(
				'SELECT S.*
				FROM '.BDD.'presses S 
				WHERE S.lang="'.mysql_real_escape_string(Session :: get('langAdminToUse')).'" 
				order by S.position'
				, $aPresses);
?>
<ul class="mainMenu">
	
	<li class="mainItem noLink firstItemMainMenu">Accueil<div class="separator" <?= (Page :: getIndexMenu() == 1)? 'style="background : url(\'img/icons/down.png\') no-repeat 55% 50%;"' :'';?>></div></li>
	<li class="underMenu"  <?= (Page :: getIndexMenu() == 1)? '' :'style="display:none;"';?>>
		<ul>
			<li><a href="<?=PATH_ADMIN?>accueil-diaporama.php">Diaporama</a></li>
			<li><a href="options.php?idParent=2">Accroche</a></li>
			<li><a href="options.php?idParent=3">Sélection</a></li>
			<li><a href="images.php?idMenu=1&idParent=4">Pub</a></li>
			<li><a href="options.php?idParent=5">Site aéro brut</a></li>
			<li><a href="options.php?idParent=6">Référencement</a></li>
		</ul>
	</li>

	<li class="mainItem noLink firstItemMainMenu">Collection<div class="separator" <?= (Page :: getIndexMenu() == 2)? 'style="background : url(\'img/icons/down.png\') no-repeat 55% 50%;"' :'';?>></div></li>
	<li class="underMenu"  <?= (Page :: getIndexMenu() == 2)? '' :'style="display:none;"';?>>
		<ul>
			<li><a href="<?=PATH_ADMIN?>collection.php">Diaporama</a></li>
		</ul>
	</li>

	<li class="mainItem noLink firstItemMainMenu">A propos<div class="separator" <?= (Page :: getIndexMenu() == 5)? 'style="background : url(\'img/icons/down.png\') no-repeat 55% 50%;"' :'';?>></div></li>
	<li class="underMenu"  <?= (Page :: getIndexMenu() == 5)? '' :'style="display:none;"';?>>
		<ul>
			<li><a href="<?=PATH_ADMIN?>images.php?idMenu=5&idParent=1">Présentation</a></li>
			<li><a href="<?=PATH_ADMIN?>temoignages.php">Témoignages</a></li>
			<li><a href="<?=PATH_ADMIN?>options.php?idParent=54">Textes</a></li>
			<li><a href="<?=PATH_ADMIN?>options.php?idParent=55">Référencement</a></li>
		</ul>
	</li>

	<li class="mainItem noLink firstItemMainMenu">Presse<div class="separator" <?= (Page :: getIndexMenu() == 6)? 'style="background : url(\'img/icons/down.png\') no-repeat 55% 50%;"' :'';?>></div></li>

	<li class="underMenu"  <?= (Page :: getIndexMenu() == 6)? '' :'style="display:none;"';?>>
		<ul>
			<li>
				<a href="<?=PATH_ADMIN?>presses.php?idParent=1">magazine</a>
				<ul class="secondLine">
					<?php foreach ($aPresses as $aPresse) : ?>
					<?php if( $aPresse['id_parent']  ==  1 ) : ?>
					<li><a href="<?=PATH_ADMIN?>presses-parutions.php?data-id=<?=$aPresse['id']?>"><?=$aPresse['title']?></a></li>
					<?php endif; ?>
					<?php endforeach ?>
				</ul>
			</li>
			<li>
				<a href="<?=PATH_ADMIN?>presses.php?idParent=2">Web</a>
				<ul class="secondLine">
					<?php foreach ($aPresses as $aPresse) : ?>
					<?php if( $aPresse['id_parent']  ==  2 ) : ?>
					<li><a href="<?=PATH_ADMIN?>presses-parutions.php?data-id=<?=$aPresse['id']?>"><?=$aPresse['title']?></a></li>
					<?php endif; ?>
					<?php endforeach ?>
				</ul>
			</li>
			<li><a href="options.php?idParent=65">Textes</a></li>
			<li><a href="options.php?idParent=66">Référencement</a></li>
		</ul>
	</li>

	<li class="mainItem noLink">Contact<div class="separator" <?= (Page :: getIndexMenu() == 8)? 'style="background : url(\'img/icons/down.png\') no-repeat 55% 50%;"' :'';?>></div></li>
	<li class="underMenu" <?= (Page :: getIndexMenu() == 8)? '' :'style="display:none;"';?>>
		<ul>
			<li><a href="<?=PATH_ADMIN?>contact-diaporama.php">Diaporama</a></li>
			<li><a href="options.php?idParent=87">Textes</a></li>
			<li><a href="options.php?idParent=88">Référencement</a></li>
		</ul>
	</li>

	<li class="mainItem noLink">Wishlist<div class="separator" <?= (Page :: getIndexMenu() == 9)? 'style="background : url(\'img/icons/down.png\') no-repeat 55% 50%;"' :'';?>></div></li>
	<li class="underMenu" <?= (Page :: getIndexMenu() == 9)? '' :'style="display:none;"';?>>
		<ul>
			<li><a href="options.php?idParent=888">Textes</a></li>
			<li><a href="options.php?idParent=889">Référencement</a></li>
		</ul>
	</li>

	<li class="mainItem noLink">Newsletter<div class="separator" <?= (Page :: getIndexMenu() == 7)? 'style="background : url(\'img/icons/down.png\') no-repeat 55% 50%;"' :'';?>></div></li>
	<li class="underMenu" <?= (Page :: getIndexMenu() == 7)? '' :'style="display:none;"';?>>
		<ul>
			<li><a href="<?=PATH_ADMIN?>emails.php">Visualiser</a></li>
			<li><a href="<?=PATH_ADMIN?>export_csv.php">Exporter en CSV</a></li>
		</ul>
	</li>

	<li class="mainItem noLink">Imports CSV<div class="separator" <?= (Page :: getIndexMenu() == 18)? 'style="background : url(\'img/icons/down.png\') no-repeat 55% 50%;"' :'';?>></div></li>
	<li class="underMenu" <?= (Page :: getIndexMenu() == 18)? '' :'style="display:none;"';?>>
		<ul>
			<li><a href="<?=PATH_ADMIN?>inventaires.php">Inventaires</a></li>
			<li><a href="<?=PATH_ADMIN?>referentiels.php">Référentiels</a></li>
			<li><a href="<?=PATH_ADMIN?>automatisations.php">Tâches automatiques</a></li>
		</ul>
	</li>

	<!-- <li class="mainItem noLink">Textes généraux<div class="separator" <?= (Page :: getIndexMenu() == 19)? 'style="background : url(\'img/icons/down.png\') no-repeat 55% 50%;"' :'';?>></div></li>
	<li class="underMenu"  <?= (Page :: getIndexMenu() == 19)? '' :'style="display:none;"';?>>
		<ul>
			<li><a href="<?=PATH_ADMIN?>options.php?idParent=6">Entete</a></li>
			<li><a href="<?=PATH_ADMIN?>options.php?idParent=7">Pied de page</a></li>
			<li><a href="<?=PATH_ADMIN?>options.php?idParent=8">Barre latérale</a></li>
			<li><a href="<?=PATH_ADMIN?>options.php?idParent=9">Blog</a></li>
			<li><a href="<?=PATH_ADMIN?>options.php?idParent=12">Mentions légales</a></li>
			<li><a href="<?=PATH_ADMIN?>options.php?idParent=13">Contact</a></li>
			<li><a href="<?=PATH_ADMIN?>options.php?idParent=14">Recherche</a></li>
			<li><a href="<?=PATH_ADMIN?>options.php?idParent=1">Général</a></li>
			<li><a href="<?=PATH_ADMIN?>options.php?idParent=15">Produits</a></li>
		</ul>
	</li> -->

	<li class="mainItem withLink"><a href="<?=PATH_ADMIN?>options.php?idParent=98">Haut de page</a></li>
	<li class="mainItem withLink"><a href="<?=PATH_ADMIN?>options.php?idParent=99">Pied de page</a></li>
	<li class="mainItem lastItemMainMenu withLink"><a href="<?=PATH_HTTP?>" target="_blank">Revenir sur le site</a></li>
</ul>
