<?php
require_once('.loader.php');

Database :: verifyConnexion();

if(User :: isAdminLogged()){
	Request :: redirectTo('index.php');
}

$error = null;

//Connexion
if(Request :: getAction() == 'connexion'){
	
	$email = Request :: getField('email');
	$password = Request :: getField('password');
	
	if( User :: adminLoggin($email, $password) == true)
		Request :: redirectTo('index.php');
	else
		$error .= 'Vos identifiants sont incorrects.';
	
}

HTML_CSS :: addFonts('JeanLucThin');
HTML_CSS :: addFile(PATH_ADMIN_CSS . 'login.css');
Page :: setAdmin(true);
Page :: setHeader('headerLogin.php');
Page :: setFooter('footerLogin.php');

//Definit le titre de la page
Page :: setTitle(TITLE_BY_DEFAULT_ADMIN);

Trigger :: call('onStartHTML');
?>

<div id="contentLogin">
	
	<h1><img src="<?=PATH_HTTP_IMG_GLOBAL?>logo_principal.png" alt="Logo" /></h1>
	<div class="boxConnexion">
		<div class="content">
			
			<?= ($error !== null) ? '<div class="error">'.$error.'</div>' : null ?>
			<form action="<?=$_SERVER['PHP_SELF']?>" method="post">
				<table>
					<tr>
						<td align="right"><label for="email">Adresse E-Mail :</label></td>
						<td><input type="text" id="email" name="email" value="" /></td>
					</tr>
					<tr>
						<td align="right"><label for="password">Mot de passe :</label></td>
						<td><input type="password" id="password" name="password" value="" /></td>
					</tr>
					<tr>
						<td colspan="2" align="center">
							<input type="hidden" name="action" value="connexion" />
							<input type="submit" class="button blueSite" name="identifier" value="Connexion" />
						</td>
					</tr>
				</table>
			</form>
			
		</div>
	</div>

</div>