<?php
require_once('.loader.php');
?>

<div class="addPicture content" style="height : 400px;">
	
	
	
	<!-- ADD -->
	
	
	
	<?php  if(Request :: getAction() == 'add') : ?>
			
	<form action="<?=str_replace('-html', '', $_SERVER['PHP_SELF'])?>?idParent=<?=Request :: getInt('idParent')?>" method="post" enctype="multipart/form-data">
		<table>
			<? if(User :: isSuperAdmin()) : ?>
				<tr>
					<td><label for="meta_key">Clef :</label></td>
					<td><input type="text" name="meta_key" id="meta_key" value="" size="50" /></td>
				</tr>
			<?php endif; ?>
			<tr>
				<td><label for="description">Description :</label></td>
				<td><input type="text" name="description" id="description" value="" size="50" /></td>
			</tr>
			<tr>
				<td><label for="valueText">Valeur :</label></td>
				<td><textarea name="valueText" id="valueText"></textarea></td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top : 10px;">
					<input type="hidden" name="action" value="add" />
					<input type="submit" class="button positionButton" name="addCollection" value="Ajouter une ligne" />
				</td>
			</tr>
		</table>
	</form>
	
	<?php endif; ?>
	
	
	
	<!-- EDIT -->
	
	
	
	<?php  if(Request :: getAction() == 'edit') : 
	
	$alLines = array();
	Database :: getLine('
	select S.id_option, S.meta_key, S.`value`, S.description
	from '.BDD.'options S
	where S.id_option="'.mysql_real_escape_string(Request :: getInt('id_option')).'"', $allLines);
	if(count($allLines) < 2){ echo 'Cet élément n\'éxiste pas.'; exit; }
	
	?>
			
	<form action="<?=str_replace('-html', '', $_SERVER['PHP_SELF'])?>?idParent=<?=Request :: getInt('idParent')?>" method="post" enctype="multipart/form-data">
		<table>
			<? if(User :: isSuperAdmin()) : ?>
				<tr>
					<td><label for="meta_key">Clef :</label></td>
					<td><input type="text" name="meta_key" id="meta_key" value="<?=$allLines['meta_key']?>" size="50" /></td>
				</tr>
			
				<tr>
					<td><label for="description">Description :</label></td>
					<td><input type="text" name="description" id="description" value="<?=$allLines['description']?>" size="50" /></td>
				</tr>
			<?php endif; ?>
			<tr>
				<td><label for="valueText">Valeur :</label></td>
				<td><textarea name="valueText" id="valueText"><?=$allLines['value']?></textarea></td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top : 10px;">
					<input type="hidden" name="action" value="edit" />
					<input type="hidden" name="id" value="<?=Request :: getInt('id_option')?>" />
					<input type="submit" class="button positionButton" name="addCollection" value="Editer une ligne" />
				</td>
			</tr>
		</table>
	</form>
	
	<?php endif; ?>
	
	
	
</div><!-- .content -->
<script>
	var instance = CKEDITOR.instances['valueText'];
  if(instance)
  {
      CKEDITOR.remove(instance);
  }
  CKEDITOR.replace('valueText', {
  	toolbar : 'FullOption',
  	height : '150px',
  	'entities_latin' : false,
    'entities_greek' : false,
    'allowedContent' : true, 
  });
</script>