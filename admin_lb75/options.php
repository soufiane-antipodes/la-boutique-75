<?php
require_once('.loader.php');

Database :: verifyConnexion();

$error 		= null;
$success 	= null;

$information = array('id' => 'id_option', 'relation' => 'id_article', 'table' => 'options');
$idParent=  Request :: getInt('idParent', 1);

if(Request :: getAction() == 'add'){

	$fields = array();
	$iCount = 0;
	$iLastID = Database :: get('SELECT ' . $information['id'] . ' FROM ' . BDD . $information['table'] . ' ORDER BY ' . $information['id'] . ' DESC LIMIT 0,1');
	foreach (unserialize(EXISTING_LANGUAGES) as $iKey => $sValue) 
	{
		$fields['id_article'] = array(
								 'name' => 'ID Relation',
								 'value' => $iLastID + 1, 
								 'type' => 'number', 
								 'isMandatory' => true
								 );
		$fields['meta_key'] = array('
								name' => 'Clef',
								'value' => Request :: getField('meta_key'), 
								'type' => 'string', 
								'isMandatory' => false
								);
		$fields['description'] = array(
								'name' => 'Description',
								'value' => Request :: getField('description'), 
								'type' => 'string', 
								'isMandatory' => false
								);
		$fields['value'] = array(
								'name' => 'Valeur',
								'value' => Request :: getField('valueText'), 
								'type' => 'string', 
								'isMandatory' => false
								);
		$fields['idOptionType'] = array(
								'name' => 'Valeur',
								'value' => $idParent, 
								'type' => 'string', 
								'isMandatory' => true
								);
		$fields['lang'] = array(
								'name' => 'Langue',
								'value' => $sValue, 
								'type' => 'string', 
								'isMandatory' => true
								);

		$position = array('position' => 'end', 'condition' => 'lang="' . $sValue . '"');
		//$position = array('position' => 'end', 'condition' => 'idOptionType="'.mysql_real_escape_string($idParent).'"');
		
		$result = Manage :: add($information['table'], array(), $fields, array(), $position, $error);
		$iCount++;
	}

	
	if($result == true) $success = 'Ligne ajoutée';
}

if(Request :: getAction() == 'edit'){
	
	$idValue = Request :: getInt('id');
	$fields = array();
	
	if(User :: isSuperAdmin())
	{
		$fields['meta_key'] = array(
							'name' => 'Clef',
							'value' => Request :: getField('meta_key'), 
							'type' => 'string', 
							'isMandatory' => false
							);
		$fields['description'] = array(
							'name' => 'Description',
							'value' => Request :: getField('description'), 
							'type' => 'string', 
							'isMandatory' => false
							);
	}
	
	$fields['value'] = array(
							'name' => 'Valeur',
							'value' => Request :: getField('valueText'), 
							'type' => 'string', 
							'isMandatory' => false
							);

	$id = array('name' => $information['id'], 'value' => $idValue);
	
	
	$result = Manage :: edit($information['table'], $id, '', $fields, array(), $error);
	if($result == true) $success = 'Ligne modifiée';
}

if(Request :: getAction() == 'delete'){
	$idValue = Request :: getInt('id');
	$aID = array();
	$iResult = 0;
	Database :: getTable(
		'SELECT ' . $information['id'] . ', 
			    ' . $information['relation'] . ' 
		FROM ' . BDD . $information['table'] . ' 
		WHERE ' . $information['relation'] . ' = 
		( SELECT ' . $information['relation'] . ' FROM ' . BDD . $information['table'] . ' WHERE ' . $information['id'] . ' = ' . $idValue . ')'
		, $aID);		

	$iCount = 0;
	foreach ($aID as $iKey => $sValue) 
	{
		$id = array('name' => $information['id'], 'value' => $sValue['id_option']);
		$pictureInfos = array();
		$result = Manage :: delete($information['table'], $id, $pictureInfos, $error);
		if ( $iCount === 0 ) $iResult = $result;
		$iCount++;
	}
	echo (int) $iResult;	
	exit;
}


$alLines = array();
Database :: getTable('
				SELECT '.$information['id'].', meta_key, value, description
				FROM '.BDD.$information['table'].' 
				WHERE lang="'.Session :: get('langAdminToUse').'" AND 
					  idOptionType="'.mysql_real_escape_string($idParent).'"
				ORDER BY position
			', $alLines);

$nameOption = Database :: get('SELECT name FROM '.BDD.'options_type WHERE idOptionType="'.mysql_real_escape_string($idParent).'"');


HTML_Script :: addFile(PATH_HTTP_PLUGINS_COLORBOX . 'colorbox.js');
HTML_CSS 		:: addFile(PATH_HTTP_PLUGINS_COLORBOX . 'colorbox-v2.css');
HTML_Script :: addFile(PATH_HTTP_JS . 'mySortable.js');
HTML_Script :: addFile(PATH_HTTP_PLUGINS_CKEDITOR . 'ckeditor.js');

HTML_Script :: addScript("

	//GENERATE POPUP
	$('body').delegate('.bubble', 'click', function(){
		$(this).colorbox({width : '50%',
		
			onClosed : function(){
				location.reload();
			}});
	});
	
	//DELETE BUTTONS
	$('.delete').click(function(){
	
		var idName = $(this).attr('id');
		var idName = idName.replace('delete-', '');
	
	if(confirm('Are you sure to delete this element ?')){
		$.ajax({
				   type: 'GET',
				   url: '',
				   data: 'action=delete&id='+idName,
				   success: function(msg){
				   	//console.log(msg);
				   	if(msg == 1){
				   		$('#myEntry-'+idName).remove();
				   	}
				   	
				   }
				 }); 
	}
	});
	
	$('.myTable tbody').mySortable({table : '".$information['table']."', nameId : '".$information['id']."'});
	
");

//Definit le titre de la page
Page :: setTitle(TITLE_BY_DEFAULT_ADMIN);
Page :: setAdmin(true);

if(in_array($idParent, array(2,3,4,5,6)))
	Page :: setIndexMenu(1);
if(in_array($idParent, array(55,54)))
	Page :: setIndexMenu(5);
if(in_array($idParent, array(66,65)))
	Page :: setIndexMenu(6);
if(in_array($idParent, array(88,87)))
	Page :: setIndexMenu(8);
if(in_array($idParent, array(888,889)))
	Page :: setIndexMenu(9);

Trigger :: call('onStartHTML');
?>

<div id="container">
	<div id="headerContainer">Textes généraux - <?=$nameOption?></div><!-- .header -->
	<div id="contentContainer">

<?= ($error !== null) ? '<div class="error">'.$error.'</div>' : null; ?>
<?= ($success !== null) ? '<div class="success">'.$success.'</div>' : null; ?>

<? if(User :: isSuperAdmin()) : ?>
<div class="action" style="margin-bottom : 10px;">
	<a href="<?=str_replace( '.php', '', $_SERVER['PHP_SELF'])?>-html.php?action=add&idParent=<?=$idParent?>" class="bubble">
		<input type="button" class="button" name="add" value="AJOUTER UNE LIGNE" />
	</a>
</div><!-- .action -->
<?php endif; ?>

<?php
	if(count($alLines) > 0){
		?>
			<table class="myTable">
				<tr>
					<? if(User :: isSuperAdmin()) : ?>
					<th>position</th>
					<th>meta_key</th><?php endif; ?>
					<th>Description</th>
					<th>Valeur</th>
					<th>Editer</th>
					<? if(User :: isSuperAdmin()) : ?><th>Supprimer</th><?php endif; ?>
				</tr>
				
				<? foreach($alLines as $line) : ?>
				
				<tr id="myEntry-<?=$line[$information['id']]?>" class="entry">
					<? if(User :: isSuperAdmin()) : ?><td class="move"><img src="<?=PATH_ADMIN_IMG_ICONS?>move.png" alt="Move" width="16" height="16" /></td>
					<td><?=$line['meta_key']?></td><?php endif; ?>
					<td><?=$line['description']?></td>
					<td><?=Sanitize :: html_excerpt(htmlspecialchars_decode($line['value']), 150)?></td>
					<td style="width:50px;"><a href="<?=str_replace( '.php', '', $_SERVER['PHP_SELF'])?>-html.php?action=edit&idParent=<?=$idParent?>&<?=$information['id']?>=<?=$line[$information['id']]?>" class="bubble"><img src="<?=PATH_ADMIN_IMG_ICONS?>edit.png" alt="Edit" title="Edit" width="16" height="16" /></td>
					<? if(User :: isSuperAdmin()) : ?><td style="width:50px;"><img class="delete" id="delete-<?=$line[$information['id']]?>" src="<?=PATH_ADMIN_IMG_ICONS?>error.png" alt="Delete" title="Delete" width="16" height="16" /></td><?php endif; ?>
				</tr>
				
				<? endforeach; ?>
				
			</table>
		<?php
	}
?>


	</div><!-- .contentContainer -->
</div><!-- #container -->