<?php
require_once('.loader.php');

$information = array('id' => 'id', 'relation' => 'id_article', 'table' => 'presses');

$iIdParent = Request::getInt('idParent');
?>

<div class="addPicture content" style="height : 360px;">
	
	
	
	<!-- ADD -->
	
	
	
	<?php  if(Request :: getAction() == 'add') : ?>
			
	<form action="<?=str_replace('-html', '', $_SERVER['PHP_SELF'])?>?idParent=<?=$iIdParent?>" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td><label for="title">Titre :<span class="star">*</span></label></td>
				<td><input type="text" name="title" id="title" value="" size="50" /></td>
			</tr>
			<tr>
				<td><label for="description">Description :</label></td>
				<td><textarea name="description" id="description"></textarea></td>
			</tr>
			<tr>
				<td><label for="meta_title">Méta titre :</label></td>
				<td><input type="text" name="meta_title" id="meta_title" value="" size="100" /></td>
			</tr>
			<tr>
				<td><label for="meta_description">Méta description :</label></td>
				<td><input type="text" name="meta_description" id="meta_description" value="" size="100" /></td>
			</tr>
			<tr>
				<td><label for="meta_keyword">Méta mots clé :</label></td>
				<td><input type="text" name="meta_keyword" id="meta_keyword" value="" size="100" /></td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top : 10px;">
					<input type="hidden" name="action" value="add" />
					<input type="submit" class="button positionButton" name="addCollection" value="Ajouter une ligne" />
				</td>
			</tr>
		</table>
	</form>
	
	<?php endif; ?>
	
	
	
	<!-- EDIT -->
	
	
	
	<?php  if(Request :: getAction() == 'edit') : 
	
	$allLines = array();
	Database :: getLine(
				'SELECT *
				FROM '.BDD.$information['table'].' S 
				WHERE S.'.$information['id'].'="'.mysql_real_escape_string(Request :: getInt('id')).'"'
				, $allLines);

	if(!isset($allLines['id']) || $allLines['id'] != Request :: getInt('id') ){ echo 'Cet élément n\'éxiste pas.'; exit; }
	
	?>
			
	<form action="<?=str_replace('-html', '', $_SERVER['PHP_SELF'])?>?idParent=<?=$iIdParent?>" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td><label for="title">Titre :<span class="star">*</span></label></td>
				<td><input type="text" name="title" id="title" value="<?=$allLines['title']?>" size="50" /></td>
			</tr>
			<tr>
				<td><label for="description">Description :</label></td>
				<td><textarea name="description" id="description"><?=$allLines['description']?></textarea></td>
			</tr>
			<tr>
				<td><label for="meta_title">Méta titre :</label></td>
				<td><input type="text" name="meta_title" id="meta_title" value="<?=$allLines['meta_title']?>" size="100" /></td>
			</tr>
			<tr>
				<td><label for="meta_description">Méta description :</label></td>
				<td><input type="text" name="meta_description" id="meta_description" value="<?=$allLines['meta_description']?>" size="100" /></td>
			</tr>
			<tr>
				<td><label for="meta_keyword">Méta mots clé :</label></td>
				<td><input type="text" name="meta_keyword" id="meta_keyword" value="<?=$allLines['meta_keyword']?>" size="100" /></td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top : 10px;">
					<input type="hidden" name="action" value="edit" />
					<input type="hidden" name="id" value="<?=Request :: getInt('id')?>" />
					<input type="submit" class="button positionButton" name="addCollection" value="Editer" />
				</td>
			</tr>
		</table>
	</form>
	
	<?php endif; ?>
	
	
	
</div><!-- .content -->

<script>
var instance = CKEDITOR.instances['description'];
if(instance)
{
	CKEDITOR.remove(instance);
}
	CKEDITOR.replace('description', {
	toolbar : 'FullOption',
	height : '150px',
	'entities_latin' : false,
    'entities_greek' : false,
    'allowedContent' : true, 
});



</script>