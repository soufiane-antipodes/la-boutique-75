<?php
require_once('.loader.php');

$information = array('id' => 'id', 'table' => 'presse_parution');

$iIdParent = Request::getInt('data-id');

?>

<div class="addPicture content" style="height : 500px;">
	
	
	
	<!-- ADD -->
	
	
	
	<?php  if(Request :: getAction() == 'add') : ?>
			
	<form action="<?=str_replace('-html', '', $_SERVER['PHP_SELF'])?>?data-id=<?=$iIdParent?>" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td><label for="title_fr">Titre Fr :<span class="star">*</span></label></td>
				<td><input type="text" name="title_fr" id="title_fr" value="" size="50" /></td>
			</tr>
			<tr>
				<td><label for="title_en">Titre En :<span class="star">*</span></label></td>
				<td><input type="text" name="title_en" id="title_en" value="" size="50" /></td>
			</tr>
			<tr>
				<td><label for="title_cn">Titre Cn :<span class="star">*</span></label></td>
				<td><input type="text" name="title_cn" id="title_cn" value="" size="50" /></td>
			</tr>
			<tr>
				<td><label for="description_fr">Description Fr :</label></td>
				<td><textarea name="description_fr" id="description_fr"></textarea></td>
			</tr>
			<tr>
				<td><label for="description_en">Description En :</label></td>
				<td><textarea name="description_en" id="description_en"></textarea></td>
			</tr>
			<tr>
				<td><label for="description_cn">Description Cn :</label></td>
				<td><textarea name="description_cn" id="description_cn"></textarea></td>
			</tr>
			<tr>
				<td><label for="filePicture">Charger une photo :<span class="star">*</span></label></td>
				<td><input type="file" id="filePicture" name="filePicture" /></td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top : 10px;">
					<input type="hidden" name="action" value="add" />
					<input type="submit" class="button positionButton" name="addCollection" value="Ajouter une ligne" />
				</td>
			</tr>
		</table>
	</form>
	
	<?php endif; ?>
	
	
	
	<!-- EDIT -->
	
	
	
	<?php  if(Request :: getAction() == 'edit') : 
	
	$allLines = array();
	Database :: getLine(
				'SELECT *
				FROM '.BDD.$information['table'].' S 
				WHERE S.'.$information['id'].'="'.mysql_real_escape_string(Request :: getInt('id')).'"'
				, $allLines);

	if(!isset($allLines['id']) || $allLines['id'] != Request :: getInt('id') ){ echo 'Cet élément n\'éxiste pas.'; exit; }
	
	?>
			
	<form action="<?=str_replace('-html', '', $_SERVER['PHP_SELF'])?>?data-id=<?=$iIdParent?>" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td><label for="title_fr">Titre Fr :<span class="star">*</span></label></td>
				<td><input type="text" name="title_fr" id="title_fr" value="<?=$allLines['title_fr']?>" size="50" /></td>
			</tr>
			<tr>
				<td><label for="title_en">Titre En :<span class="star">*</span></label></td>
				<td><input type="text" name="title_en" id="title_en" value="<?=$allLines['title_en']?>" size="50" /></td>
			</tr>
			<tr>
				<td><label for="title_cn">Titre Cn :<span class="star">*</span></label></td>
				<td><input type="text" name="title_cn" id="title_cn" value="<?=$allLines['title_cn']?>" size="50" /></td>
			</tr>
			<tr>
				<td><label for="description_fr">Description Fr :</label></td>
				<td><textarea name="description_fr" id="description_fr"><?=$allLines['description_fr']?></textarea></td>
			</tr>
			<tr>
				<td><label for="description_en">Description En :</label></td>
				<td><textarea name="description_en" id="description_en"><?=$allLines['description_en']?></textarea></td>
			</tr>
			<tr>
				<td><label for="description_cn">Description Cn :</label></td>
				<td><textarea name="description_cn" id="description_cn"><?=$allLines['description_cn']?></textarea></td>
			</tr>
			<tr>
				<td><label for="filePicture">Charger une photo :<span class="star">*</span></label></td>
				<td><input type="file" id="filePicture" name="filePicture" /></td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top : 10px;">
					<input type="hidden" name="action" value="edit" />
					<input type="hidden" name="id" value="<?=Request :: getInt('id')?>" />
					<input type="submit" class="button positionButton" name="addCollection" value="Editer" />
				</td>
			</tr>
		</table>
	</form>
	
	<?php endif; ?>
	
	
	
</div><!-- .content -->

<script>
var instance = CKEDITOR.instances['description_fr'];
if(instance)
{
	CKEDITOR.remove(instance);
}
	CKEDITOR.replace('description_fr', {
	toolbar : 'FullOption',
	height : '150px',
	'entities_latin' : false,
    'entities_greek' : false,
    'allowedContent' : true, 
});

var instance = CKEDITOR.instances['description_en'];
if(instance)
{
	CKEDITOR.remove(instance);
}
	CKEDITOR.replace('description_en', {
	toolbar : 'FullOption',
	height : '150px',
	'entities_latin' : false,
    'entities_greek' : false,
    'allowedContent' : true, 
});


var instance = CKEDITOR.instances['description_cn'];
if(instance)
{
	CKEDITOR.remove(instance);
}
	CKEDITOR.replace('description_cn', {
	toolbar : 'FullOption',
	height : '150px',
	'entities_latin' : false,
    'entities_greek' : false,
    'allowedContent' : true, 
});



</script>