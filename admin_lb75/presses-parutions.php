<?php
require_once('.loader.php');

Database :: verifyConnexion();

$error 		= null;
$success 	= null;

$information = array('id' => 'id', 'table' => 'presse_parution');

$iIdParent = Request::getInt('data-id');

if(Request :: getAction() == 'add'){
	
	$fields = array();
	$fields['id_parent'] = array(
							 'name' => 'ID Relation',
							 'value' => $iIdParent,
							 'type' => 'number', 
							 'isMandatory' => true
							 );
	$fields['title_fr'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('title_fr'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$fields['title_en'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('title_en'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$fields['title_cn'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('title_cn'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$fields['description_fr'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('description_fr'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$fields['description_en'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('description_en'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$fields['description_cn'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('description_cn'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$file = array();
	$thumbnails = array();

	$file = $_FILES['filePicture'];
	$file['idName'] = 'id_item';
	$file['isMandatory'] = true;
	$file['dest'] = PATH_IMG_PAGES_ALL;
	$file['extension'] = array('.jpg', '.JPG', '.png','.PNG');

	$position = array('position' => 'first', 'condition' => 'lang="' . Session :: get('langAdminToUse') . '"');
	$result = Manage :: add($information['table'], $file, $fields, $thumbnails, $position, $error);


	if($result == true) $success = 'Ligne ajoutée';

}

if(Request :: getAction() == 'edit'){
	
	$idValue = Request :: getInt('id');
	$fields = array();
	

	$fields['title_fr'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('title_fr'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$fields['title_en'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('title_en'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$fields['title_cn'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('title_cn'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$fields['description_fr'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('description_fr'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$fields['description_en'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('description_en'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$fields['description_cn'] = array(
							 'name' => 'Titre',
							 'value' => Request :: getField('description_cn'), 
							 'type' => 'string', 
							 'isMandatory' => true
							 );
	$file = array();
	$thumbnails = array();

	$file = $_FILES['filePicture'];
	$file['idName'] = 'id_item';
	$file['isMandatory'] = true;
	$file['dest'] = PATH_IMG_PAGES_ALL;
	$file['extension'] = array('.jpg', '.JPG', '.png','.PNG');

	$id = array('name' => $information['id'], 'value' => $idValue);
	
	$result = Manage :: edit($information['table'], $id, $file, $fields, $thumbnails, $error);
	if($result == true) $success = 'Ligne modifiée';
}

if(Request :: getAction() == 'delete')
{	
	$idValue = Request :: getInt('id');
	$aID = array();
	$iResult = 0;
	Database :: getTable('SELECT id FROM ' . BDD . $information['table'] . ' WHERE id_article = ( SELECT id_article FROM ' . BDD . $information['table'] . ' WHERE id = ' . $idValue . ')', $aID);
	$iCount = 0;
	foreach ($aID as $iKey => $sValue) 
	{
		$id = array('name' => $information['id'], 'value' => $sValue['id']);
		$pictureInfos = array('normal' => array('dest' => PATH_IMG_SLIDER));
		$result = Manage :: delete($information['table'], $id, $pictureInfos, $error);
		if ( $iCount === 0 ) $iResult = $result;
		$iCount++;
	}
	echo (int) $iResult;	
	exit;
}

$alLines = array();
Database :: getTable(
				'SELECT S.*,
				 I.value AS img, 
				 I.extension AS img_extension
				 FROM '.BDD.$information['table'].' S 
				 	LEFT JOIN '.BDD.'item I ON I.id_item = S.id_item 
				 WHERE S.id_parent="'.mysql_real_escape_string( $iIdParent ).'" 
				 ORDER BY S.position'
				 , $alLines);

HTML_Script :: addFile(PATH_HTTP_PLUGINS_COLORBOX . 'colorbox.js');
HTML_CSS 		:: addFile(PATH_HTTP_PLUGINS_COLORBOX . 'colorbox-v2.css');

HTML_Script :: addFile(PATH_HTTP_PLUGINS_CKEDITOR . 'ckeditor.js');

HTML_Script :: addFile(PATH_HTTP_JS . 'myPublish.js');
HTML_Script :: addFile(PATH_HTTP_JS . 'mySortable.js');

HTML_Script :: addScript("

	//GENERATE POPUP
	$('body').delegate('.bubble', 'click', function(){
		$(this).colorbox({width : '50%',
		
			onClosed : function(){
				location.reload();
			}});
	});
	
	//DELETE BUTTONS
	$('.delete').click(function(){
	
		var idName = $(this).attr('id');
		var idName = idName.replace('delete-', '');
	
	if(confirm('Are you sure to delete this element ?')){
		$.ajax({
				   type: 'GET',
				   url: '',
				   data: 'action=delete&id='+idName,
				   success: function(msg){
				   	//console.log(msg);
				   	if(msg == 1){
				   		$('#myEntry-'+idName).remove();
				   	}
				   	
				   }
				 }); 
	}
	});
	
	$('.myTable tbody').myPublish({table : '" . $information['table'] . "', nameId : '" . $information['id'] . "'});
	$('.myTable tbody').mySortable({table : '" . $information['table'] . "', nameId : '" . $information['id'] . "'});
	
");

//Definit le titre de la page
Page :: setTitle(TITLE_BY_DEFAULT_ADMIN);
Page :: setAdmin(true);
Page :: setIndexMenu(6);
Trigger :: call('onStartHTML');

$sNamePage = ($iIdParent == 1 ) ? 'Magazin':'Web';

?>

<div id="container">
	<div id="headerContainer">Presse parution <!-- - <?=$sNamePage?> --></div><!-- .header -->
	<div id="contentContainer">

<?= ($error !== null) ? '<div class="error">'.$error.'</div>' : null; ?>
<?= ($success !== null) ? '<div class="success">'.$success.'</div>' : null; ?>

<div class="action" style="margin-bottom : 10px;">
	<a href="<?=str_replace( '.php', '', $_SERVER['PHP_SELF'])?>-html.php?action=add&amp;data-id=<?=$iIdParent?>" class="bubble">
		<input type="button" class="button" name="add" value="AJOUTER UNE LIGNE" />
	</a>
</div><!-- .action -->

<?php
	if(count($alLines) > 0){
		?>
			<table class="myTable">
				<tr>
					<th></th>
					<th>Image</th>
					<th>Titre</th>
					<th>Description</th>
					<th>Publier</th>
					<th>Editer</th>
					<th>Supprimer</th>
				</tr>
				
				<? foreach($alLines as $line) : ?>
				<tr id="myEntry-<?=$line[$information['id']]?>" class="entry">
					<td class="move">
						<img src="<?=PATH_ADMIN_IMG_ICONS?>move.png" alt="Move" width="16" height="16" />
					</td>
					<td>
						<img src="<?=PATH_HTTP_IMG_PAGES_ALL . Sanitize :: keepValidChars($line['img']) . '-' . $line['id_item'] . '.' . $line['img_extension']?>" alt="" style="width:auto;" height="100" />
					</td>
					<td><?=$line['title_'.Session :: get('langAdminToUse')]?></td>
					<td><?=Sanitize :: html_excerpt(htmlspecialchars_decode($line['description_'.Session :: get('langAdminToUse')]), 150)?></td>
					<td style="width:50px;">
						<input type="checkbox" name="publish" class="publish" id="publish-<?= $line[$information['id']] ?>" rel="publish-<?= $line[$information['relation']] ?>" value="1" <?=($line['publish'] == 1) ? 'checked="checked"':null;?>/>
					</td>
					<td style="width:50px;">
						<a href="<?=str_replace( '.php', '', $_SERVER['PHP_SELF'])?>-html.php?action=edit&amp;id=<?=$line[$information['id']]?>&amp;data-id=<?=$iIdParent?>" class="bubble">
							<img src="<?=PATH_ADMIN_IMG_ICONS?>edit.png" alt="Edit" title="Edit" width="16" height="16" />
						</a>
					</td>
					<td style="width:50px;">
						<img class="delete" id="delete-<?=$line[$information['id']]?>" src="<?=PATH_ADMIN_IMG_ICONS?>error.png" alt="Delete" title="Delete" width="16" height="16" />
					</td>
				</tr>
				
				<? endforeach; ?>
				
			</table>
		<?php
	}
?>


	</div><!-- .contentContainer -->
</div><!-- #container -->
