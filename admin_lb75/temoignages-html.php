<?php
require_once('.loader.php');
$information = array('id' => 'id_image', 'table' => 'images');


?>

<div class="addPicture content" style="height : 480px;">	
	
	<!-- ADD -->	
	
	<?php  if(Request :: getAction() == 'add') : ?>
			
	<form action="<?=str_replace('-html', '', $_SERVER['PHP_SELF'])?>" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td><label for="auteur">Auteur :<span class="star">*</span></label></td>
				<td><input type="text" name="auteur" id="auteur" value="" size="50" required /></td>
			</tr>
      		<tr>
				<td><label for="temoignage">Témoignage :<span class="star">*</span></label></td>
				<td><textarea name="temoignage" id="temoignage" ></textarea></td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top : 10px;">
					<input type="hidden" name="action" value="add" />
					<input type="submit" class="button positionButton" name="addCollection" value="Ajouter une ligne" />
				</td>
			</tr>
		</table>
	</form>
	
	<?php endif; ?>
	
	
	<!-- EDIT -->	
	
	
	<?php  if(Request :: getAction() == 'edit') : 
	
	$allLines = array();
	Database :: getLine('select * from '.BDD.$information['table'].' n where n.'.$information['id'].'="'.mysql_real_escape_string(Request :: getInt('id')).'"', $allLines);
	if(count($allLines) < 2){ echo 'Cet élément n\'éxiste pas.'; exit; }

	?>
	
	<div class="info">La photo doit avoir les tailles max suivantes : 2500*2500 pixels | 4Mo | jpg/png</div>
			
	<form action="<?=str_replace('-html', '', $_SERVER['PHP_SELF'])?>" method="post" enctype="multipart/form-data">
		<table>
			<tr>
				<td><label for="auteur">Auteur :<span class="star">*</span></label></td>
				<td><input type="text" name="auteur" id="auteur" value="<?=$allLines['auteur']?>" size="50" required /></td>
			</tr>
	         <tr>
			<td>
	          <label for="temoignage">Témoignage :<span class="star">*</span></label>
	        </td>
	        <td>
	          <textarea name="temoignage" id="temoignage" ><?=$allLines['temoignage']?></textarea>
	        </td>
	      </tr>
			<tr>
				<td colspan="2" style="padding-top : 10px;">
					<input type="hidden" name="action" value="edit" />
					<input type="hidden" name="id" value="<?=Request :: getInt('id')?>" />
					<input type="submit" class="button positionButton" name="addCollection" value="Editer une ligne" />
				</td>
			</tr>
		</table>
	</form>
	
	<?php endif; ?>	
</div><!-- .content -->

<script>
	var instance = CKEDITOR.instances['temoignage'];
  if(instance)
  {
      CKEDITOR.remove(instance);
  }
  CKEDITOR.replace('temoignage', 
  {
  	toolbar : 'FullOption',
  	height : '300px',
  	'entities_latin' : false,
    'entities_greek' : false,
    'allowedContent' : true
  });
</script> 