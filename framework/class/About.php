<?php
Class About{
	
	public static function edit($fileToUpload, $textFR, $textEN, &$error){
		
		if($fileToUpload['size'] > 0){
			//Test upload
			$result = Picture :: isOkForUpload($fileToUpload, $error);
			if($result !== true) return $result;
		}
		
		//START Transaction
		Database :: query('BEGIN;');
		
		//Insert New Film in BDD) 
		$result = Database :: query('update '.BDD.'t_about set textFR="'.mysql_real_escape_string($textFR).'", textEN="'.mysql_real_escape_string($textEN).'"');
		
		//Upload File
		if($fileToUpload['size'] > 0){
			
			if(file_exists(PATH_IMG_ABOUT . 'about.jpg')){
				unlink(PATH_IMG_ABOUT . 'about.jpg');
			}
			
			//Upload File
			$result = Picture :: upload($fileToUpload, PATH_IMG_ABOUT . 'about', $error);
			if($result !== true){ Database :: query('ROLLBACK;');return $result;}
			
			//Create Thumbnail
			$result = Picture :: createThumb(PATH_IMG_ABOUT, PATH_IMG_ABOUT, 'about.jpg', null, 461, 504);
			if($result !== true){ Database :: query('ROLLBACK;');$error.='Fichier source corrompu'; Database :: query('ROLLBACK;');return $result;}
		}
		
		//END Transaction
		Database :: query('COMMIT;');
		
		return $result;

	}
	
}


