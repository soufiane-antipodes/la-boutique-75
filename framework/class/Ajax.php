<?php
Class Ajax{
	
	/*****
	***** FRONT
	*****/
	public static function sendRecrutement($table, $fileToUpload, &$error){
		
		Picture :: setExtensions(array('.pdf'));
		$name = basename($fileToUpload['name'], '.pdf');
		
		//Test upload
		$result = Picture :: isOkForUpload($fileToUpload, $error);
		if($result !== true) return $result;
		
		foreach($table as $key => $value){
			if($key != 'message' && empty($value)){ $error .= 'Un ou plusieurs champ est vide ou incorrect'; return false;}
		}
			
		$result = Validation :: email($table['email']);
		if($result !== true){ $error .= 'Email incorrect'; return $result; }
			
		$result = Validation :: phone($table['phone']);
		if($result !== true){ $error .= 'Téléphone incorrect - 10 nombres'; return $result; }
			
		//Upload File
		$result = Picture :: upload($fileToUpload, PATH_IMG_PDF . '/' . Sanitize :: keepValidChars($table['firstname']).'_'.Sanitize :: keepValidChars($table['lastname']), $error);
		if($result !== true){return $result;}
		
		$html = '
						Bonjour, <br /><br />Une personne éssaie de prendre contact avec vous pour un recrutement<br /><br />
						
						Informations : <br />
						Nom : '.$table['lastname'].'<br />
						Prénom : '.$table['firstname'].'<br />
						Tél : '.$table['phone'].'<br />
						Email : '.$table['email'].'<br />
						Message : '.$table['message'].'<br /><br />
						'.( ($fileToUpload['size'] > 0) ? 'Le CV est disponible ici : '.PATH_HTTP_IMG_PDF . Sanitize :: keepValidChars($table['firstname']).'_'.Sanitize :: keepValidChars($table['lastname']) . '.pdf' : 'Aucun CV n\'a été fournit').'<br />
						
						--<br />
						Cordialement.<br />
						  L\'équipe IDI System.
						';
			
			$email = new Email();
			$result = $email	-> setTo(EMAIL_ADMIN . EMAIL_TEST)
												-> setFrom(EMAIL_FROM)
												-> setSubject('[IDI System] - Demande de recrutement')
												-> setBody($html)
												-> send();
			return $result;
	}
	
}


