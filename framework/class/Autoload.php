<?
function __autoload($className){
	
	$className = str_replace('_', '/', $className);

	if(file_exists(PATH_FRAMEWORK_CLASSES . $className . '.php'))
		require_once(PATH_FRAMEWORK_CLASSES . $className . '.php');
	else
		throw new Exception('La Classe ' . $className . ' n\'a pas pu être autoloadée.');
} 

?>