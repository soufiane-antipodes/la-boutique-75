<?php

/**
* Class Breadcrumbs
* 
* Permet de creer une arborescence
*
* @author Julien Guion
* @version 1.0
* @example >>Port Folio >> Hobo & Mojo >> Album 1
*/
class Breadcrumbs{
	
	/**
	* Cree l'arborescence
	*
	*	@param integer $idCategory
	*	@return mixed
	*/
	public static function createBreadcrumbs($idCategory){
		
		$arrayToReturn = array();
		$end = false;
		
		while($end === false){
			$infos = array();
			Database :: getLine('select idCategory, idSubCategory, name from '.BDD.'t_categories where idCategory = "'.$idCategory.'"', $infos);
			
			if(count($infos) > 2)
				$arrayToReturn[] = array('idCategory' => $infos['idCategory'], 'name' => $infos['name']);
			else
				$end = true;
				
			$idCategory = $infos['idSubCategory'];
		}
		
		return array_reverse($arrayToReturn);
	}
	
	/**
	* Recupere l'arborescence en html
	*
	*	@param mixed $breadcrumbs
	*	@return string
	*/
	public static function getBreadcrumbs($breadcrumbs){
		
		$html = null;
		
		if(count($breadcrumbs) > 0){
				$html .= '<span>';
			
					foreach($breadcrumbs as $key => $breadcrumb)
						$html .= '<a href="'.$_SERVER['PHP_SELF'].'?parse=parse&amp;idCategory='.$breadcrumb['idCategory'].'">'.$breadcrumb['name'].'</a>';
					
				$html .= '</span>';
		}
		
		return $html;
	}

}