<?php
Class Category{
	
	private static $_initialized        = false;
	private static $_tData              = array();
	private static $_tDataSimplified    = array();
	private static $_tDataWithInformation  = array();
	private static $_maxlevel        	= 3;
	
	public static function prepend(){
		
		$allLines = array();
		Database :: getTable('select idCategory, idSubCategory, name_fr, name_en, id_continent from '.BDD.'categories order by position', $allLines);
		
		$allLinesSimplified = array();
		Database :: getOrderedTable('select idCategory, idSubCategory, name_fr, name_en, id_continent from '.BDD.'categories order by position', 'idCategory', $allLinesSimplified);
		
		self :: $_tData = $allLines;
		self :: $_tDataSimplified = $allLinesSimplified;
		self :: $_tDataWithInformation = self :: recurse(0, $allLines);
		
	}
	
	public static function getAllCategories(){
		if(!self::isInitialized()) self::prepend();
		return self :: $_tDataWithInformation;
	}
	
	public static function getChildrens($idCategory){
		if(!self::isInitialized()) self::prepend();
		return self :: recurse( $idCategory, self :: $_tData);
	}
	
	public static function getParents($idCategory){
		if(!self::isInitialized()) self::prepend();
		$result = array();
		self :: recurseInverse( $idCategory, self :: $_tDataSimplified, $result);
		return array_reverse($result);
	}
	
	public static function showAllCategories(){
		if(!self::isInitialized()) self::prepend();
		$table = self :: showRecurse(self :: $_tDataWithInformation, 0);
		return (count($table) > 0) ? '<ul>'.$table.'</ul>' : null;
	}
	
	public static function showAllCategoriesComplex(){
		if(!self::isInitialized()) self::prepend();
		$table = self :: showRecurseComplex(self :: $_tDataWithInformation, 0);
		return (count($table) > 0) ? $table : null;
	}
	
	public static function isLastLevel($idCategory){
		if(!self::isInitialized()) self::prepend();
		return (count(self :: getParents($idCategory)) < self :: $_maxlevel) ? true : false;
	}
	
	public static function level($idCategory){
		if(!self::isInitialized()) self::prepend();
		return (count(self :: getParents($idCategory)));
	}
	
	public static function isBeforeLastLevel($idCategory){
		if(!self::isInitialized()) self::prepend();
		return (count(self :: getParents($idCategory)) < (self :: $_maxlevel - 1)) ? true : false;
	}
	
	public static function getBreadCrumb($idCategory){
		if(!self::isInitialized()) self::prepend();
		$table = self :: getParents($idCategory);
		return self :: createBreadCrumb($table);
	}
	
	public static function getBreadCrumbFront($idCategory){
		if(!self::isInitialized()) self::prepend();
		$table = self :: getParents($idCategory);
		return self :: createBreadCrumbFront($table);
	}
	
	public static function showAllCategoriesFront($maxLevel = 0){
		if(!self::isInitialized()) self::prepend();
		$is_footer=false;
		if($maxLevel == 0){
				self::setMaxLevel(3);
		}else{
				self::setMaxLevel($maxLevel);
				$is_footer=true;
		}
		$table = self :: showRecurseFront(self :: $_tDataWithInformation, 0, $is_footer);
		return (count($table) > 0) ? $table : null;
	}
	
	private static function setMaxLevel($maxLevel){
		self::$_maxlevel = $maxLevel;
	}
	
	
	/* */
	private static function recurse($parent, $array) {
	   $result = array();
	   foreach ($array as $noeud) {
	      if ($parent == $noeud['idSubCategory']) {
	         $result[] = array(
	            'id' => $noeud['idCategory'],
	            'id_continent' => $noeud['id_continent'],
	            'name_fr' => $noeud['name_fr'],
	            'name_en' => $noeud['name_en'],
	            'children' => self :: recurse($noeud['idCategory'], $array)
	         );      
	      }
	   }
	   return $result;
	}
	
	private static function recurseInverse($idCategory, $array, &$result) {
			if(isset($array[$idCategory])){
				$result[$idCategory] = array('idCategory' => $idCategory, 'idSubCategory' => $array[$idCategory]['idSubCategory'], 'name_fr' => $array[$idCategory]['name_fr'], 'name_en' => $array[$idCategory]['name_en']);
				self :: recurseInverse($array[$idCategory]['idSubCategory'], $array, $result);
			}
	   return $result;
	}
	
	
	private static function showRecurse($tab, $currentLevel) {
	   $list = '';
	   if(count($tab) > 0){
		   foreach ($tab as $elem) {
		   	
		   		if($currentLevel >= self :: $_maxlevel){
		   			return $list;
		   		}
		   		
			    $list .= '<li class="';
			    
			    switch($currentLevel){
			    	case 0 : $list .= 'firstLine';break;
			    	case 1 : $list .= 'secondLine';break;
			    	case 2 : $list .= 'thirdLine';break;
			    	case 3 : $list .= 'fourthLine';break;
			    	default : return $list;
			    }
			    
			    $list .='">';
			    
			    if($currentLevel < 2)
			    	$list .='<a href="'.PATH_ADMIN.'page.php?idParent='.$elem['id'].'">';
			    	$list .= $elem['name_fr'];
			    
			    if($currentLevel < 2)
			    	$list .='</a>';
			    if (count($elem['children']) > 0) {
			      $list .= '<ul>'.self :: showRecurse($elem['children'], ($currentLevel + 1)).'</ul>';
			    }
			    $list .= '</li>';
			  }
			}
	   return $list;
	}
	
	private static function showRecurseComplex($tab, $currentLevel) {
	   $list = '';
	   if(count($tab) > 0){
		   foreach ($tab as $elem) {
		   	
		   		if($currentLevel >= self :: $_maxlevel){
		   			return $list;
		   		}
		   		
			    switch($currentLevel){
			    	case 0 : $list .= '<div class="map-header" id="map-header_'.$elem['id'].'">';break;
			    	case 1 : $list .= '<div class="countries continent_'.$elem['id_continent'].'" id="country_'.$elem['id'].'">';break;
			    	case 2 : $list .= '<div class="cities" id="city_'.$elem['id'].'">';break;
			    	default : return $list;
			    }
			    
			    	$list .= $elem['name_'.Session :: get('langToUse')];
			    
			    if (count($elem['children']) > 0) {
			      $list .= self :: showRecurseComplex($elem['children'], ($currentLevel + 1));
			    }
			    $list .= '</div>';
			  }
			}
	   return $list;
	}
	
	private static function showRecurseFront($tab, $currentLevel, $is_footer) {
	   
	   $list = '';
	   $compteurFirstLevel=0;
	   if(count($tab) > 0){
	   	
				if($currentLevel == 1) $list .= '<ul class="subnav">';
				else if($currentLevel == 2) $list .= '<ul class="subsubnav">';
				
		   foreach ($tab as $elem) {
		   	
		   		if($currentLevel >= self :: $_maxlevel){
		   			continue;
		   		}
		   		
		   		
			    $list .= '<li';
			    if(Page :: getIndexMenu() == '2_'.$elem['id']) $list .= ' class = highlightMenu';			    
			    $list .= '>';
			    
			    
			    
			    	$list .='<a href="'.PATH_HTTP.'page.php?id='.$elem['id'].'">';
			    		$list .= $elem['name'];
			    	$list .='</a>';
			    	
			    if (count($elem['children']) > 0) {
			      $list .= self :: showRecurseFrontSubnav($elem['children'], ($currentLevel + 1), $is_footer);
			    }
			    $list .= '</li>';
			    if($is_footer AND ($compteurFirstLevel % 5 == 4) && count($tab) - 1 != $compteurFirstLevel) $list .= '</ul><ul>';
			    if($currentLevel== 0) $compteurFirstLevel++;
			  }
			  
			  if($currentLevel > 0) $list .= '</ul>';
			  
			}
	   return $list;
	}
	
		private static function showRecurseFrontSubnav($tab, $currentLevel, $is_footer){
	   
	   $list = '';
	   $compteurFirstLevel=0;
	   if(count($tab) > 0){
	   	
				if($currentLevel == 1) $list .= '<ul class="subnav">';
				else if($currentLevel == 2) $list .= '<ul class="subsubnav">';
				
		   foreach ($tab as $elem) {
		   	
		   		if($currentLevel >= self :: $_maxlevel){
		   			continue;
		   		}
		   		
		   		
		   	
			    $list .= '<li';
			    	if(Page :: getIndexMenu() == '2_'.$elem['id']) $list .= ' class = highlightMenu';
			    $list .= '>';
			    
			    
			    	$chevronListe = $is_footer ? '> ' : '';
			    	$list .='<a href="'.PATH_HTTP.'page.php?id='.$elem['id'].'">';
			    		$list .= $chevronListe.$elem['name'];
			    	$list .='</a>';
			    	
			    if (count($elem['children']) > 0) {
			      $list .= self :: showRecurseFrontSubnav($elem['children'], ($currentLevel + 1), $is_footer);
			    }
			    $list .= '</li>';
			    if($is_footer AND ($compteurFirstLevel % 5 == 4) && count($tab) - 1 != $compteurFirstLevel) $list .= '</ul><ul>';
			    if($currentLevel== 0) $compteurFirstLevel++;
			  }
			  
			  if($currentLevel > 0) $list .= '</ul>';
			  
			}
	   return $list;
	}
	
	private static function createBreadCrumb($table){
		$result = '<a href="page.php">Arborescence</a>';
		if(count($table) > 0){
			$cpt = 0;
			foreach($table as $key => $line){
				if((count($table) - 1) > $cpt)
					$result .= ' > <a href="page.php?idParent='.$line['idCategory'].'">'.$line['name'].'</a>';
				else 
					$result .= ' > '.$line['name'];
					
				$cpt ++;
			}
		}
		return $result;
	}
	
	private static function createBreadCrumbFront($table){
		$result = '<a href="'.PATH_HTTP.'">Accueil</a>';
		if(count($table) > 0){
			$cpt = 0;
			foreach($table as $key => $line){
				if((count($table) - 1) > $cpt)
					$result .= ' > <a href="'.PATH_HTTP.'page.php?idParent='.$line['idCategory'].'">'.$line['name'].'</a>';
				else 
					$result .= ' > '.$line['name'];
					
				$cpt ++;
			}
		}
		return $result;
	}
	
	
	/**
   * Initialize application (load data)
   */
  public static function initialize() {
    self::$_initialized = true;
  }

  /**
   * Check is application is initialized
   * @return boolean
   */
  public static function isInitialized() {
      return self::$_initialized;
  }
	
}


