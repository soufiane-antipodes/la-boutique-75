<?php
Class Collection{
	
	public static function add($releaseDate, $title, $titleFR, &$error){
		
		if(!preg_match('#^[0-9]{2}/[0-9]{2}/[0-9]{4}#', $releaseDate)){
			$error .='Date incorrecte'; return false;
		}
		else{
			list($day, $month, $year) = explode('/', $releaseDate);
			$releaseDate = $year.'-'.$month.'-'.$day;
		}
		
		if(Database :: countOf(BDD.'t_collection', 'nameFR="'.mysql_real_escape_string($title).'"') > 0){
			$error .= 'Le titre existe d�j�'; 
			return false; 
		}
		
		//START Transaction
		Database :: query('BEGIN;');
		
		//Insert New Film in BDD) 
		$result = Database :: query('insert into '.BDD.'t_collection(idCollection, nameEN, nameFR, position, releaseDate) values("", "'.mysql_real_escape_string($title).'", "'.mysql_real_escape_string($titleFR).'", 0, "'.mysql_real_escape_string($releaseDate).'")');
		$idCollection = Database :: getGeneratedId();
		
		Position :: generatePosition('t_collection', '', 'first');
		
		if(!file_exists(PATH_IMG_COLLECTION . Sanitize :: keepValidChars($titleFR))){
			mkdir(PATH_IMG_COLLECTION . Sanitize :: keepValidChars($titleFR));
		}
		
		//END Transaction
		Database :: query('COMMIT;');
		
		return $result;

	}
	
	public static function edit($releaseDate, $idCollection, $nameEN, $nameFR, &$error){
		$isExtension = null;
		
		if(!preg_match('#^[0-9]{2}/[0-9]{2}/[0-9]{4}#', $releaseDate)){
			$error .='Date incorrecte'; return false;
		}
		else{
			list($day, $month, $year) = explode('/', $releaseDate);
			$releaseDate = $year.'-'.$month.'-'.$day;
		}
		
		$collection = array();
		Database :: getLine('select idCollection, nameFR from '.BDD.'t_collection where idCollection="'.mysql_real_escape_string($idCollection).'"', $collection);
		if(count($collection) < 2) return false;
		
		$item = array();
		Database :: getLine('select C.idCollection, C.nameFR, I.idItem from '.BDD.'t_items I inner join '.BDD.'t_collection C on C.idCollection=I.idCollection where I.isHomePage=1 and C.idCollection="'.mysql_real_escape_string($idCollection).'"', $item);
		
		$idItem = $item['idItem'];
		
		//START Transaction
		Database :: query('BEGIN;');
		
		$existNewName = Database :: countOf(BDD.'t_collection', 'idCollection != "'.mysql_real_escape_string($idCollection).'" and nameFR="'.mysql_real_escape_string($nameFR).'"');
		if($existNewName > 0){
			$error .= 'Le titre existe d�j�'; 
			return false; 
		}
		
		if($collection['nameFR'] != $nameFR && file_exists(PATH_IMG_COLLECTION . Sanitize :: keepValidChars($collection['nameFR']))){
			rename(PATH_IMG_COLLECTION . Sanitize :: keepValidChars($collection['nameFR']), PATH_IMG_COLLECTION . Sanitize :: keepValidChars($nameFR));
		}
		
		$result = Database :: query('update '.BDD.'t_collection set nameEN="'.mysql_real_escape_string($nameEN).'", nameFR="'.mysql_real_escape_string($nameFR).'", releaseDate="'.mysql_real_escape_string($releaseDate).'" where idCollection="'.mysql_real_escape_string($idCollection).'"');
		
		//END Transaction
		Database :: query('COMMIT;');
		
		return $result;

	}
	
}


