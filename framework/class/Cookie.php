<?php
/**
 * Cookie Management
 *
 * @date        september 2011
 * @author      Julien Guion
 * @copyright   Julien Guion
 * @version     1
 */
class Cookie {

    private static $_initialized        = false;
    private static $_started            = false;
    private static $_expire							= null;

 
    /**
     * Check if a value has been setted in session
     * @param string $var
     * @param string $spaceName
     * @return boolean
     */   
    public static function exists($cookieName) {
    	if(!self::isInitialized()) self::initialize();
      return isset($_COOKIE[$cookieName]);
    }

    /**
     * Getter
     * @param string $var
     * @param string $spaceName
     * @return mixed
     */
    public static function get($cookieName) {
        if(!self::isInitialized()) self::initialize();
			return $_COOKIE[$cookieName];
    }

    /**
     * Setter
     * @param string $var
     * @param mixed $value
     * @param string $spaceName
     */
    public static function set($cookieName, $value) {
        if(!self::isInitialized()) self::initialize();
				setcookie($cookieName, $value, self::$_expire);
    }
    

    /**
     * Initialize application (load data)
     */
    public static function initialize() {
    	self::start();
      self::$_initialized = true;
    }

    /**
     * Check is application is initialized
     * @return boolean
     */
    public static function isInitialized() {
        return self::$_initialized;
    }

    /**
     * Start the session
     */
    public static function start() {
        if(!self::$_started) {
        		//Cache de 15 jours.
        		self::$_expire = time()+3600*24*15;
            self::$_started = true;
        }
    }

    /**
     * Destroy the cookie
     * @param string $cookieName
     */
    public static function destroy($cookieName) {
			
			// Finalement, on détruit la session.
			setcookie($cookieName);
    	self::$_initialized = false;
    	self::$_started = false;
    }
    
    /**
     * Destroy all cookies
     */
    public static function destroyAll(){
    	
    	if(isset($_COOKIE)){
    		foreach($_COOKIE as $key => $cookie){
    			if($key == 'PHPSESSID') continue;
    			setcookie($key);
    		}
    	}
    	
    	self::$_initialized = false;
    	self::$_started = false;
    }
    
}