<?php

/**
 * @class Database
 * 
 * Permet de faire des requêtes dans la base de données.
 * Recuperer/Inserer
 *
 * @date mars 2011
 * @author Julien Guion 
 * @copyright Julien Guion
 * @version 1
 */
class Database {
	
  public static $_db = null;
	public static $_host = DB_HOST;
	public static $_user = DB_USER;
	public static $_password = DB_PASSWORD;
	public static $_base = DB_BASE;
	public static $_nbRequest = 0;
	
	public static function verifyConnexion() {
		
		if(self :: $_db === null){
			self :: $_db = @mysql_connect(self :: $_host, self :: $_user , self :: $_password);	
			if (!self :: $_db)
				throw new Exception('Impossible de se connecter à la BDD : ' . mysql_error());
			else{
				$ok = mysql_select_db(self :: $_base, self :: $_db);
				if(!$ok)
					throw new Exception('Impossible de sélectionner la base : ' . self :: $base);
					
				mysql_set_charset('utf8');
			}
		}
		
	}
	
	private static function addLog($request) {
		
		$file = @fopen(PATH_FRAMEWORK_LOGS . 'mysql.log', 'a+');
		@fwrite($file, date('Y-d-m H:i:s') . ' a2w Intranet - ' . $request . ' - FILE : ' . $_SERVER['SCRIPT_NAME'] . ' (' . $_SERVER['REQUEST_URI'] . ') - Error: ' . mysql_error() . "\n\n");
		@fclose($file);	
		
	}
	
	public static function affectedRows() {
		
		return mysql_affected_rows(self :: $_db);
		
	}
  
  //::::::::::::::::::::::::::::::::::
  //::::: get count * of a table :::::
  public static function countOf($tableName, $where='') {
 
  	self :: $_nbRequest++;
	  self :: verifyConnexion();
	  $result = false;
	  
	  if (strlen($where)>0)
	  	$where = "where ".$where;
	  	
	  $request = 'select COUNT(*) from `'.$tableName.'` ' . $where;
    $result = mysql_query($request, self :: $_db);
    
    if ($result) {
      $row = mysql_fetch_array($result, MYSQL_NUM);
      $result = $row[0];
    } 
    else {
			self :: addLog($query);	 
    }
    
    return $result;
    
  }
  
  //::::::::::::::::::::::::
  //::::: single query :::::
  public static function query($request) {
 
  	self :: $_nbRequest++;
	  self :: verifyConnexion();
    
	  $result = mysql_query($request, self :: $_db);
		if (!$result) {
			self :: addLog($request);
		}
	
	 return $result;    
    
  }
  
  //::::::::::::::::::::::::::::::::::::::::::::
  //::::: parse the element and returns it :::::
  public static function get($request) {
  	
  	self :: $_nbRequest++;
	  self :: verifyConnexion();
	  $result = false;
	  
    $result = mysql_query($request,self :: $_db);
    if ($result) {
      $row = mysql_fetch_array($result,MYSQL_NUM);
      $result = $row[0];
    } 
    else {
			self :: addLog($request);
		}
		
		return $result;
  }
    
  //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  //::::: parse the first line and returns it as a table like $table['nameOfField']="" :::::
  public static function getLine($request,&$tableToReturn) {
  	
  	self :: $_nbRequest++;
	  self :: verifyConnexion();
	  $result = false;
	  
    $result = mysql_query($request,self :: $_db);
    if ($result) { 
	    $tableToReturn = mysql_fetch_array($result,MYSQL_ASSOC);
	    $result = mysql_num_rows($result); 
	  } 
	  else {
			self :: addLog($request);	
		}
		
		return $result;
	
  }
  
  //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
  //::::: Parse all results and returns it to a table like $table[lineNumber]['nameOfFile'] :::::
  //::::: Returns the number of lines ......................................................:::::
  
  public static function getTable($request,&$tableToReturn) {
		self :: $_nbRequest++;
  	self :: verifyConnexion();
  	$result = false;
  	
    $dbResult = mysql_query($request, self :: $_db);
    if ($dbResult) {
      $result = mysql_num_rows($dbResult);
      $i=0;
      while ($row = mysql_fetch_assoc($dbResult)) {
        $tableToReturn[$i]=$row;
        $i++;
      }
    } 
    else {
	 		self :: addLog($request);   
		}

    return $result;
  }
  
	//::::: returns a 2 dimensions table, where the first level is created by the $IDorder column of the request :::::
  public static function getOrderedTable($request,$IDorder,&$tableToReturn) {
  	self :: $_nbRequest++;
	  self :: verifyConnexion();
	  $result = false;
	  
    $dbResult = mysql_query($request, self :: $_db);
    if ($dbResult) {
      $result = mysql_num_rows($dbResult);
      while ($row = mysql_fetch_assoc($dbResult)) {
	    foreach ($row as $key=>$value) $tableToReturn[$row[$IDorder]][$key]=$value;
      }
    } 
    else {
    	self :: addLog($request);
		}
		
    return $result;
  }
	
	//::::: returns a one dimension table, where index is the firstColumn result and contains is the second column :::::
	//::::: ie : select codeIso,country from country = array['FR']='France'
	public static function getSimpleArray($request,&$tableToReturn) {
		self :: $_nbRequest++;
	  self :: verifyConnexion();
	  $result = false;
	  
	  $dbResult = mysql_query($request,self :: $_db);
    if ($dbResult) {
      $result = mysql_num_rows($dbResult);
      while ($row = mysql_fetch_array($dbResult,MYSQL_NUM)) {
        $tableToReturn[$row[0]]=$row[1];
      }
    } 
    else {
			self :: addLog($request);
		}

    return $result;
	}
	
	//::::: returns a multidimensional table :::::
	public static function getMultidimensionalArray($request,$columns,&$table) {
   
    self :: getTable($request, $sqlTable);
    $toEvaluate = "";
    foreach($columns as $column) $toEvaluate .= '[$element[\''.$column.'\']]';
    $toReturn = array();

  if( is_array($sqlTable) ) {
    	foreach ($sqlTable as $element) eval('$toReturn'.$toEvaluate.' = $element;');
    }
    
    $table = $toReturn;

    return count($table);
    
  }	
	
	//::::: returns a one dimension table into an array var :::::
	//::::: request must be something like : select country from tCountry :::::
	public static function getOneColumn($request,&$tableToReturn,$resetTable=true) {
		self :: $_nbRequest++;
    self :: verifyConnexion();
    $result = false;
    
	  $dbResult = mysql_query($request, self :: $_db);
		if (($resetTable) || (!is_array($tableToReturn))) $tableToReturn = array();
    if ($dbResult) {
      $result = mysql_num_rows($dbResult);
      while ($row = mysql_fetch_array($dbResult,MYSQL_NUM)) {
			  array_push($tableToReturn,$row[0]);
      }
    } 
    else {
 			self :: addLog($request);
		}
    return $result;
	}
	
	//::::: returns the last generated Id of the last request :::::
	public static function getGeneratedId() {
		
		self :: $_nbRequest++;
		return @mysql_insert_id(self :: $_db);
		
	}
	
	public static function getEnum($tableName, $field){
		
		$enum = array();
		$request = 'SHOW COLUMNS FROM `'.$tableName.'` where field="'.$field.'"';
		self :: getLine($request, $enum);
		$enum = $enum['Type'];
		$enum = substr($enum, 5, -1);
		$enum = str_replace("'", '', $enum);
		$enum = explode(',', $enum);
		
		return $enum;
	}
	
	
	public static function getNbRequest(){
		return self :: $_nbRequest;
	}
    
  //:::::::::::::::::
	//::::: close :::::
	public static function close() {
	  mysql_close(self :: $_db);
		self :: $_db = null;
	}
}

?>