<?php

/**
* Class Date
*
* Permet de manipuler les dates
*
* @author Julien Guion
* @version 1.0
*/
class Date{
	
	/**
	* Recupere la date formatter
	*
	* @param string $date -> format SQL
	* @param string $format
	* @return string date
	*/
	public static function getFrSqlDate($date, $format){
		
		$year = substr($date, 0, 4);
		$month = substr($date, 5, 2);
		$day = substr($date, 8, 2);
		
		$hour = substr($date, 11, 2);
		$min = substr($date, 14, 2);
		$second = substr($date, 17, 2);
		
		return date($format, mktime($hour, $min, $second, $month, $day, $year));
	}
	
	public static function transformEnToFr($date){
		
		$day = array('Mon' => 'Lundi', 'Tue' => 'Mardi', 'Wed' => 'Mercredi', 'Thu' => 'Jeudi', 'Fri' => 'Vendredi', 'Sat' => 'Samedi', 'Sun' => 'Dimanche');
		$month = array('January' => 'Janvier', 'February' => 'Février', 'March' => 'Mars', 'April' => 'Avril', 'May' => 'Mai', 'June' => 'Juin', 'July' => 'Juillet', 'August' => 'Août', 'September' => 'Septembre', 'October' => 'Octobre', 'November' => 'Novembre', 'December' => 'Décembre');
		
		$date		 =		 strtr($date, $day);
		$date		 =		 strtr($date, $month);
		return $date;
	}
	
	/**
	* Recupere le timestamp
	*
	* @param string $date -> format SQL
	* @return string timestamp
	*/
	public static function getTimestamp($date){
		return mktime(self :: getHours($date), self :: getMinutes($date), self :: getSeconds($date), self :: getMonth($date), self :: getDay($date), self :: getYear($date));
	}
	
	public static function getSeconds($date){
		return self :: getFrSqlDate($date, 's');
	}
	
	public static function getMinutes($date){
		return self :: getFrSqlDate($date, 'i');
	}
	
	public static function getHours($date){
		return self :: getFrSqlDate($date, 'H');
	}
	
	public static function getDay($date){
		return self :: getFrSqlDate($date, 'd');
	}
	
	public static function getMonth($date){
		return self :: getFrSqlDate($date, 'm');
	}
	
	public static function getMonthWithoutZero($date){
		return self :: getFrSqlDate($date, 'n');
	}
	
	public static function getYear($date){
		return self :: getFrSqlDate($date, 'Y');
	}
	
	public static function getDayOfWeek($date){
		return self :: getFrSqlDate($date, 'N');
	}
	
	public static function getNbDaysByMonth($date){
		return date('t', mktime(self :: getHours($date), self :: getMinutes($date), self :: getSeconds($date), self :: getMonth($date), self :: getDay($date), self :: getYear($date)));
	}
	
	public static function getFirstDayOfMonth($date){
		return date('N', mktime(self :: getHours($date), self :: getMinutes($date), self :: getSeconds($date), self :: getMonth($date), 1, self :: getYear($date)));
	}
	
	public static function getFirstDateOfMonth($date){
		return date('Y-m-d H:i:s', mktime(0, 0, 0, self :: getMonth($date), 1, self :: getYear($date)));
	}
	
	public static function getLastDateOfMonth($date){
		return date('Y-m-d H:i:s', mktime(23, 59, 59, self :: getMonth($date), self :: getNbDaysByMonth($date), self :: getYear($date)));
	}
	
	public static function getFirstDayOfWeek($date, $format){
		$firstDayOfWeek = self :: getDayOfWeek($date);
		
		if($firstDayOfWeek - 1 > 0)
			$result = date($format, mktime(0, 0, 0, self :: getMonth($date), (self :: getDay($date) - ($firstDayOfWeek - 1)), self :: getYear($date)));
		else 
			$result = date($format, mktime(0, 0, 0, self :: getMonth($date), self :: getDay($date), self :: getYear($date)));
			
		return $result;
	}
	
	public static function getLastDayOfWeek($date, $format){
		
		$lastDayOfWeek = self :: getDayOfWeek($date);
		
		if(7 - $lastDayOfWeek > 0)
			$result = date($format, mktime(23, 59, 59, self :: getMonth($date), (self :: getDay($date) + (7 - $lastDayOfWeek)), self :: getYear($date)));
		else 
			$result = date($format, mktime(23, 59, 59, self :: getMonth($date), self :: getDay($date), self :: getYear($date)));
			
		return $result;
	}
	
	public static function getAddDate($date, $addDay, $format){
		return date($format, mktime(self :: getHours($date), self :: getMinutes($date), self :: getSeconds($date), self :: getMonth($date), (self :: getDay($date) + $addDay), self :: getYear($date)));
	}
	
	public static function getMonthDate($date, $addMonth, $format){
		return date($format, mktime(self :: getHours($date), self :: getMinutes($date), self :: getSeconds($date), (self :: getMonth($date) + $addMonth), 15, self :: getYear($date)));
	}
	
	public static function changeMinutes($date, $change, $format){
		return date($format, mktime(self :: getHours($date), (self :: getMinutes($date) + $change), self :: getSeconds($date), self :: getMonth($date), self :: getDay($date), self :: getYear($date)));
	}
	
	public static function changeHours($date, $change, $format){
		return date($format, mktime((self :: getHours($date) + $change), self :: getMinutes($date), self :: getSeconds($date), self :: getMonth($date), self :: getDay($date), self :: getYear($date)));
	}
	
	public static function changeDay($date, $change, $format){
		return date($format, mktime(self :: getHours($date), self :: getMinutes($date), self :: getSeconds($date), self :: getMonth($date), (self :: getDay($date) + $change), self :: getYear($date)));
	}
	
	public static function getWeek($date, $add, $format){
		return date($format, mktime(self :: getHours($date), self :: getMinutes($date), self :: getSeconds($date), self :: getMonth($date), (self :: getDay($date) + (7*$add)), self :: getYear($date)));
	}
	
}