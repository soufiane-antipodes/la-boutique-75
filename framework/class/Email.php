<?php	
/**
 * @class Mail
 * @brief Class allows to manage Mail 
 *
 * @package     Core
 * @date        Mars 2011
 * @author      Julien Guion
 * @copyright   BMX Family
 * @version     1
 */
class Email 
{
	const TEXT_FORMAT = 'text';
	const HTML_FORMAT = 'html';
	
	protected $_from        	= null;
	protected $_to           	= null;
	protected $_subject      	= null;
	protected $_body         	= null;
	protected $_content			= array();
	protected $_attachments		= array();
	protected $_headers      	= array();
  protected $_headerSeparator = "\r\n";


	//*************************************************************************************************
	//**
	//** FROM / TO
	//**
	//*************************************************************************************************	
	/**
   	 * Get current From
     * @return string
     */
    public function getFrom() 
    {
    	return $this->_from;
    }

    /**
     * Get current To
     * @return string
     */
    public function getTo() 
    {
        return explode(';', $this->_to);
    }
    
	/**
     * Set the current From
     * @param string $from
     * @return object $this
     */
    public function setFrom($from) 
    {
        $this->_from = $from;
        return $this;
    } 
		
	/**
     * Set the current To
     * @param string $to
     * @return object $this
     */
    public function setTo($to) 
    {
        $this->_to =$to;
        return $this;
    }
    
	//*************************************************************************************************
	//**
	//** MAIL CONTENT
	//**
	//*************************************************************************************************	
	/**
     * Get current Subject
     * @return string
     */
    public function getSubject() 
    {
        return $this->_subject;
    }
    
	/**
     * Get current Body
     * @return string
     */
    public function getBody() 
    {
        return $this->_body;
    }
         
    /**
     * Set the current Subject
     * @param string $subject
     * @return object $this
     */
    public function setSubject($subject) 
    {
        $this->_subject = $subject;
        return $this;
    }     
 
	/**
     * Set the current Body
     * @param string $body
     * @return object $this
     */
    public function setBody($body) 
    {
        $this->_body = $body;
        return $this;
    }		
    
	//*************************************************************************************************
	//**
	//** ATTACHMENTS
	//**
	//*************************************************************************************************    
	/**
     * Return the list of mail attachments
     * @return array
     */	  
	public function getAttachments()
	{
		return $this->_attachments;	
	}
	
	/**
     * Add one or more file to the mail
     * @return $this
     */		
	public function addAttachments($files)   
	{
		$this->_attachments = array_merge($this->_attachments, (array)$files);
		$this->_attachments = array_unique($this->_attachments);
		return $this;	
	}
	   
    /**
     * encodeAttachment function.
     * @access public
     * @param mixed $file
     * @return void
     */
    protected function encodeAttachment($file)
    {
    	$source = file_get_contents($file);
		$source = chunk_split(base64_encode($source));
		return $source;
    }
	
    
	//*************************************************************************************************
	//**
	//** HEADERS
	//**
	//************************************************************************************************* 
    /**
     * Get current(s) Headers
     * @return string
     */
    public function getHeaders() 
    {
        return $this->_headers;
    }    
      
	/**
     * Set the current(s) Headers
     * @param array $headers
     * @return object $this
     */
    public function setHeaders($headers) 
    {
        $this->_headers = $headers;
        return $this;
    }

	/**
     * Add datas in the header's aray
     * @param array $headers
     * @return object $this
     */         
    public function addHeaders($headers) 
  	{
	     foreach($headers as $key=>$value)
	     {
	     	$this->_headers[$key]= $value;
	     }
		return $this;
    }
    
    /**
     * Get the header separator
     * @return object $this
     */
    public function getHeaderSeparator() 
    {
        return $this->_headerSeparator;
    }
    
	/**
     * Set the the header separator
     * @param string $headerSeparator
     * @return object $this
     */
    public function setHeaderSeparator($headerSeparator) 
    {
        $this->_headerSeparator = $headerSeparator;
    }
    
	/**
     * Parse informations in the header's array
     * @return array
     */
    protected function getHeadersToSend()
    {
        $headers = array();
        
        //headers added
        foreach($this->_headers as $key => $value)
        {
            $headers[] = $key.': ' .$value;
        }
        
        if($this->getFrom() !== null){
        	$headers[] = 'From: '.$this->_from;
        }
        
        return implode($this->_headerSeparator, $headers).$this->_headerSeparator;
    }
    
	//*************************************************************************************************
	//**
	//** SENDING METHODS
	//**
	//************************************************************************************************* 	
	/**
     * Determine if the mail is multipart
     * @return boolean
     */		
	public function isMultipart()
	{
		return !empty($this->_attachments);
	}
	
	/**
     * Send Mail
     * @param string $format
     * @return boolean
     */
    public function send($format = self::HTML_FORMAT)
    {
    	$this->_content = array();
    	
    	$this->addHeaders(array('MIME-Version'=>'1.0'));
    	
    	//attachments => multipart message
    	if($this->isMultipart()) 
    	{
    		$boundary = self::generateBoundary();
    		
	 		$this->addHeaders(array('Content-type'=>'multipart/mixed; boundary="'.$boundary.'"'));	
			$this->addContent('This is a multi-part message in MIME format.'."\n\n"); 
			
   	
	    	//mail content part
	    	switch($format)
	    	{
	    		case self::HTML_FORMAT:
	    			$this -> addContent('--'.$boundary)
						  -> addContent('Content-Type: text/html; charset="utf-8"')
						  -> addContent('Content-Transfer-Encoding: 8bit')
						  -> addContent();		
	    			break;
	    			
	    		case self::TEXT_FORMAT:
	    			$this -> addContent('--'.$boundary)
						  -> addContent('Content-Type: text/plain; charset="utf-8"')
						  -> addContent('Content-Transfer-Encoding: 8bit')
						  -> addContent();
	    			break;
	    	}   		
    	}
    	//one part
    	else
    	{
    		$this->addHeaders(array("Content-type"=>'text/html; charset="utf-8"'));	
    	}
    	
    	//inserting body
    	$this->addContent($this->getBody());
 
 
    	//attachments
    	if(!empty($this->_attachments))
    	{
    		$this -> addContent('--'.$boundary);
    		
	    	foreach($this->_attachments as $attachment)
	    	{
				$fileContent = $this->encodeAttachment($attachment);
				$filename    = basename($attachment);
				$extension   = explode(".", $filename);
				
				$this	-> addContent('--'.$boundary)
						-> addContent('Content-Type: application/'.$extension[1].'; name="'.$filename.'"')
						-> addContent('Content-Transfer-Encoding: base64')
						-> addContent('Content-Disposition: attachment; filename="'.$filename.'"')
						-> addContent()
						-> addContent($fileContent)
						-> addContent()
						-> addContent('--'.$boundary)
						-> addContent();
						
	    	}
	    	
	    }
	    				    	
    	return $this->generalSend();
    }	
    
 	/**
     * Add some content to the final mail content
     * @param string $content
     * @return $this
     */
	protected function addContent($content = '')
	{
		$this->_content[] = $content."\n";
		return $this;
	}   
	
	/**
	 * Send the mail
	 * @access public
	 * @return boolean
	 */
	protected function generalSend()
	{
			
	    if(count($this->getTo()) == 0 || $this->getSubject() == null || $this->getBody() == null)
	    {
	        return false;
	    }
	
			$result = false;
	    $headers = $this->getHeadersToSend();
	    $subject = $this->getSubject();
	    $to 	 = $this->getTo();
	    $body 	 = implode($this->_content);
    	
	    //Gérer l'erreur problable
	    foreach($to as $dest){
	    	
	   		$ok =  mail($dest, '=?UTF-8?B?'.base64_encode(Sanitize :: unEscapeHtml($subject)).'?=', $body, $headers);
	   		if($ok != true){
	   			$result = false;
	   			break;
	   		}
	   		else
	   			$result = true;
	   	}
	   	
	   	return $result;
	}
	
	
	//*************************************************************************************************
	//**
	//** ENGINE
	//**
	//************************************************************************************************* 	
	/**
	 * Generate a boundary id
	 * @return string
	 */
	protected static function generateBoundary()
	{
		return md5(uniqid(rand(),true));
	}
	
}	
	 