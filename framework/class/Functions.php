<?php

/**
* Cree une miniature
*
*	@param string $full_path
*	@param string $file
*	@param string $thumb_path
*	@param string $thumb_prefix
*	@param integer $width
*	@param integer $height
*	@return boolean
*/
function CreateThumb($full_path,$file,$thumb_path,$thumb_prefix,$width=96,$height=72, $maxWidth=null) {
	
    if (!is_readable($full_path.$file)) {
        echo '<br>fichier '.$full_path.$file.' non accessible';
        return false;
    }
    
    switch (strtolower(substr($file,strlen($file)-3,3))) {
        case 'jpg':
            $type = 'jpg';
            $source = @imagecreatefromjpeg($full_path.$file);
            if(!$source) return false;
            break;
        case 'png':
            $type = 'png';
            $source = @imagecreatefrompng($full_path.$file);
            if(!$source) return false;
            break;
        case 'gif':
            return true;
            break;
        default:
            // format inconnu
            echo '<br>format inconnu';
            return false;
    }
 
    $source_width = @imagesx($source);
    $source_height = @imagesy($source);
    
 		//keep ratio
 		if(empty($width)){
 			$width = $source_width * ($height / $source_height);
 		}
 		else if(empty($height)){
 			$height = $source_height * ($width / $source_width);
 		}
 		
 		$destination_width = $width;
    $destination_height = $width*$source_height/$source_width;
 		
    // On crée la miniature vide
    if (!$destination = @imagecreatetruecolor($width, $height)) {
        echo '<br>impossible de creer miniature';
        return false;
    }
 		
 		//Draw background
 		$white = @imagecolorallocate($destination, 255, 255, 255);
		@imagefilledrectangle($destination, 0, 0, $width, $height, $white);
 		
 		$posY = 0;
 		
 		if(round($destination_height) < round($height)){
 			$posY = $height - $destination_height;
 			$posY = round($posY/2);
 		}
 		
 		if($posY < 0) $posY = 0;
 		
    if (!@imagecopyresampled($destination, $source, 0, $posY, 0, 0, $destination_width, $destination_height, $source_width, $source_height)) {
        echo '<br>erreur sur imagecopyresampled';
        return false;
    }
    
    switch ($type) {
        case 'jpg':
            if (!@imagejpeg($destination, $thumb_path.$thumb_prefix.$file, 100)) {
                echo '<br>erreur sur imagejpeg';
                return false;
            }
            break;
        case 'png':
            if (!@imagepng($destination, $thumb_path.$thumb_prefix.$file, 9)) {
                echo '<br>erreur sur imagepng';
                return false;
            }
            break;
        default:
            // format inconnu
            echo '<br>format inconnu 2';
            return false;
    }
    return true;
}

/**
	* Cree une pagination des pages
	*
	*	@param integer $current_page
	*	@param integer $nb_pages
	*	@param string $link
	*	@param integer $around
	*	@param integer $firstlast
	*	@return string $pagination
	*/
function pagination($current_page, $nb_pages, $link='?page=%d', $around=3, $firstlast=1)
{
	$pagination = '';
	$link = preg_replace('`%([^d])`', '%%$1', $link);
	if ( !preg_match('`(?<!%)%d`', $link) ) $link .= '%d';
	if ( $nb_pages > 1 ) {

		// Lien précédent
		if ( $current_page > 1 )
			$pagination .= '<a class="prevnext" href="'.sprintf($link, $current_page-1).'"><< Précédent</a>';
		else
			$pagination .= '<span class="prevnext disabled"><< Précédent</span>';

		// Lien(s) début
		for ( $i=1 ; $i<=$firstlast ; $i++ ) {
			$pagination .= ' ';
			$pagination .= ($current_page==$i) ? '<span class="current">'.$i.'</span>' : '<a href="'.sprintf($link, $i).'">'.$i.'</a>';
		}

		// ... après pages début ?
		if ( ($current_page-$around) > $firstlast+1 )
			$pagination .= ' &hellip;';

		// On boucle autour de la page courante
		$start = ($current_page-$around)>$firstlast ? $current_page-$around : $firstlast+1;
		$end = ($current_page+$around)<=($nb_pages-$firstlast) ? $current_page+$around : $nb_pages-$firstlast;
		for ( $i=$start ; $i<=$end ; $i++ ) {
			$pagination .= ' ';
			if ( $i==$current_page )
				$pagination .= '<span class="current">'.$i.'</span>';
			else
				$pagination .= '<a href="'.sprintf($link, $i).'">'.$i.'</a>';
		}

		// ... avant page nb_pages ?
		if ( ($current_page+$around) < $nb_pages-$firstlast )
			$pagination .= ' &hellip;';

		// Lien(s) fin
		$start = $nb_pages-$firstlast+1;
		if( $start <= $firstlast ) $start = $firstlast+1;
		for ( $i=$start ; $i<=$nb_pages ; $i++ ) {
			$pagination .= ' ';
			$pagination .= ($current_page==$i) ? '<span class="current">'.$i.'</span>' : '<a href="'.sprintf($link, $i).'">'.$i.'</a>';
		}

		// Lien suivant
		if ( $current_page < $nb_pages )
			$pagination .= ' <a class="prevnext" href="'.sprintf($link, ($current_page+1)).'" title="Page suivante">Suivant >></a>';
		else
			$pagination .= ' <span class="prevnext disabled">Suivant >></span>';
	}
	return $pagination;
}

/**
	* Supprime un repertoire
	*
	*	@param string $tmp_path
	*	@return boolean
	*/
function delete_folder($tmp_path){
	if(is_dir($tmp_path)){
	  if(!is_writeable($tmp_path) && is_dir($tmp_path)){chmod($tmp_path,0777);}
	    $handle = opendir($tmp_path);
	  while($tmp=readdir($handle)){
	    if($tmp!='..' && $tmp!='.' && $tmp!=''){
	         if(is_writeable($tmp_path.'/'.$tmp) && is_file($tmp_path.'/'.$tmp)){
	                 unlink($tmp_path.'/'.$tmp);
	         }elseif(!is_writeable($tmp_path.'/'.$tmp) && is_file($tmp_path.'/'.$tmp)){
	             chmod($tmp_path.'/'.$tmp,0666);
	             unlink($tmp_path.'/'.$tmp);
	         }
	        
	         if(is_writeable($tmp_path.'/'.$tmp) && is_dir($tmp_path.'/'.$tmp)){
	                delete_folder($tmp_path.'/'.$tmp);
	         }elseif(!is_writeable($tmp_path.'/'.$tmp) && is_dir($tmp_path.'/'.$tmp)){
	                chmod($tmp_path.'/'.$tmp,0777);
	                delete_folder($tmp_path.'/'.$tmp);
	         }
	    }
	  }
	  closedir($handle);
	  rmdir($tmp_path);
	  return true;
	}
} 


function trace($value){

	echo '<pre>';
		if(is_array($value))
			print_r($value);
		else
			echo $value;
			
	echo '</pre>';

}

function regroupArrayBy($aArray, $sValueToRegroupWith, $mValueToPreserve = false)
{
	$aResult = array();
	foreach ($aArray as $iKey => $sValue) 
	{
		// On preserve la clé de regroupement avant destruction
		$sPreservedValue = $sValue[$sValueToRegroupWith];
		// On détruit la clé et sa valeur du tableau initial pour éviter les redondances
		unset($sValue[$sValueToRegroupWith]);
		// On regroupe puis on determine si il y a une clé qu'on preserve ou bien si on garde la totalité du tableau interne
		$aResult[$sPreservedValue][] = (!$mValueToPreserve) ? $sValue : $sValue[$mValueToPreserve];
	}
	return $aResult;
}
function mergeArrayMenus($aMenus,$aSubMenus) 
{
	$aFinalMenu = array();
	foreach ($aMenus as $iKey => $sValue) 
    {
    	$aFinalMenu[$iKey][] = $sValue;
    	foreach ($aSubMenus as $iSubKey => $sSubValue) 
    	{
    		if ( $sSubValue['idSubPage'] === $sValue['id'] )
    		{
    			$aFinalMenu[$iKey][$iSubKey] = $sSubValue;
    		}
    	}
    }

    return $aFinalMenu;
}

function getMenuFamily($aMenus)
{
	$aFinalMenu = array();
	$iCount = 0;
	
	foreach ($aMenus as $__iKey => $__sValue)
	{
		$aFinalMenu[$iCount] = array();
		// Ne contient pas de sous menu
		if ( count($__sValue) === 1 )
		{
			foreach ($__sValue as $_iKey => $_sValue) 
			{
				foreach ($_sValue as $iKey => $sValue) 
				{
					$aFinalMenu[$iCount][$iKey] = $sValue;
				}
			}
		}
		// Contient un sous menu, on prend le premier
		else
		{
			$iSubCount = 0;
			foreach ($__sValue as $_iKey => $_sValue) 
			{
				foreach ($_sValue as $iKey => $sValue) 
				{
					// On saute le menu parent
					if ( $iSubCount === 0 ) continue;
					// Si on arrive à une deuxieme sous page on sort de la boucle
					if ( $iSubCount > 1 ) break;
					
					$aFinalMenu[$iCount][$iKey] = $sValue;
					$aFinalMenu[$iCount]['parent_id'] = $__sValue[0]['id'];
					$aFinalMenu[$iCount]['parent_name'] = $__sValue[0]['name'];
				}

				$iSubCount++;
			}	
		}
		$iCount++;
	}

	return $aFinalMenu;
}

function outputCSV($aData, $aEntete) 
{
	$fStream = fopen('php://output', 'w');
	foreach ($aData as $iKey => $sVal) 
	{
	    if ( $iKey === 0 ) 
	    	fputcsv($fStream, $aEntete, ';');
	    fputcsv($fStream, $sVal, ';', '"');
	}
	fclose($fStream);
}

/*
	Retourne le chemin dynamique de l'article en question
	Input : 	Int 		: ID de l'article
				sName 		: Le nom de l'article
				beforeURL 	: Le nom de la catégorie (Blog, article, page...) Selon .htaccess
	Output : 	String 		: / *beforeURL* / *ID* - *sName*
*/
function buildUpURL($ID, $sName, $beforeURL = null)
{
	$sUrl = $ID . '-' . Sanitize :: keepValidCharsRewriting($sName); 
	$sUrl = str_replace('_', '-', $sUrl);

	return ( $beforeURL !== null ) ? '/' . $beforeURL . '/' . $sUrl : $sUrl;

}

/*
 Retourne l'URL de base avec la langue en question : 
 Input :  String  : La langue en question
    Bool  : True  = Construction de l'URL pour l'admin
        False = Construction de l'URL pour le Front

 Output :  String  : Front = *BASE_URL* / *Langue* /
        Admin = *BASE_URL*?langAdmin=*Langue*
*/
function buildLangURL($sLang, $bAdmin = false, $sDefaultLang = 'fr')
{
	// Front
	if ( !$bAdmin )
	{
	  	$aLangsUrl = array();
		// Suppression des langues de l'URL courante
		foreach (unserialize(EXISTING_LANGUAGES) as $iKey => $sValue) { array_push($aLangsUrl, '/' . $sValue); }
		$sUrl = str_replace($aLangsUrl, '', $_SERVER['REQUEST_URI']);
		return '/' . $sLang . $sUrl;
	}
	// Admin
	else 
	{	
		// Il y a déjà un paramètre dans l'URL
		if ( strpos($_SERVER['REQUEST_URI'], '?') !== false ){
			/* 
			Il y a déjà une langue dans l'URL, on remplace la langue
			Sinon on rajoute le paramètre de langue
			*/
			$sUrl = $_SERVER['REQUEST_URI'];
			return ( strpos($sUrl, 'langAdmin' ) != false ) ? preg_replace('/langAdmin=[a-zA-Z]{2,3}/', 'langAdmin=' . $sLang, $sUrl) : $sUrl . '&amp;langAdmin=' . $sLang;
		}else{
			// Pas de paramètres !
			return $_SERVER['REQUEST_URI'] . '?langAdmin=' . $sLang;
		}
	}

}



/*
	Retourne la langue actuelle utilisée :  
	Input : 	String 	: La langue en question
				Vide 	: Par défaut en FR
	Output : 	String 	: *lang*
*/
function getActualLanguage($sDefaultLang = 'fr')
{
	return (Session :: get('langToUse') !== $sDefaultLang) ? '/' . Session :: get('langToUse') : null;
}

/*
	Execute la requête en $sQuery, dans le cas où c'est vide avec la langue actuelle, on renvoie la requête avec la langue par defaut  
	Input : 	String 	: Requête SQL
				Array 	: 
							type 		  : Type de la requete à executer (getLine, getTable, getOrderedTable...)
							orderArrayBy  : Le champ avec lequel on ordonne le résultat
	Output : 	String 	: *lang*
*/
function fetchElementByLang($sQuery, $aOptions, $sDefaultLang = 'fr')
{
	
	// Nous vérifion si nous avons un type de fetch
	if ( !isset($aOptions['type']) ) return array();

	// Instanciation du résultat
	$aArray = array();
	// Execution de la requête
	if ( isset($aOptions['orderArrayBy']) ) 
		Database :: $aOptions['type']($sQuery, $aOptions['orderArrayBy'], $aArray);
	else
		Database :: $aOptions['type']($sQuery, $aArray);

	// Si le résultat est trouvé avec la langue actuelle on retourne le résultat
	if ( $aArray && count($aArray) > 0 ) return $aArray;

	// Sinon on modifie la requête et on retourne le résultat avec la langue par défaut
	$sQuery = preg_replace('/lang(| )=.*(\'|")/', 'lang = \'' . $sDefaultLang . '\'', $sQuery);



	// Execution de la requête
	if ( isset($aOptions['orderArrayBy']) ) 
		Database :: $aOptions['type']($sQuery, $aOptions['orderArrayBy'], $aArray);
	else
		Database :: $aOptions['type']($sQuery, $aArray);

	return $aArray;

}
?>