<?php

/**
* Cree une miniature
*
*	@param string $full_path
*	@param string $file
*	@param string $thumb_path
*	@param string $thumb_prefix
*	@param integer $width
*	@param integer $height
*	@return boolean
*/
function CreateThumb($full_path,$file,$thumb_path,$thumb_prefix,$width=96,$height=72, $maxWidth=null, $maxHeight=null) {
	
    if (!is_readable($full_path.$file)) {
        echo '<br>fichier '.$full_path.$file.' non accessible';
        return false;
    }
    
    switch (strtolower(substr($file,strlen($file)-3,3))) {
        case 'jpg':
            $type = 'jpg';
            $source = @imagecreatefromjpeg($full_path.$file);
            if(!$source) return false;
            break;
        case 'png':
            $type = 'png';
            $source = @imagecreatefrompng($full_path.$file);
            if(!$source) return false;
            break;
        case 'gif':
            return true;
            break;
        default:
            // format inconnu
            echo '<br>format inconnu';
            return false;
    }
 
    $source_width = @imagesx($source);
    $source_height = @imagesy($source);
    
 		//keep ratio
 		if(empty($width)){
 			$width = $source_width * ($height / $source_height);
 		}
 		else if(empty($height) || is_null($height)){
 			$height = round($source_height * ($width / $source_width));
 		}
 		
 		if($maxHeight !== null && $height > $maxHeight){
 			$height = $maxHeight;
 		}
 		
 		$destination_width = $width;
    $destination_height = $width*$source_height/$source_width;
 		
    // On crée la miniature vide
    if (!$destination = @imagecreatetruecolor($width, $height)) {
        echo '<br>impossible de creer miniature';
        return false;
    }
 		
 		//Draw background
 		$white = @imagecolorallocate($destination, 255, 255, 255);
		@imagefilledrectangle($destination, 0, 0, $width, $height, $white);
		
 		$posY = 0;
 		
 		if(round($destination_height) < round($height)){
 			$posY = $height - $destination_height;
 			$posY = round($posY/2);
 		}
 		
 		if($posY < 0) $posY = 0;
 		
 		$posX = 0;
 		
 		if(round($destination_height) > round($height)){
 			
 			$destination_height = $height;
 			$destination_width = $destination_height * ($source_width / $source_height);
 			
 			$posX = $width - $destination_width;
 			$posX = round($posX/2);
 			if($posX < 0) $posX = 0;
 		}
 		
 		/*
 		trace('maxHeight : '.$maxHeight);
 		trace('posX : '.$posX);
 		trace('posY : '.$posY);
 		trace('destination_width : '.$destination_width);
 		trace('destination_height : '.$destination_height);
 		trace('source_width : '.$source_width);
 		trace('source_height : '.$source_height);
 		trace('width : '.$width);
 		trace('height : '.$height);
 		trace('thumb_path : '.$thumb_path);
 		trace('thumb_prefix : '.$thumb_prefix);
 		trace('file : '.$file);
 		*/
 		
 		
    if (!@imagecopyresampled($destination, $source, $posX, $posY, 0, 0, $destination_width, $destination_height, $source_width, $source_height)) {
        echo '<br>erreur sur imagecopyresampled';
        return false;
    }
    
    switch ($type) {
        case 'jpg':
            if (!@imagejpeg($destination, $thumb_path.$thumb_prefix.$file, 100)) {
                //echo '<br>erreur sur imagejpeg';
                return false;
            }
            break;
        case 'png':
            if (!@imagepng($destination, $thumb_path.$thumb_prefix.$file, 9)) {
                //echo '<br>erreur sur imagepng';
                return false;
            }
            break;
        default:
            // format inconnu
            echo '<br>format inconnu 2';
            return false;
    }
    return true;
}

/**
	* Cree une pagination des pages
	*
	*	@param integer $current_page
	*	@param integer $nb_pages
	*	@param string $link
	*	@param integer $around
	*	@param integer $firstlast
	*	@return string $pagination
	*/
function pagination($current_page, $nb_pages, $link='?page=%d', $around=3, $firstlast=1)
{
	$pagination = '';
	$link = preg_replace('`%([^d])`', '%%$1', $link);
	if ( !preg_match('`(?<!%)%d`', $link) ) $link .= '%d';
	if ( $nb_pages > 1 ) {

		// Lien précédent
		if ( $current_page > 1 )
			$pagination .= '<a class="prevnext" href="'.sprintf($link, $current_page-1).'" title="Page précédente"><img src="img/icons/prev.png" alt="Précédent" width="5" height="5" /></a>';
		else
			$pagination .= '<span class="prevnext disabled"><img src="img/icons/prev.png" alt="Précédent" width="5" height="5" /></span>';

		// Lien(s) début
		for ( $i=1 ; $i<=$firstlast ; $i++ ) {
			$pagination .= ' ';
			$pagination .= ($current_page==$i) ? '<span class="current">'.$i.'</span>' : '<a href="'.sprintf($link, $i).'">'.$i.'</a>';
		}

		// ... après pages début ?
		if ( ($current_page-$around) > $firstlast+1 )
			$pagination .= ' &hellip;';

		// On boucle autour de la page courante
		$start = ($current_page-$around)>$firstlast ? $current_page-$around : $firstlast+1;
		$end = ($current_page+$around)<=($nb_pages-$firstlast) ? $current_page+$around : $nb_pages-$firstlast;
		for ( $i=$start ; $i<=$end ; $i++ ) {
			$pagination .= ' ';
			if ( $i==$current_page )
				$pagination .= '<span class="current">'.$i.'</span>';
			else
				$pagination .= '<a href="'.sprintf($link, $i).'">'.$i.'</a>';
		}

		// ... avant page nb_pages ?
		if ( ($current_page+$around) < $nb_pages-$firstlast )
			$pagination .= ' &hellip;';

		// Lien(s) fin
		$start = $nb_pages-$firstlast+1;
		if( $start <= $firstlast ) $start = $firstlast+1;
		for ( $i=$start ; $i<=$nb_pages ; $i++ ) {
			$pagination .= ' ';
			$pagination .= ($current_page==$i) ? '<span class="current">'.$i.'</span>' : '<a href="'.sprintf($link, $i).'">'.$i.'</a>';
		}

		// Lien suivant
		if ( $current_page < $nb_pages )
			$pagination .= ' <a class="prevnext" href="'.sprintf($link, ($current_page+1)).'" title="Page suivante"><img src="img/icons/next.png" alt="Suivant" width="5" height="5" /></a>';
		else
			$pagination .= ' <span class="prevnext disabled"><img src="img/icons/next.png" alt="Suivant" width="5" height="5" /></span>';
	}
	return $pagination;
}

/**
	* Supprime un repertoire
	*
	*	@param string $tmp_path
	*	@return boolean
	*/
function delete_folder($tmp_path){
	if(is_dir($tmp_path)){
	  if(!is_writeable($tmp_path) && is_dir($tmp_path)){chmod($tmp_path,0777);}
	    $handle = opendir($tmp_path);
	  while($tmp=readdir($handle)){
	    if($tmp!='..' && $tmp!='.' && $tmp!=''){
	         if(is_writeable($tmp_path.'/'.$tmp) && is_file($tmp_path.'/'.$tmp)){
	                 unlink($tmp_path.'/'.$tmp);
	         }elseif(!is_writeable($tmp_path.'/'.$tmp) && is_file($tmp_path.'/'.$tmp)){
	             chmod($tmp_path.'/'.$tmp,0666);
	             unlink($tmp_path.'/'.$tmp);
	         }
	        
	         if(is_writeable($tmp_path.'/'.$tmp) && is_dir($tmp_path.'/'.$tmp)){
	                delete_folder($tmp_path.'/'.$tmp);
	         }elseif(!is_writeable($tmp_path.'/'.$tmp) && is_dir($tmp_path.'/'.$tmp)){
	                chmod($tmp_path.'/'.$tmp,0777);
	                delete_folder($tmp_path.'/'.$tmp);
	         }
	    }
	  }
	  closedir($handle);
	  rmdir($tmp_path);
	  return true;
	}
} 


function trace($table){
	echo '<pre>';
	print_r($table);
	echo '</pre>';
}
?>