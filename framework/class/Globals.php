<?
/**
* Définit les chemins du Site Web
*/

//LOCAL
if($_SERVER['SERVER_NAME']=='laboutique75.localhost'){
	define('PATH_HTTP', 'http://laboutique75.localhost/');
	
	//DATABASE
	define('DB_HOST', 'localhost');
	define('DB_USER', 'root');
	define('DB_PASSWORD', '');
	define('DB_BASE', 'laboutique75');
}elseif($_SERVER['SERVER_NAME']=='localhost'){
	define('PATH_HTTP', 'http://localhost/laboutique75/');
	
	//DATABASE
	define('DB_HOST', 'localhost');
	define('DB_USER', 'root');
	define('DB_PASSWORD', '');
	define('DB_BASE', 'laboutique_dev');
}else{
	define('PATH_HTTP', 'http://'.$_SERVER['HTTP_HOST'].'/');
	
	//DATABASE
	define('DB_HOST', '10.0.250.69');
	define('DB_USER', 'laboutique_dev');
	define('DB_PASSWORD', '4rLK6s4y');
	define('DB_BASE', 'laboutique_dev');
	
}


//Define path framework
define('PATH_FRAMEWORK_LOGS', PATH_FRAMEWORK . 'logs/');

//Define all path www
define('PATH_LOAD', PATH_WWW . 'load/');

define('PATH_PDF', PATH_WWW . 'PDF/');

define('PATH_TRAITEMENTS', PATH_WWW . 'traitements/');

define('PATH_IMG', PATH_WWW . 'img/');
define('PATH_IMG_SLIDER', PATH_IMG . 'home_slider/');
define('PATH_IMG_CONTACT_SLIDER', PATH_IMG . 'contact_slider/');
define('PATH_IMG_FAVICON', PATH_IMG . 'favicon/');
define('PATH_IMG_PROGRAM', PATH_IMG . 'program/');
define('PATH_IMG_UPLOAD', PATH_IMG . 'uploads/');
define('PATH_IMG_ARTICLES', PATH_IMG . 'articles/');
define('PATH_IMG_PAGES', PATH_IMG . 'pages/');
define('PATH_IMG_PAGES_ALL', PATH_IMG . 'images/');

define('PATH_CSS', PATH_WWW . 'css/');
define('PATH_CSS_FONTS', PATH_CSS . 'fonts/');
define('PATH_JS', PATH_WWW . 'js/');
define('PATH_PLUGINS_JQUERY', PATH_WWW . 'plugin/jQuery/');
define('PATH_PLUGINS_SLIDES', PATH_WWW . 'plugin/slides/');

//Define all paths HTTP
define('PATH_HTTP_LOAD', PATH_HTTP . 'load/');

define('PATH_HTTP_IMG', PATH_HTTP . 'img/');

define('PATH_HTTP_IMG_GLOBAL', PATH_HTTP_IMG . 'global/');
define('PATH_HTTP_IMG_THEME', PATH_HTTP_IMG . 'theme/');
define('PATH_HTTP_IMG_FAVICON', PATH_HTTP_IMG . 'favicon/');
define('PATH_HTTP_ICONS', PATH_HTTP_IMG . 'icons/');
define('PATH_HTTP_IMG_SLIDER', PATH_HTTP_IMG . 'home_slider/');
define('PATH_HTTP_IMG_CONTACT_SLIDER', PATH_HTTP_IMG . 'contact_slider/');
define('PATH_HTTP_IMG_BLOCS_HOME', PATH_HTTP_IMG . 'home_blocs/');
define('PATH_HTTP_IMG_ACTUALITES', PATH_HTTP_IMG . 'actualites/');
define('PATH_HTTP_IMG_GALLERY', PATH_HTTP_IMG . 'gallery/');
define('PATH_HTTP_IMG_EVENTS', PATH_HTTP_IMG . 'events/');
define('PATH_HTTP_IMG_ARTISTS', PATH_HTTP_IMG . 'artists/');
define('PATH_HTTP_IMG_NEWS', PATH_HTTP_IMG . 'news/');
define('PATH_HTTP_IMG_TEAM', PATH_HTTP_IMG . 'team/');
define('PATH_HTTP_IMG_PROGRAM', PATH_HTTP_IMG . 'program/');
define('PATH_HTTP_IMG_UPLOAD', PATH_HTTP_IMG . 'uploads/');
define('PATH_HTTP_IMG_ARTICLES', PATH_HTTP_IMG . 'articles/');
define('PATH_HTTP_IMG_PAGES', PATH_HTTP_IMG . 'pages/');
define('PATH_HTTP_IMG_PAGES_ALL', PATH_HTTP_IMG . 'images/');

define('PATH_HTTP_CSS', PATH_HTTP . 'css/');
define('PATH_HTTP_CSS_FONTS', PATH_HTTP_CSS . 'fonts/');

define('PATH_HTTP_SVG', PATH_HTTP . 'svg/');

define('PATH_HTTP_FONTS', PATH_HTTP . 'fonts/');

define('PATH_HTTP_JS', PATH_HTTP . 'js/');
define('PATH_HTTP_JS_THEME', PATH_HTTP . 'js/theme/');
define('PATH_HTTP_PLUGINS_JQUERY', PATH_HTTP . 'plugin/jQuery/');
define('PATH_HTTP_PLUGINS_JQUERYUI', PATH_HTTP . 'plugin/jQueryUI/');
define('PATH_HTTP_PLUGINS_BGPOS', PATH_HTTP . 'plugin/bgPos/');
define('PATH_HTTP_PLUGINS_ACCORDION', PATH_HTTP . 'plugin/accordion/');
define('PATH_HTTP_PLUGINS_GMAP3', PATH_HTTP . 'plugin/gmap3/');
define('PATH_HTTP_PLUGINS_SLIDEBARS', PATH_HTTP . 'plugin/slidebars/');
define('PATH_HTTP_PLUGINS_INFINITESCROLL', PATH_HTTP . 'plugin/infiniteScroll/');
define('PATH_HTTP_PLUGINS_SLIDES', PATH_HTTP . 'plugin/slides/');
define('PATH_HTTP_PLUGINS_VALIDFORM', PATH_HTTP . 'plugin/validForm/');
define('PATH_HTTP_PLUGINS_FANCYBOX', PATH_HTTP . 'plugin/fancybox/');
define('PATH_HTTP_PLUGINS_COLORBOX', PATH_HTTP . 'plugin/colorbox/');
define('PATH_HTTP_PLUGINS_TINYMCE', PATH_HTTP . 'plugin/tinymce/');
define('PATH_HTTP_PLUGINS_PLUPLOAD', PATH_HTTP . 'plugin/plupload/');
define('PATH_HTTP_PLUGINS_CKEDITOR', PATH_HTTP . 'plugin/ckeditor/');
define('PATH_HTTP_PLUGINS_BOOTSTRAP', PATH_HTTP . 'plugin/bootstrap/');
define('PATH_HTTP_PLUGINS_SMINT', PATH_HTTP . 'plugin/smint/');
define('PATH_HTTP_PLUGINS_STICKY', PATH_HTTP . 'plugin/sticky/');
define('PATH_HTTP_PLUGINS_ROYALSLIDER', PATH_HTTP . 'plugin/jquery.royalslider/');
define('PATH_HTTP_PLUGINS_NICESCROLL', PATH_HTTP . 'plugin/jquery.nicescroll/');
define('PATH_HTTP_PLUGINS_SWIPE', PATH_HTTP . 'plugin/swiper-master/');
define('PATH_HTTP_PLUGINS_MAGNIFICPOPUP', PATH_HTTP . 'plugin/magnific-popup-master/');
define('PATH_HTTP_PLUGINS_ONEPAGE', PATH_HTTP . 'plugin/jquery-one-page/');
define('PATH_HTTP_PLUGINS_RESPONSIVEMENU', PATH_HTTP . 'plugin/responsive-menu/');
define('PATH_HTTP_PLUGINS_PARALLAX', PATH_HTTP . 'plugin/parallax-master/');
define('PATH_HTTP_PLUGINS_TEXTFIT', PATH_HTTP . 'plugin/textFit/');

//Define admin path
define('PATH_WWW_ADMIN', PATH_WWW . 'admin_lb75/');
define('PATH_WWW_ADMIN_LOAD', PATH_WWW_ADMIN . 'load/');
define('PATH_WWW_ADMIN_IMG', PATH_WWW_ADMIN . 'img/');

//Define admin path HTTP
define('PATH_ADMIN', PATH_HTTP . 'admin_lb75/');
define('PATH_ADMIN_IMG', PATH_ADMIN . 'img/');
define('PATH_ADMIN_IMG_ICONS', PATH_ADMIN_IMG . 'icons/');
define('PATH_ADMIN_CSS', PATH_ADMIN . 'css/');
define('PATH_ADMIN_JS', PATH_ADMIN . 'js/');

define('SEPARATOR', '_');
define('BDD', 'lb75_');
define('TITLE_BY_DEFAULT', 'La Boutique 75');
define('TITLE_BY_DEFAULT_ADMIN', 'La Boutique 75 - Administration');
define('META_DESCRIPTION_BY_DEFAULT', '');

define('EXISTING_LANGUAGES', serialize(array('fr' => 'fr', 'en' => 'en', 'cn' => 'cn')));

define('IMG_DEFAULT', PATH_HTTP . 'timthumb.php?src=' . PATH_HTTP_IMG_GLOBAL . 'logo_principal.png&amp;w=200&amp;h=200&amp;zc=2&amp;q=100');

//MAILS
define('EMAIL_ADMIN', 'flodevelop@gmail.com');
define('EMAIL_TEST', '');
define('EMAIL_ME', '');
define('EMAIL_FROM', 'flodevelop@gmail.com');

?>