<?php
/**
 * @class HTML
 * @brief Class to manage HTML content
 *
 * @abstract
 * @author      Julien Guion
 * @version     1
 */
abstract class HTML {
    
    private $_mode              	= 'return'; // echo|return
    static private $_modeS     	 	= 'return'; // echo|return
    static private $_plugins		= array();
    static private $_loadedPlugins 	= array();
    const BR                    	= "\n";

    /**
     * Returns or writes HTML content, switch the current mode
     * @param string  html
     * @return mixed
     */
    public function write($html) {
        if(!isset($this)) return self::writeStatic($html);
        $html   .= self::BR;
        $mode   = $this->getMode();
        if($mode == 'echo') {
            echo $html;
            return null;
        } else {
            return $html;
        }
    }

	/**
	 * compress function.
	 * @access public
	 * @static
	 * @param mixed $html
	 * @return void
	 */
	public static function compress($html){
		
		$html = preg_replace('/[\n\r\t ]+\/\/.*?[\n\r]/i','',$html);
		$html = preg_replace('/[\t\n ]+/is',' ',$html);
		$html = preg_replace('/\/\*.*?\*\//is','',$html);
		$html = preg_replace('/<!--.*?-->/is','',$html);
		$html = preg_replace('!(\})[\t ]+((?:\$\()?function)!i', "$1\n$2", $html);
		return $html;
	}

    /**
     * Returns or writes HTML content, switch the current mode, for static objects
     * @abstract
     * @param string  html
     * @return mixed
     */
    public static function writeStatic($html) {
        $mode   = self::getModeStatic();
        if($mode == 'echo') {
            echo $html;
            return null;
        } else {
            return $html;
        }
    }

    /**
     * Get the current mode
     * @return string
     */
    public function getMode() {
        return $this->_mode;
    }

    /**
     * The the current mode
     * @param String $_mode
     * @return HTML $this
     */
    public function setMode($_mode) {
        $this->_mode    = $_mode;
        return $this;
    }

    /**
     * Get the current mode, for static objects
     * @return string
     */
    public static function getModeStatic() {
        return self::$_modeS;
    }

    /**
     * Set the current mode, for static objects
     * @param string $_mode
     */
    public static function setModeStatic($_mode) {
        self::$_modeS    = $_mode;
    }
    
    /**
     * Add attribute
     * @param array $tAttributes
     * @param array $newAttribute
     * @return string
     */
    public static function addAttributes(array $tAttributes, array $newAttribute, $force=false) {

		if($force)
			$tAttributes = array_merge($tAttributes, $newAttribute);
		else
			$tAttributes = array_merge($newAttribute, $tAttributes);

        return $tAttributes;
    }
    
    /**
     * Convert an array to html attributes
     * @param array $tAttributes
     * @return string
     */
    public static function arrayToAttributes(array $tAttributes) {
 		$attributes     = '';
        foreach($tAttributes as $attr => $value) {
            $attributes   .= ' '.$attr.'="'.self::protect($value).'"';
        }

        return $attributes;
    }
    
    /**
     * protect HTML string
     * @param mixed $value
     * @return string
     */
    public static function protect($value) {
    	
    	if(is_array($value))
    	{
    		foreach($value as $key=>$elem)
    		{
    			$value[$key] = self::protect($elem);
    		}	
    	}
    	else
    	{
    		$value     = str_replace('"', "''", $value);
        	$value     = htmlentities($value);
    	}
    	
        return $value;
    }	
      
}

