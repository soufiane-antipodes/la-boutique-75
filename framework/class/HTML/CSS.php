<?php
/**
 * @class       CSS
 * @brief       CSS Manager
 * This class allows to add CSS file calls into the header
 *
 * @subpackage  HTML
 * @date        Mars 2011
 * @author      Julien Guion
 * @copyright   BMX Family
 * @version     1
 */
abstract class HTML_CSS extends HTML {

    private static $_tFiles             = array();
    private static $_filesInitialized   = false;

    /**
     * Add Css file
     * @param string $file
     */
    public static function addFile($file) {
			if(!self::isInitialized()) self::initialize();
		        if(!in_array($file, self::$_tFiles))	self::$_tFiles[]    = $file;
    }

    /**
     * Add Css font files
     * @param string $file
     */    
    public static function addFonts($font) {
    	
    	$args = func_get_args();
    	
    	if(count($args)>1)
    	{
    		foreach($args as $arg)
    		{
    			self::addFonts($arg);	
    		}
    	}
    	else
    	{
    		$font = trim($args[0]);
    		//if(!empty($font)) self::addFile(PATH_HTTP_CSS_FONTS . $font.'/'.$font.'.css');	
    	}
    }
    
    /**
    * Add Basic CSS Style file
    *
    */
    public static function addCssFile($file) {
    	
    	$args = func_get_args();
    	
    	if(count($args)>1)
    	{
    		foreach($args as $arg)
    		{
    			self::addCssFile($arg);	
    		}
    	}
    	else
    	{
    		$file = trim($args[0]);
    		if(!empty($file)) self::addFile(PATH_HTTP_CSS . $file.'.css');	
    	}
    }

    /**
     * Display all css files called
     * @return string
     */
    public static function displayFiles() {

	Trigger::call('onBeforeParseCssFiles');
	
	$tFiles	    = self::$_tFiles;
	$html	    = '';
	foreach($tFiles as $file) {
	    $html   .= self::BR.'<link rel="stylesheet" href="'.$file.'" />';
	}
	Trigger::call('onAfterParseCssFiles');
	echo $html;
    }

    /**
     * Check if parseing is initialized
     * @return boolean
     */
    private static function isInitialized() {
        return self::$_filesInitialized;
    }

    /**
     * Initialize parsing
     */
    private static function initialize() {
        Trigger::bind('onAfterParseHeader',array('HTML_CSS', 'displayFiles'));
        self::$_filesInitialized   = true;
    }
}
?>