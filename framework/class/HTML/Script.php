<?php
/**
 * @class HTML_Script
 * @brief Manage JavaScript content JavaScript element
 *
 * You can add scripts everywhere in your PHP code, with HTML_Script::addScript()
 *
 * @abstract
 * @package     Core
 * @subpackage  HTML
 * @date        Mars 2011
 * @author      Julien Guion
 * @copyright   BMX Family
 * @version     1
 * @uses        HTML    Extends of HTML, to write html content
 */
abstract class HTML_Script extends HTML {

    static private $_tQueries		= array();
    static private $_tQueriesWithoutFunction		= array();
    static private $_filesInitialized	= false;
    static private $_tFiles		= array();
    static private $_useScript		= array(
            'jQuery'       => array('url'=>'http://ajax.googleapis.com/ajax/libs/jquery/%s/jquery.min.js', 'version' => null )
            ,'jQuery.ui'    => array('url'=>'http://ajax.googleapis.com/ajax/libs/jqueryui/%s/jquery-ui.min.js', 'version' =>null));

    /**
     * Add query do Queries Stack
     * @param string $query
     * @param boolean $insertBefore    Default:false. Set on true to insert this script in first
     */
    public static function addScript($query,$insertBefore = false, $withoutFunction=false) {
        if(is_array($query)) $query = implode("\n\t", $query);
        
        if($withoutFunction){
        	if(!$insertBefore && count(self::$_tQueriesWithoutFunction)>0) {
	            self::$_tQueriesWithoutFunction = array_merge(self::$_tQueriesWithoutFunction, (array)$query);
	        } else {
	            self::$_tQueriesWithoutFunction = array_merge((array)$query, self::$_tQueriesWithoutFunction);
	        }
        }
        else{
	        if(!$insertBefore && count(self::$_tQueries)>0) {
	            self::$_tQueries = array_merge(self::$_tQueries, (array)$query);
	        } else {
	            self::$_tQueries = array_merge((array)$query, self::$_tQueries);
	        }
	      }
    }
    
    public static function formatJsVar($value)
    {
    	if(is_int($value) || is_float($value))
    		return $value;
    	elseif(is_bool($value))
    		return $value ? 'true' : 'false';
    	elseif(is_array($value) || is_object($value))
    		return json_encode($value);
    	else
    		return '"'.$value.'"';
    }
     
	/**
     * Insert a php value to javascript
     * @param array $value
     * @param string $name
     */
    public static function addVar($value, $name)
    {    	    	
    	HTML_Script::addScript($name.' = '.self::formatJsVar($value).';', true);
    }
       
	/**
     * Insert a php array to javascript
     * @param array $array
     * @param string $name
     */
    public static function addArray($array, $name)
    {    	
    	foreach($array as $key=>$value)
    	{
    		HTML_Script::addScript($name.'["'.$key.'"] = '.self::formatJsVar($value).';', true);
    	}
    	
    	HTML_Script::addScript($name.' = new Array();', true);
    }
    
	/**
     * Insert a php array to javascript
     * @param array $array
     * @param string $name
     */
    public static function addMultidimensionalArray($array, $name)
    {    	
    	self::addVar($array, $name);
    }

    /**
     * Add a function at the top of the stack
     * @param string $function
     */    
    public static function addFunction($function)
    {
    	self::addScript($function,true);
    }

    /**
     * Display the query stack
     * [@param  boolean with scripts tags]
     */
    public static function displayAll($withScriptTags=true) {

        Trigger::call('onBeforeParseScript');
        

        // ::::: install jQuery :::::
        //$htmlInit   = self::initLibrairies();
        
        $html='';
        
        // ::::: write scripts :::::
        if(!empty(self::$_tQueries))
        {
	        $html   .= '$(function() {';
	        foreach(self::$_tQueries as $query) $html   .= "\n\t{$query}";
	        $html   .= "\n});";
	        if($withScriptTags) $html   = self::BR.'<script>'.self::BR.$html."\n</script>";
        }
        
        if(!empty(self::$_tQueriesWithoutFunction))
        {
	        foreach(self::$_tQueriesWithoutFunction as $query) $html   .= "\n\t{$query}";
        }
        Trigger::call('onAfterParseScript');
      
        return self::writeStatic($html);
    }

    /**
     * Disabled script
     * @param string $script
     */
    public static function disabledScript($script) {
        if( isset(self::$_useScript[$script]) ) unset(self::$_useScript[$script]);
    }

   /**
     * Add source script in content
     * @param string $file
     */
    public static function addFile($file) {
    	    	
	if(!in_array($file, self::$_tFiles))	self::$_tFiles[]    = $file;
	if(!self::$_filesInitialized) {
	    self::$_filesInitialized = true;
	    Trigger::bind('onAfterParseScript',array('HTML_Script', 'displayFiles'));
	}
    }

    /**
     * Display all javascript file' calls
     * @return string
     */
    public static function displayFiles() {

	Trigger::call('onBeforeParseScriptFiles');

	$tFiles	    = self::$_tFiles;
	$html	    = '';
	foreach($tFiles as $file) {
	    $html   .= self::BR.'<script src="'.$file.'"></script>';
	}

	Trigger::call('onAfterParseScriptFiles');
	echo $html;
    }


    /**
     * Convert array of parameters to string
     * @param array $tArgs
     * @return string $parameters
     */
    public static function arrayToArgs($tArgs) {
        $string   = '';
        
        foreach($tArgs as $key => $arg) {

            if(is_array($arg)) {
                $string .= '{'.self::arrayToArgs($arg).'}';
            } else {

                // ::::: string :::::
                if($arg[0] != ':' ) {
                    $string .= (empty($string) ? '' : ', ')
                        .(is_numeric($key) ? '' : "'".addslashes($key)."'".':')
                        . "'".addslashes($arg)."'";
                // ::::: expression :::::
                } else {
                    $string .= (empty($string) ? '' : ', ')
                        .(is_numeric($key) ? '' : "'".addslashes($key)."'".':')
                        .substr($arg, 1);
                }
            }
        }
        return $string;
    }
  
    /**
     * Clear the list of scripts
     */  
    public static function clearAll()
    {
     	self::$_tQueries = array();
     	self::$_tQueriesWithoutFunction = array();
   		self::$_tFiles   = array();   	
    }
}

