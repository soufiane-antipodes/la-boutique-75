<?php
/**
 * @class Event
 * @brief Class Event allows to
 *
 * @package     Core
 * @date        October 2011
 * @author      Julien Guion
 * @copyright   a2w
 * @version     1
 */
class HTML_Script_Event extends HTML_Script {
    
     /**
     * Insert content into element, in Ajax
     * @param string $domElement
     * @param array $tParams
     * @param string $url
     * @param string $effect
     * @return string
     */
    public static function ajaxLoadContent($domTarget, $action, array $tParams = array(), $effect = null,  $url = null) {
        $js = '';
        $domTarget  = "$('".addslashes($domTarget)."')";
        if(!is_null($effect)) {
            if(!preg_match('!\((.*?)\)!', $effect)) {
                $effect .= '()';
            }
            $js .= $domTarget.'.hide();';
        }

        $tParams['action']  = $action;
        $params = self::_formatAjaxParameters($tParams);
        if(is_null($url))   $url = basename($_SERVER['PHP_SELF']);

        $js .= "$.post('".$url."', ".$params.", function(data) {"
            .$domTarget.".html(data);"
            .(is_null($effect) ? '' : $domTarget.'.'.$effect )
            ."\n});";
        return $js;
    }

}
?>