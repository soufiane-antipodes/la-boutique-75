<?php
Class Item{
	
	public static function addGallery($fileToUpload, $idCollection, $isPress, &$error){
		
		//Test upload
		$result = Picture :: isOkForUpload($fileToUpload, $error);
		if($result !== true) return $result;
			
		//START Transaction
		Database :: query('BEGIN;');
		
		$extension = substr(strrchr($fileToUpload['name'],'.'), 1);
		$name = basename($fileToUpload['name'], '.'.$extension);
		
		$position = Position :: generatePosition('t_items', 'idCollection="'.$idCollection.'"', 'end');
		
		$result = Database :: query('insert into '.BDD.'t_items(idItem, idCollection, isPress, value, extension, type, position) VALUES("", "'.mysql_real_escape_string($idCollection).'", "'.mysql_real_escape_string($isPress).'", "'.mysql_real_escape_string($name).'", "'.mysql_real_escape_string(strtolower($extension)).'", "picture", "'.mysql_real_escape_string($position).'")');
		$idItem = Database :: getGeneratedId();
		
		
		if($isPress == 1){
			//Upload File
			$result = Picture :: upload($fileToUpload, PATH_IMG_PRESS . '/' . Sanitize :: keepValidChars($name) . '-' . $idItem, $error);
			if($result !== true){ Database :: query('ROLLBACK;');return $result;}
			
			//Create Thumbnail
			$result = Picture :: createThumb(PATH_IMG_PRESS, PATH_IMG_PRESS . 'large/', Sanitize :: keepValidChars($name) . '-' . $idItem . '.'. strtolower($extension), null, 598, 800);
			if($result !== true){ Database :: query('ROLLBACK;');return $result;}
			
			//Create Thumbnail
			$result = Picture :: createThumb(PATH_IMG_PRESS, PATH_IMG_PRESS . 'medium/', Sanitize :: keepValidChars($name) . '-' . $idItem . '.'. strtolower($extension), null, 377, 504);
			if($result !== true){ Database :: query('ROLLBACK;');return $result;}
			
			//Create Thumbnail
			$result = Picture :: createThumb(PATH_IMG_PRESS, PATH_IMG_PRESS . 'small/', Sanitize :: keepValidChars($name) . '-' . $idItem . '.'. strtolower($extension), null, 181, 252);
			if($result !== true){ Database :: query('ROLLBACK;');return $result;}
			
			if(file_exists(PATH_IMG_PRESS . '/' . Sanitize :: keepValidChars($name) . '-' . $idItem. '.'. strtolower($extension))){
				unlink(PATH_IMG_PRESS . '/' . Sanitize :: keepValidChars($name) . '-' . $idItem. '.'. strtolower($extension));
			}
			
		}
		else{
		
			//Upload File
			$result = Picture :: upload($fileToUpload, Picture :: getPhysicalPictureCollection($idCollection) . '/' . Sanitize :: keepValidChars($name) . '-' . $idItem, $error);
			if($result !== true){ Database :: query('ROLLBACK;');return $result;}
			
			//Create Thumbnail
			$result = Picture :: createThumb(Picture :: getPhysicalPictureCollection($idCollection) . '/', Picture :: getPhysicalPictureCollection($idCollection) . '/', Sanitize :: keepValidChars($name) . '-' . $idItem . '.'. strtolower($extension), null, null, 504);
			if($result !== true){ Database :: query('ROLLBACK;');return $result;}
		}
		//END Transaction
		Database :: query('COMMIT;');
		
		return $result;

	}
	
	public static function addDescription($fileToUpload, $idCollection, $description, $descriptionFR, &$error){
		
		//Test upload
		$result = Picture :: isOkForUpload($fileToUpload, $error);
		if($result !== true) return $result;
			
		//START Transaction
		Database :: query('BEGIN;');
		
		$extension = substr(strrchr($fileToUpload['name'],'.'), 1);
		$name = basename($fileToUpload['name'], '.'.$extension);
		
		$position = Position :: generatePosition('t_items', 'idCollection="'.$idCollection.'"', 'end');
		
		$result = Database :: query('insert into '.BDD.'t_items(idItem, idCollection, value, descriptionEN, descriptionFR, extension, type, position) VALUES("", "'.mysql_real_escape_string($idCollection).'", "'.mysql_real_escape_string($name).'", "'.mysql_real_escape_string($description).'", "'.mysql_real_escape_string($descriptionFR).'", "'.mysql_real_escape_string(strtolower($extension)).'", "description", "'.mysql_real_escape_string($position).'")');
		$idItem = Database :: getGeneratedId();
		
		//Upload File
		$result = Picture :: upload($fileToUpload, Picture :: getPhysicalPictureCollection($idCollection) . '/' . Sanitize :: keepValidChars($name) . '-' . $idItem, $error);
		if($result !== true){ Database :: query('ROLLBACK;');return $result;}
		
		//Create Thumbnail
		$result = Picture :: createThumb(Picture :: getPhysicalPictureCollection($idCollection) . '/', Picture :: getPhysicalPictureCollection($idCollection) . '/', Sanitize :: keepValidChars($name) . '-' . $idItem . '.'. strtolower($extension), null, 413, 356);
		if($result !== true){ Database :: query('ROLLBACK;');return $result;}
		
		//END Transaction
		Database :: query('COMMIT;');
		
		return $result;

	}
	
	public static function editGallery($idItem, $description, $descriptionFR, &$error){
		
		$result = Database :: query('update '.BDD.'tItems set description="'.mysql_real_escape_string($description).'", descriptionFR="'.mysql_real_escape_string($descriptionFR).'" where idItem="'.mysql_real_escape_string($idItem).'"');
		
		return $result;

	}
	
	public static function editDescription($fileToUpload, $idItem, $description, $descriptionFR, &$error){
		
		$isExtension = null;
		
		if($fileToUpload['size'] > 0){
			//Test upload
			$result = Picture :: isOkForUpload($fileToUpload, $error);
			if($result !== true) return $result;
			
			$extension = substr(strrchr($fileToUpload['name'],'.'), 1);
			$name = basename($fileToUpload['name'], '.'.$extension);
			
			$isExtension = ', extension="'.mysql_real_escape_string(strtolower($extension)).'"';
			$item = array();
			Database :: getLine('select C.idCollection, C.nameFR from '.BDD.'t_items I inner join '.BDD.'t_collection C on C.idCollection=I.idCollection where I.idItem="'.mysql_real_escape_string($idItem).'"', $item);
			
			if(file_exists(Picture :: getPhysicalPictureItem($idItem)))
				unlink(Picture :: getPhysicalPictureItem($idItem));
		}
			
		//START Transaction
		Database :: query('BEGIN;');
		
		$result = Database :: query('update '.BDD.'t_items set value="'.mysql_real_escape_string($name).'", descriptionEN="'.mysql_real_escape_string($description).'", descriptionFR="'.mysql_real_escape_string($descriptionFR).'"'.$isExtension.' where idItem="'.mysql_real_escape_string($idItem).'"');
		
		//Upload File
		if($fileToUpload['size'] > 0){
			//Upload File
			$result = Picture :: upload($fileToUpload, Picture :: getPhysicalPictureCollection($item['idCollection']) . '/' . Sanitize :: keepValidChars($name) . '-' . $idItem, $error);
			if($result !== true){ Database :: query('ROLLBACK;');return $result;}
			
			//Create Thumbnail
			$result = Picture :: createThumb(Picture :: getPhysicalPictureCollection($item['idCollection']) . '/', Picture :: getPhysicalPictureCollection($item['idCollection']) . '/', Sanitize :: keepValidChars($name) . '-' . $idItem . '.'. strtolower($extension), null, 413, 356);
			if($result !== true){ Database :: query('ROLLBACK;');return $result;}
		}
		
		
		//END Transaction
		Database :: query('COMMIT;');
		
		return $result;

	}
}


