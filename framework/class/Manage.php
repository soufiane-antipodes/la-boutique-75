<?php
Class Manage{
	
	public static function add($tableName, $fileToUpload='', $fields, $thumbnails='', $position=array(), &$error){
		
		$typeSql = '';
		$valueSql = '';
		
		//TEST FILE BEFORE UPLOAD
		if(!empty($fileToUpload)){
			if(isset($fileToUpload[0])){
				
				foreach($fileToUpload as $key => $file){
					if($file['size'] > 0 || $file['isMandatory']){
						Picture :: setExtensions($file['extension']);
						$result = Picture :: isOkForUpload($file, $error);
						if($result !== true) return $result;
						
						${'extension_'.$key} = substr(strrchr($file['name'],'.'), 1);
						${'name_'.$key} = basename($file['name'], '.'.${'extension_'.$key});
					}
				}
				
			}
			else{
				if($fileToUpload['size'] > 0 || $fileToUpload['isMandatory']){
					Picture :: setExtensions($fileToUpload['extension']);
					$result = Picture :: isOkForUpload($fileToUpload, $error);
					if($result !== true) return $result;
					
					$extension = substr(strrchr($fileToUpload['name'],'.'), 1);
					$name = basename($fileToUpload['name'], '.'.$extension);
				}
			}
		}
		
		if(count($fields) > 0){
			foreach($fields as $key => $field){
				$isPassword = false;
				if($field['type'] == 'password') $isPassword = true;
				
				if($field['value'] != '' || $field['isMandatory']){
					switch($field['type']){
						case 'string' : 
							if(!Validation :: string($field['value'])){ $error .= 'Le champ '.$field['name'].' ne doit pas être vide.'; return false; }
						break;
						case 'number' : 
							if(!Validation :: integer($field['value'])){ $error .= 'Le champ '.$field['name'].' doit être un nombre.'; return false; }
						break;
						case 'email' : 
							if(!Validation :: email($field['value'])){ $error .= 'Le champ '.$field['name'].' doit être un email.'; return false; }
						break;
						case 'password' : 
							$field['value'] = 'MD5("'.mysql_real_escape_string($field['value']).'"), ';
							$isPassword = true;
						break;
						case 'youtube' : 
							$value = Validation :: youtube($field['value']);
							if(!$value){ $error .= 'Le champ '.$field['name'].' est incorrect.'; return false; }
							else $field['value'] = $value;
						break;
						case 'date-d/m' : 
							if(preg_match('#^[0-9]{0,2}\/[0-9]{0,2}$#', $field['value'], $matches)) $field['value'] = date('Y').'-'.substr($field['value'], 3, 2).'-'.substr($field['value'], 0, 2).' '.date('H:i:s');
							else{ $error .= 'Le champ '.$field['name'].' est incorrect.'; return false; } 
						break;
						case 'date-d/m/Y' : 
							if(preg_match('#^[0-9]{0,2}\/[0-9]{0,2}\/[0-9]{0,4}$#', $field['value'], $matches)) $field['value'] = substr($field['value'], 6, 4).'-'.substr($field['value'], 3, 2).'-'.substr($field['value'], 0, 2).' '.date('H:i:s');
							else{ $error .= 'Le champ '.$field['name'].' est incorrect.'; return false; } 
						break;
					}
				}
				
				if($isPassword){
					if(!empty($field['value'])){
						$typeSql .= $key . ',';
						$valueSql .= $field['value'];
					}
				}else{
					$typeSql .= $key . ',';
					$valueSql .= '"'.mysql_real_escape_string($field['value']).'", ';
				}
			}
		}
		
		
		//START Transaction
		Database :: query('BEGIN;');
		
		if(!empty($position) && isset($position['position'])){
			switch($position['position']){
				case 'first' : 
					$positionName = ',position';
					$positionSql = ', "0"';
				break;
				case 'end' : 
					$positionName = ',position';
					$positionSql = ', "'.Position :: generatePosition($tableName, $position['condition'], 'end').'"';
				break;
				default : 
					$positionName = null;
					$positionSql = null;
				break;
			}
		}
		else{ 
			$positionName = null;
			$positionSql = null;
		}
		
		if(!empty($fileToUpload)){
			if(isset($fileToUpload[0])){
				foreach($fileToUpload as $key => $file){
					if($file['size'] > 0 || $file['isMandatory']){
						$result = Database :: query('insert into '.BDD.'item(id_item, value, extension) VALUES("", "'.mysql_real_escape_string(${'name_'.$key}).'", "'.mysql_real_escape_string(strtolower(${'extension_'.$key})).'")');
						${'idItem_'.$key} = Database :: getGeneratedId();
					}
				}
			}
			else{
				if($fileToUpload['size'] > 0 || $fileToUpload['isMandatory']){
					$result = Database :: query('insert into '.BDD.'item(id_item, value, extension) VALUES("", "'.mysql_real_escape_string($name).'", "'.mysql_real_escape_string(strtolower($extension)).'")');
					$idItem = Database :: getGeneratedId();
				}
			}
		}
		
		$request = 'insert into '.BDD.$tableName . '('.substr($typeSql, 0, -1).((isset($idItem))? ((!empty($typeSql)) ? ', ':null).'id_item':null);
		if(isset($fileToUpload[0])){
			foreach($fileToUpload as $key => $file){
				$request .= ((!empty($typeSql)) ? ', ':null).$file['idName'];
			}
		}
		
		$request .= $positionName.') values('.substr($valueSql, 0, -2).((isset($idItem))? ((!empty($typeSql)) ? ', ':null).$idItem :null);
		
		if(isset($fileToUpload[0])){
			foreach($fileToUpload as $key => $file){
				$request .= ((!empty($typeSql)) ? ', ':null).${'idItem_'.$key};
			}
		}
		
		$request .= $positionSql.')';
		
		//INSERT BDD
		$result = Database :: query($request);
		
		if(!empty($position) && isset($position['position'])){
			switch($position['position']){
				case 'first' : 
					Position :: generatePosition($tableName, $position['condition'], 'first');
				break;
			}
		}
		
		if(!empty($fileToUpload)){
			if(isset($fileToUpload[0])){
				foreach($fileToUpload as $key => $file){
					if($file['size'] > 0 || $file['isMandatory']){
						//Upload File
						$result = Picture :: upload($file, $file['dest'] . '/' . Sanitize :: keepValidChars(${'name_'.$key}) . '-' . ${'idItem_'.$key}, $error);
						if($result !== true){ Database :: query('ROLLBACK;');return $result;}
						
						
						if(count($thumbnails) > 0 && isset($thumbnails[$key])){
							foreach($thumbnails[$key] as $thumbnail){
								
								$maxHeight = (isset($thumbnail['maxHeight'])) ? $thumbnail['maxHeight'] : null;
								
								//Create Thumbnail
								$result = Picture :: createThumb($thumbnail['source'], $thumbnail['dest'], Sanitize :: keepValidChars(${'name_'.$key}) . '-' . ${'idItem_'.$key} . '.'. strtolower(${'extension_'.$key}), null, $thumbnail['width'], $thumbnail['height'], null, $maxHeight);
								if($result !== true){ Database :: query('ROLLBACK;'); $error .= 'L\'image contient une erreur et ne peux être redimentionée.'; return $result;}
							}
						}
					}
				}
			}
			else{
				if($fileToUpload['size'] > 0 || $fileToUpload['isMandatory']){
					//Upload File
					$result = Picture :: upload($fileToUpload, $fileToUpload['dest'] . '/' . Sanitize :: keepValidChars($name) . '-' . $idItem, $error);
					if($result !== true){ Database :: query('ROLLBACK;');return $result;}
					
					
					if(count($thumbnails) > 0){
						foreach($thumbnails as $thumbnail){
							
							$maxHeight = (isset($thumbnail['maxHeight'])) ? $thumbnail['maxHeight'] : null;
							
							//Create Thumbnail
							$result = Picture :: createThumb($thumbnail['source'], $thumbnail['dest'], Sanitize :: keepValidChars($name) . '-' . $idItem . '.'. strtolower($extension), null, $thumbnail['width'], $thumbnail['height'], null, $maxHeight);
							if($result !== true){ Database :: query('ROLLBACK;'); $error .= 'L\'image contient une erreur et ne peux être redimentionée.'; return $result;}
						}
					}
				}
			}
		}
		
		//END Transaction
		Database :: query('COMMIT;');
		
		return $result;
	}
	
	public static function edit($tableName, $id, $fileToUpload='', $fields, $thumbnails='', &$error){
		
		$valueSql = '';
		$idItemUpdated = '';
		
		//TEST FILE BEFORE UPLOAD
		if(!empty($fileToUpload)){
			if(isset($fileToUpload[0])){
				foreach($fileToUpload as $key => $file){
					if($file['size'] > 0){
						
						Picture :: setExtensions($file['extension']);
						$result = Picture :: isOkForUpload($file, $error);
						if($result !== true) return $result;
						
						${'extension_'.$key} = substr(strrchr($file['name'],'.'), 1);
						${'name_'.$key} = basename($file['name'], '.'.${'extension_'.$key});
					}
				}
				
			}
			else{
				if($fileToUpload['size'] > 0){
					Picture :: setExtensions($fileToUpload['extension']);
					$result = Picture :: isOkForUpload($fileToUpload, $error);
					if($result !== true) return $result;
					
					$extension = substr(strrchr($fileToUpload['name'],'.'), 1);
					$name = basename($fileToUpload['name'], '.'.$extension);
				}
			}
		}
		
		if(count($fields) > 0){
			foreach($fields as $key => $field){
				$isPassword = false;
				if($field['type'] == 'password') $isPassword = true;
				
				if(!empty($field['value']) || $field['isMandatory']){
					
					switch($field['type']){
						case 'string' : 
							if(!Validation :: string($field['value'])){ $error .= 'Le champ '.$field['name'].' ne doit pas être vide.'; return false; }
						break;
						case 'number' : 
							if(!Validation :: integer($field['value'])){ $error .= 'Le champ '.$field['name'].' doit être un nombre.'; return false; }
						break;
						case 'email' : 
							if(!Validation :: email($field['value'])){ $error .= 'Le champ '.$field['name'].' doit être un email.'; return false; }
						break;
						case 'password' : 
							$field['value'] = 'MD5("'.mysql_real_escape_string($field['value']).'"), ';
						break;
						case 'youtube' : 
							$value = Validation :: youtube($field['value']);
							if(!$value){ $error .= 'Le champ '.$field['name'].' est incorrect.'; return false; }
							else $field['value'] = $value;
						break;
						case 'date-d/m' : 
							if(preg_match('#^[0-9]{0,2}\/[0-9]{0,2}$#', $field['value'], $matches)) $field['value'] = date('Y').'-'.substr($field['value'], 3, 2).'-'.substr($field['value'], 0, 2).' '.date('H:i:s');
							else{ $error .= 'Le champ '.$field['name'].' est incorrect.'; return false; } 
						break;
						case 'date-d/m/Y' : 
							if(preg_match('#^[0-9]{0,2}\/[0-9]{0,2}\/[0-9]{0,4}$#', $field['value'], $matches)) $field['value'] = substr($field['value'], 6, 4).'-'.substr($field['value'], 3, 2).'-'.substr($field['value'], 0, 2).' '.date('H:i:s');
							else{ $error .= 'Le champ '.$field['name'].' est incorrect.'; return false; } 
						break;
					}
				}
				if($isPassword){
					if(!empty($field['value'])) $valueSql .= $key . '='.$field['value'];
				}
				else $valueSql .= $key . '="'.mysql_real_escape_string($field['value']).'", ';
			}
		}
		
		//START Transaction
		Database :: query('BEGIN;');
		
		if(!empty($fileToUpload)){
			if(isset($fileToUpload[0])){
				foreach($fileToUpload as $key => $file){
					if($file['size'] > 0 ){
					
						$lastIdItem = Database :: get('select '.$file['idName'].' from '.BDD. $tableName.' where '.$id['name'].'="'.mysql_real_escape_string($id['value']).'"');
						$lastItem = array();
						Database :: getLine('select id_item, value, extension from '.BDD.'item where id_item="'.mysql_real_escape_string($lastIdItem).'"', $lastItem);
						
						//DELETE OLD ITEM
						//Database :: query('delete from '.BDD.'item where id_item="'.mysql_real_escape_string($lastIdItem).'"');
						
						
						//SUPPRESSION DES FICHIERS A FAIRE
						/*
						if(count($thumbnails) > 0 && isset($thumbnails[$key])){
							foreach($thumbnails[$key] as $thumbnail){
								if(file_exists($thumbnail['dest'] . Sanitize :: keepValidChars($lastItem['value']) . '-'.$lastItem['id_item'].'.'.$lastItem['extension'])){
									//unlink($thumbnail['dest'] . Sanitize :: keepValidChars($lastItem['value']) . '-'.$lastItem['id_item'].'.'.$lastItem['extension']);
								}
							}
						}
						else if(isset($file['dest'])){
							if(file_exists($file['dest'] . Sanitize :: keepValidChars($lastItem['value']) . '-'.$lastItem['id_item'].'.'.$lastItem['extension'])){
								//unlink($file['dest'] . Sanitize :: keepValidChars($lastItem['value']) . '-'.$lastItem['id_item'].'.'.$lastItem['extension']);
							}
						}
						*/
						
						$result = Database :: query('insert into '.BDD.'item(id_item, value, extension) VALUES("", "'.mysql_real_escape_string(${'name_'.$key}).'", "'.mysql_real_escape_string(strtolower(${'extension_'.$key})).'")');
						${'idItem_'.$key} = Database :: getGeneratedId();
						
						$idItemUpdated .= ((!empty($valueSql)) ? ', ':null).$file['idName'].'='.${'idItem_'.$key};
					}
				}
			}
			else{
				if($fileToUpload['size'] > 0){
					
					$lastIdItem = Database :: get('select id_item from '.BDD. $tableName.' where '.$id['name'].'="'.mysql_real_escape_string($id['value']).'"');
					$lastItem = array();
					Database :: getLine('select id_item, value, extension from '.BDD.'item where id_item="'.mysql_real_escape_string($lastIdItem).'"', $lastItem);
					
					//DELETE OLD ITEM
					//Database :: query('delete from '.BDD.'item where id_item="'.mysql_real_escape_string($lastIdItem).'"');
					
					
					//SUPPRESSION DES FICHIERS A FAIRE
					/*
					if(count($thumbnails) > 0){
						foreach($thumbnails as $thumbnail){
							if(file_exists($thumbnail['dest'] . Sanitize :: keepValidChars($lastItem['value']) . '-'.$lastItem['id_item'].'.'.$lastItem['extension'])){
								unlink($thumbnail['dest'] . Sanitize :: keepValidChars($lastItem['value']) . '-'.$lastItem['id_item'].'.'.$lastItem['extension']);
							}
						}
					}
					else if(isset($fileToUpload['dest'])){
						if(file_exists($fileToUpload['dest'] . Sanitize :: keepValidChars($lastItem['value']) . '-'.$lastItem['id_item'].'.'.$lastItem['extension'])){
							unlink($fileToUpload['dest'] . Sanitize :: keepValidChars($lastItem['value']) . '-'.$lastItem['id_item'].'.'.$lastItem['extension']);
						}
					}
					*/
					
					$result = Database :: query('insert into '.BDD.'item(id_item, value, extension) VALUES("", "'.mysql_real_escape_string($name).'", "'.mysql_real_escape_string(strtolower($extension)).'")');
					$idItem = Database :: getGeneratedId();
					
					$idItemUpdated = ((!empty($valueSql)) ? ', ':null).'id_item='.$idItem;
				}
			}
		}
		
		//UPDATE BDD
		$result = Database :: query('update '.BDD.$tableName . ' set '.substr($valueSql, 0, -2) . $idItemUpdated . ' where '.$id['name'].'="'.mysql_real_escape_string($id['value']).'"');
		
		if(!empty($fileToUpload)){
			if(isset($fileToUpload[0])){
				foreach($fileToUpload as $key => $file){
					if($file['size'] > 0){
						//Upload File
						$result = Picture :: upload($file, $file['dest'] . '/' . Sanitize :: keepValidChars(${'name_'.$key}) . '-' . ${'idItem_'.$key}, $error);
						if($result !== true){ Database :: query('ROLLBACK;');return $result;}
						
						
						if(count($thumbnails) > 0 && isset($thumbnails[$key])){
							foreach($thumbnails[$key] as $thumbnail){
								
								if(isset($thumbnail['createFolder']) && $thumbnail['createFolder']){
									if(!file_exists($thumbnail['dest'])){
										mkdir($thumbnail['dest']);
									}
								}
								
								$maxHeight = (isset($thumbnail['maxHeight'])) ? $thumbnail['maxHeight'] : null;
								
								//Create Thumbnail
								$result = Picture :: createThumb($thumbnail['source'], $thumbnail['dest'], Sanitize :: keepValidChars(${'name_'.$key}) . '-' . ${'idItem_'.$key} . '.'. strtolower(${'extension_'.$key}), null, $thumbnail['width'], $thumbnail['height'], null, $maxHeight);
								if($result !== true){ Database :: query('ROLLBACK;'); $error .= 'L\'image contient une erreur et ne peux être redimentionée.'; return $result;}
							}
						}
					}
				}
			}
			else{
				
				if($fileToUpload['size'] > 0){
					//Upload File
					$result = Picture :: upload($fileToUpload, $fileToUpload['dest'] . '/' . Sanitize :: keepValidChars($name) . '-' . $idItem, $error);
					if($result !== true){ Database :: query('ROLLBACK;');return $result;}
					
					
					if(count($thumbnails) > 0){
						foreach($thumbnails as $thumbnail){
							
							if(isset($thumbnail['createFolder']) && $thumbnail['createFolder']){
								if(!file_exists($thumbnail['dest'])){
									mkdir($thumbnail['dest']);
								}
							}
							
							$maxHeight = (isset($thumbnail['maxHeight'])) ? $thumbnail['maxHeight'] : null;
							
							//Create Thumbnail
							$result = Picture :: createThumb($thumbnail['source'], $thumbnail['dest'], Sanitize :: keepValidChars($name) . '-' . $idItem . '.'. strtolower($extension), null, $thumbnail['width'], $thumbnail['height'], null, $maxHeight);
							if($result !== true){ Database :: query('ROLLBACK;'); $error .= 'L\'image contient une erreur et ne peux être redimentionée.'; return $result;}
						}
					}
				}
			}
		}
		
		//END Transaction
		Database :: query('COMMIT;');
		
		return $result;
	}
	
	public static function delete($tableName, $id, $pictureInfos, &$error){
		
		if(count($pictureInfos) > 0){
			$idItem = Database :: get('select id_item from '.BDD.$tableName.' where '.$id['name'].'="'.mysql_real_escape_string($id['value']).'"');
			$item = array();
			Database :: getLine('select id_item, value, extension from '.BDD.'item where id_item="'.mysql_real_escape_string($idItem).'"', $item);
			
			foreach($pictureInfos as $picture){
				if(file_exists($picture['dest'] . Sanitize :: keepValidChars($item['value']) . '-'.$item['id_item'].'.'.$item['extension'])){
					unlink($picture['dest'] . Sanitize :: keepValidChars($item['value']) . '-'.$item['id_item'].'.'.$item['extension']);
				}
			}
			
			$result = Database :: query('delete from '.BDD.'item where id_item="'.mysql_real_escape_string($idItem).'"');
		}
		
		$result = Database :: query('delete from '.BDD.$tableName.' where '.$id['name'].'="'.mysql_real_escape_string($id['value']).'"');
		
		return $result;
	}
	
	
	//Options
	public static function updateOption($fields, &$error){
		
		$allLines = array();
		$request = 'select `key`, `value` from '.BDD.'option';
		Database :: getSimpleArray($request, $allLines);
		
		if(count($fields) > 0){
			foreach($fields as $key => $field){
				$isPassword = false;
				if($field['type'] == 'password') $isPassword = true;
				
				if(!empty($field['value']) || $field['isMandatory']){
					
					switch($field['type']){
						case 'string' : 
							if(!Validation :: string($field['value'])){ $error .= 'Le champ '.$field['name'].' ne doit pas être vide.'; return false; }
						break;
						case 'number' : 
							if(!Validation :: integer($field['value'])){ $error .= 'Le champ '.$field['name'].' doit être un nombre.'; return false; }
						break;
						case 'email' : 
							if(!Validation :: email($field['value'])){ $error .= 'Le champ '.$field['name'].' doit être un email.'; return false; }
						break;
						case 'password' : 
							$field['value'] = 'MD5("'.mysql_real_escape_string($field['value']).'"), ';
						break;
						case 'youtube' : 
							$value = Validation :: youtube($field['value']);
							if(!$value){ $error .= 'Le champ '.$field['name'].' est incorrect.'; return false; }
							else $field['value'] = $value;
						break;
						case 'date-d/m' : 
							if(preg_match('#^[0-9]{0,2}\/[0-9]{0,2}$#', $field['value'], $matches)) $field['value'] = date('Y').'-'.substr($field['value'], 3, 2).'-'.substr($field['value'], 0, 2).' '.date('H:i:s');
							else{ $error .= 'Le champ '.$field['name'].' est incorrect.'; return false; } 
						break;
						case 'date-d/m/Y' : 
							if(preg_match('#^[0-9]{0,2}\/[0-9]{0,2}\/[0-9]{0,4}$#', $field['value'], $matches)) $field['value'] = substr($field['value'], 6, 4).'-'.substr($field['value'], 3, 2).'-'.substr($field['value'], 0, 2).' '.date('H:i:s');
							else{ $error .= 'Le champ '.$field['name'].' est incorrect.'; return false; } 
						break;
					}
				}
				
				if(isset($allLines[$key])){
					$result = Database :: query('update '.BDD.'option set `value`="'.mysql_real_escape_string($field['value']).'" where `key`="'.mysql_real_escape_string($key).'"');
					if(!$result) return false;
				}
				else{
					$result = Database :: query('insert into '.BDD.'option(`key`, `value`) values("'.mysql_real_escape_string($key).'", "'.mysql_real_escape_string($field['value']).'")');
					if(!$result) return false;
				}
				
			}
		}
		
		return true;
		
	}
	
	
	//Options
	public static function deleteOption($fields, &$error){
		
		$result = false;
		
		if(count($fields) > 0){
			foreach($fields as $field){
				$result = Database :: query('delete from '.BDD.'option where `key`="'.mysql_real_escape_string($field).'"');
			}
		}
		
		return $result;
		
	}
	
}


