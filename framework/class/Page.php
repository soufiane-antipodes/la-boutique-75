<?

/**
* Class Page
*
* Permet de gerer des variables de configurations de la page
*
* @author Julien Guion
* @version 1.0
*/
class Page{
	
	private static $_title = TITLE_BY_DEFAULT;
	private static $_meta_description = META_DESCRIPTION_BY_DEFAULT;
	private static $_posMenu = 0;
	private static $_header = 'header.php';
	private static $_footer = 'footer.php';
	private static $_isAdmin = false;
	private static $_path = array(0=> PATH_LOAD, 1=> PATH_WWW_ADMIN_LOAD);
	private static $_dashboardMenu = array();
	private static $_classBody = array();
	private static $_meta_image = IMG_DEFAULT;
	private static $_meta_fb_description = '';
	
	/**
	* GETTERS
	*/
	public static function getTitle(){
		return self::$_title;
	}
	
	public static function getMetaDescription(){
		return self::$_meta_description;
	}
	
	public static function getIndexMenu(){
		return self::$_posMenu;
	}
	
	public static function getHeader(){
		return ((self::$_isAdmin == true) ? self::$_path[1] : self::$_path[0]) . self :: $_header;
	}
	
	public static function getFooter(){
		return ((self::$_isAdmin == true) ? self::$_path[1] : self::$_path[0]) . self :: $_footer;
	}
	
	public static function getDashboardMenu(){
		return self::$_dashboardMenu;
	}
	
	public static function getClassBody(){
		return implode(' ', self::$_classBody);
	}
	
	public static function getMetaImage(){
		return self::$_meta_image;
	}
	
	public static function getMetaFbDescription(){
		if(!empty(self :: $_meta_fb_description)){
			return self::$_meta_fb_description;
		}else{
			return self :: getMetaDescription();
		}
		
	}
	
	/**
	* SETTERS
	*/
	
	public static function setAdmin($isAdmin){
		self::$_isAdmin = (bool) $isAdmin;
	}
	
	public static function setTitle($newTitle){
		self::$_title = $newTitle;
	}
	
	public static function setMetaDescription($newDescription){
		global $globals;
		
		if(empty($newDescription)){
			self::$_meta_description = $globals['META_DESC_DEFAULT'];
		}else{
			self::$_meta_description = $newDescription;
		}
	}
	
	public static function setIndexMenu($position){
		self::$_posMenu = $position;
	}
	
	public static function setHeader($header){
		self :: $_header = $header;
	}
	
	public static function setFooter($footer){
		self :: $_footer = $footer;
	}
	
	public static function setDashboardMenu($dashboardMenu){
		self :: $_dashboardMenu = $dashboardMenu;
	}
	
	public static function setClassBody($classBody){
		if(is_array($classBody)) self :: $_classBody = array_merge($classBody, self :: $classBody);
		else self :: $_classBody[] = $classBody;
	}
	
	public static function setMetaImage($meta_image){
		self :: $_meta_image = $meta_image;
	}
	
	public static function setMetaFbDescription($meta_fb_description){
		self :: $_meta_fb_description = $meta_fb_description;
	}
	
}