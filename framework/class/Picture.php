<?php

/**
* Class Picture
*
* Permet de manipuler les images
*
* @author Julien Guion
* @version 1.0
*/
class Picture{
	
	private static $_extensions = array('.jpg', '.JPG', '.png');
	private static $_maxSize = 64000000;
	private static $_fileNameIsFormated = false;
	private static $_isOkForUpload = false;
		
	/**
	* Cree une image miniature
	*
	*	@param mixed $pictures
	*	@param string $source
	*	@param string $destination
	*	@param integer $index
	*	@param integer $width
	*	@param integer $height
	*	@return boolean
	*/
	public static function createThumb($source, $destination, $fileName, $thumbPrefix, $width, $height, $maxWidth=null, $maxHeight=null){
		return CreateThumb($source, $fileName, $destination, $thumbPrefix, $width, $height, $maxWidth, $maxHeight);
	}
	
	/**
	* Recupere la picture si celle est presente physiquement - si elle n'existe pas on retourne $_noPicture
	*
	*	@param string $path
	*	@return string
	*/
	public static function getPicture($path){
		return (file_exists($path)) ? $path : null;
	}
	
	/**
	* Test l'upload d'une image.
	*
	*	@param string $path
	*	@return boolean
	*/
	public static function upload($uploadFile, $destination, &$error){
		
		$result = false;
		
		$extension = strrchr($uploadFile['name'], '.'); 
		
		if(!move_uploaded_file($uploadFile['tmp_name'], $destination . strtolower($extension)))
			$error .= 'Problem when sending<br />';
		else{
			$result = true;
		}
		
		return $result;
	}
	
	/**
	* Effectue tous les test d'upload de fichier
	*
	*	@param string $path
	*	@return boolean
	*/
	public static function isOkForUpload($uploadFile, &$error){

		if(self::isVerifUploadIsGood()) return true;
		
		$result = false;
		
		if(count($uploadFile) < 1){
			$error .= 'Le fichier est obligatoire<br />';
			return false;
		}

		
		if($uploadFile['size'] < 1){
			$error .= 'Le fichier est obligatoire<br />';
			return false;
		}
		
		$size = filesize($uploadFile['tmp_name']);
		$extension = strrchr($uploadFile['name'], '.'); 
		
/*		echo '<pre>', print_r(self::$_extensions, true), '</pre>';
		echo '<pre>', print_r($extension, true), '</pre>';
		die();*/

		//Début des vérifications de sécurité...
		if(!in_array($extension, self::$_extensions)){
			$error .= 'Le format doit etre : '.implode(',', self::$_extensions).'<br />';
			return false;
		}
	
		if($size > self::$_maxSize){
			$error .= 'Le fichier est trop volumnineux, maximum '.floor(self::$_maxSize/1000000).'Mo<br />';	
			return false;
		}
		
		if($uploadFile['error']){
			switch ($uploadFile['error']){    
				case 1:
					$error .= 'Le fichier est trop volumnineux, maximum '.floor(self::$_maxSize/1000000).'Mo<br />';
				break;    
				
				case 2:
					$error .= 'Le fichier est trop volumnineux, maximum '.floor(self::$_maxSize/1000000).'Mo<br />';
				break;    
				
				case 3:
					$error .="Le transfert a été interrompu !<br />";    
				break;    
				
				case 4:
					$error .="Taille vide !<br />";
				break;  
			}
		}
		
		if($error === null){
			$result = true;
			self::$_isOkForUpload = true;
		}
		
		return $result;
		
	}
	
	public static function uploadFile($uploadFile, $destination, $fileName, &$error, $createThumb=array()){
		
		$result = false;
		
		$result = self :: isOkForUpload($uploadFile, $error);
		if($result !== true) return $result;   

		
		$result = self :: upload($uploadFile, $destination . $fileName, $error);
		if($result !== true) return $result;


		if(is_array($createThumb) && count($createThumb) > 0){
			foreach($createThumb as $thumb){
				
				$result = self :: createThumb($thumb['source'], $thumb['destination'], $thumb['fileName'], $thumb['prefix'], $thumb['width'], $thumb['height']);
				if($result !== true) return $result;
				
			}
			
		}
		
		return $result;
	}
	
	public static function formatFileName($fileName, $forced=false){
		if(!self::fileNameIsFormated() || $forced){
			$fileName = Sanitize :: keepValidChars($fileName);
			$fileName = preg_replace('/([^.a-z0-9]+)/i', '_', $fileName);
			$fileName = strtolower($fileName);
			self::$_fileNameIsFormated = true;
		}
		return $fileName;
	}
	
	public static function fileNameIsFormated(){
		return self::$_fileNameIsFormated;
	}
	
	public static function isVerifUploadIsGood(){
		return self::$_isOkForUpload;
	}
	
	public static function setMaxSize($maxSize){
		self::$_maxSize = $maxSize;
	}
	
	public static function setExtensions($extensions){
		self::$_extensions = $extensions;
	}
	
	public static function getPictureCollection($id){
		$collection = array();
		Database :: getLine('select nameFR from '.BDD.'t_collection where idCollection="'.mysql_real_escape_string($id).'"', $collection);
		return PATH_HTTP_IMG_COLLECTION . Sanitize :: keepValidChars($collection['nameFR']);
	}
	
	public static function getPhysicalPictureCollection($id){
		$collection = array();
		Database :: getLine('select nameFR from '.BDD.'t_collection where idCollection="'.mysql_real_escape_string($id).'"', $collection);
		return PATH_IMG_COLLECTION . Sanitize :: keepValidChars($collection['nameFR']);
	}
	
	
	public static function getPictureItem($id, $type='small'){
		$item = array();
		Database :: getLine('select idItem, C.nameFR as collectionName, isPress, value, extension from '.BDD.'t_items I left join '.BDD.'t_collection C on C.idCollection=I.idCollection where I.idItem="'.mysql_real_escape_string($id).'"', $item);
		
		if($item['isPress'] == 1)
			return PATH_HTTP_IMG_PRESS . $type . '/'.Sanitize :: keepValidChars($item['value']) . '-' . $item['idItem'] . '.' . $item['extension'];
		else
			return PATH_HTTP_IMG_COLLECTION . Sanitize :: keepValidChars($item['collectionName']) . '/' . Sanitize :: keepValidChars($item['value']) . '-' . $item['idItem'] . '.' . $item['extension'];
	}
	
	public static function getPhysicalPictureItem($id, $bigger=false){
		$item = array();
		Database :: getLine('select idItem, C.nameFR as collectionName, isPress, value, extension from '.BDD.'t_items I left join '.BDD.'t_collection C on C.idCollection=I.idCollection where I.idItem="'.mysql_real_escape_string($id).'"', $item);
		
		if($item['isPress'] == 1)
			return PATH_IMG_PRESS . Sanitize :: keepValidChars($item['value']) . '-' . $item['idItem'] . '.' . $item['extension'];
		else
			return PATH_IMG_COLLECTION . Sanitize :: keepValidChars($item['collectionName']) . '/' . Sanitize :: keepValidChars($item['value']) . '-' . $item['idItem'] . '.' . $item['extension'];
	}
	
}