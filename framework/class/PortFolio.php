<?php

Class PortFolio{
	
	public static function newFolder($idCategory, $fileName, &$error){
		
		$result = false;
		$position = Position :: generatePosition('t_categories', 'idSubCategory="'.$idCategory.'" and lang="'.mysql_real_escape_string(Session :: get('langAdminToUse')).'"', 'end');
		
		$result = Database :: query('insert into '.BDD.'t_categories(idCategory, idSubCategory, name, lang, position) values("", "'.mysql_real_escape_string($idCategory).'", "'.mysql_real_escape_string(ucwords($fileName)).'", "'.mysql_real_escape_string(Session :: get('langAdminToUse')).'", "'.$position.'")');
		if($result !== true) return $result;
			
		return $result;
	}
	
	public static function newFilePicture($uploadFile, $idCategory, $fileName, &$error){
		Picture :: setExtensions(array('.jpg', '.JPG', '.png', '.xls', '.xlsx', '.pdf', '.PDF', '.txt', '.doc', '.docx', '.zip', '.stl'));
		$result = Picture :: isOkForUpload($uploadFile, $error);
		if($result !== true) return $result;
		
		if(empty($fileName)){ $error .= 'Nom vide.'; return false; }
		
		$extension = str_replace('.', '', strrchr($uploadFile['name'], '.'));
		
		$result = Database :: query('insert into '.BDD.'t_items(idItem, value, extension) VALUES("", "'.mysql_real_escape_string($fileName).'", "'.mysql_real_escape_string(strtolower($extension)).'")');
		$idItem = Database :: getGeneratedId();
		$position = Position :: generatePosition('t_categories', 'idSubCategory="'.$idCategory.'" and lang="'.mysql_real_escape_string(Session :: get('langAdminToUse')).'"', 'end');
		
		$result = Database :: query('insert into '.BDD.'t_categories(idCategory, idSubCategory, name, idItem, lang, position) values("", "'.mysql_real_escape_string($idCategory).'", "'.mysql_real_escape_string($fileName).'","'.$idItem.'", "'.mysql_real_escape_string(Session :: get('langAdminToUse')).'", "'.$position.'")');
		if($result !== true) return $result;
		
		$result = Picture :: upload($uploadFile, PATH_IMG_DOCUMENTATION . Sanitize :: keepValidChars($fileName) . '-' . $idItem, $error);
		if($result !== true){ Database :: query('ROLLBACK;');return $result;}
			
		return $result;
	}
	
	public static function newFileMovie($idCategory, $fileName, $url, &$error){
		
		if(empty($fileName)){
			$error .= 'Le nom de la vidéo est vide <br />';
			return false;
		}
		
		$link = Validation :: youtube($url);
		if(!$link){ $error .='Lien Youtube incorrect'; return false; }
		
		$position = Position :: generatePosition('t_categories', 'idSubCategory="'.$idCategory.'" and lang="'.mysql_real_escape_string(Session :: get('langAdminToUse')).'"', 'end');
		$result = Database :: query('insert into '.BDD.'t_categories(idCategory, idSubCategory, name, link, lang, position) values("", "'.mysql_real_escape_string($idCategory).'", "'.mysql_real_escape_string(ucwords($fileName)).'", "'.mysql_real_escape_string($link).'", "'.mysql_real_escape_string(Session :: get('langAdminToUse')).'", "'.$position.'")');
		
		
		return $result;
	}
	
	public static function editName($name, $id, &$error){
		if(empty($name)){
			$error .= 'Nom vide';
			return false;
		}
		
		return Database :: query('update '.BDD.'t_categories set name="'.mysql_real_escape_string($name).'" where idCategory="'.mysql_real_escape_string($id).'"');
		
	}
	
}