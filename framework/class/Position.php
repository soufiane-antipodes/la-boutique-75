<?php
Class Position{
	
	public static function generatePosition($table, $condition, $position='end'){
		
		$result = false;
		
		$where = (!empty($condition)) ? ' where '.$condition : '';
		
		switch($position){
			case 'first': 
				$result = Database :: query('update '.BDD.$table.' set position=position+1');
			break;
			case 'end' : 
				$result = Database :: get('select max(position)+1 from '.BDD.$table.$where);
				$result = ($result > 0) ? $result : 1;
			break;
			default : break;
		}
		
		return $result;
		
	}
	
}


