<?php
/**
 * @class Request
 * @brief Class to manage HTTP Request
 * 
 * You can use it instead of use param or $_POST...
 *
 * @date        march 2011
 * @author      Julien Guion
 * @copyright   BMX Family
 * @version     1
 */
class Request {
    
    private static $_actionField = 'action';

    /**
     * Is there request ?
     * @return Boolean
     */
    public static function hasRequest($type=null) {
        if(is_null($type)) {
            return ( (isset($_POST) && !empty($_POST)) || (isset($_GET) && !empty($_GET)) || (isset($_FILES) && !empty($_FILES)));
        } else {
            $type   = strtoupper($type);
            if($type == 'GET')  return isset($_GET) && !empty($_GET);
            if($type == 'POST') return (isset($_POST) && !empty($_POST)) || (isset($_FILES) && !empty($_FILES)) ;
        }
    }
    

    /**
     * Get all DATA for the current HTTP request
     * @param boolean $withAction Data contains action
     * @return array
     */
    public static function getData($withAction=false) {
        $tData  = array();
        if(isset($_POST)) $tData    = array_merge($tData, $_POST);
        if(isset($_GET)) $tData     = array_merge($tData, $_GET);
        foreach($tData as $key=>$data) $tData[$key] = self::getField($key);
        if(!$withAction) {
            if(isset($tData['action'])) unset($tData['action']);
        }
        return $tData;
    }
    
    /**
     * Determines if a field exists
     * @param string $field
     * @return boolean
     */   
	public static function fieldExists($field)
	{
		return isset($_GET[$field]) || isset($_POST[$field]);
	}

    /**
     * Get value from $_POST or $_GET
     * @param string $field
     * @param string $default
     * @param string $from post|get|null(both)
     * @return string
     */
    public static function getField($field, $default=null, $from=null) {

        $value      = '';
        switch(strtoupper($from)){
            case 'POST':
                $value  = isset($_POST[$field]) ? $_POST[$field] : '';
                break;
            case 'GET':
                $value  = isset($_GET[$field]) ? $_GET[$field] : '';
                break;
            default:
            	if(isset($_POST[$field]))   	$value  = $_POST[$field];
                elseif(isset($_GET[$field]))	$value  = $_GET[$field];
                break;
        }

        if(is_null($value) || empty($value)) {
            return !is_null($default) ? $default : null;
        }
        
        if(is_array($value)) return $value;

        if(is_string($value))   $value  = trim($value);
        return htmlspecialchars(trim($value));
    }

    

    /**
     * Get Int value from get or post
     * @param string $field
     * @return integer $value
     */
    public static function getInt($field, $default=null) {
        return (int) self::getField($field, $default);
    }

    /**
     * Get Array value from get or post
     * @param string $field
     * @return array $value
     */
    public static function getArray($field ,$default=array()) {
        return (array) self::getField($field, $default);
    }

    /**
     * Get Int value from get or post
     * @param string $field
     * @return float $value
     */
    public static function getFloat($field, $default=null) {
        return (float) self::getField($field, $default);
    }

    /**
     * Get Int value from get or post
     * @param string $field
     * @return string $value
     */
    public static function getString($field, $default=null) {
        return (string) self::getField($field, $default);
    }

    /**
     * Get Bool value from get or post
     * @param string $field
     * @return string $value
     */
    public static function getBool($field, $default=null) {
        return (bool) self::getField($field, $default);
    }

    /**
     * Clear $_POST, $_GET
     */
    public static function clear() {
        $_GET   = $_POST    = array();
    }

    /**
     * Check if the current HTTP Request is AJAX
     * @return boolean
     */
    public static function isAjax() {
    	
    	if(self::fieldExists('ajax'))
    		return self::getField('ajax');
    	else
        	return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] != 'XMLRequest';
    }

    /**
     * Get Navigator language
     * @return string $language
     */
    public static function getLanguage() {
        $lang      = $_SERVER['HTTP_ACCEPT_LANGUAGE'];
        if(!empty($lang))   $lang   = strtoupper( substr($lang, 0, 2) );
        if(empty($lang))    $lang   = 'EN';
        return $lang;
    }


    /**
     * Modify Request
     * @param string $field
     * @param string $value
     * @global string $GLOBALS['_GET']
     * @name $_GET
     */
    public static function setField($field, $value, $get = false) {
		if($get)
			$_GET[$field]	= $value;
		else
			$_POST[$field]	= $value;
    }

    /**
     * Set action
     * @param string $value
     */
    public static function setAction($value) {
        self::setField( self::$_actionField, $value);
    }
    
    /**
     * Get action
     * @param string $default
     * @return string
     */
    public static function getAction($default=null) {
        return self::getField( self::$_actionField, $default);
    }

    /**
     * Set name of action field
     * @param string $name
     */
    public static function setActionField($name) {
       self::$_actionField  = $name;
    }

    /**
     * get action fieldName
     * @return string
     */
    public static function getActionField() {
       return (string) self::$_actionField;
    }
    
    /**
    * Redirect to
    * @return void
    */
    public static function redirectTo($url){
    	
    	if (!headers_sent())
        header('Location: '.$url);
	    else {
	        echo '<script type="text/javascript">'
	            .'window.location.href="'.$url.'";'
	            .'</script>'
	            .'<noscript>'
	            .'<meta http-equiv="refresh" content="0;url='.$url.'" />'
	            .'</noscript>';
	    }
	    exit;
    }
}
?>
