<?php
/**
 * @class Sanitize
 * @brief Class Zanitize allows to sanitize string, int...
 *
 * @package     Core
 * @date        February 2010
 * @author      Jean-François LEPINE
 * @copyright   Believe Digital
 * @version     1
 * @todo        stripHexa()
 */
abstract class Sanitize {


    const ALLOWED_TAGS          = '<html><head><body><style><p><div><a><em><u><i><strike><b><strong><font><img><ul><li><ol><br><table></span><tr><td>';
    const ALLOWED_ATTRIBUTES    = 'href,rel,rev,width,height,hmargin,vmargin,color,family,face,src,alt,title,align,border,size';

    /**
     * Clean HTML content to keep only current html tags
     * @param string $string
     * @param string $allowedTags
     * @param string $allowedAttributes
     * @return string
     */
    public static function cleanHtml($string, $allowedTags = self::ALLOWED_TAGS, $allowedAttributes = self::ALLOWED_ATTRIBUTES) {
        return self::_strip_tags_attributes($string, self::ALLOWED_TAGS, self::ALLOWED_ATTRIBUTES);
    }

    /**
     * Clear HTML content to remove all HTML content
     * @param string $string
     * @param string $allowedTags
     * @param string $allowedAttributes
     * @return string
     */
    public static function removeHtml($string ) {
        $string     = preg_replace('~(</?)(\w*)((/(?!>)|[^/>])*)(/?>)~', '', (string) $string);
        $string     = preg_replace('!([<>])!', '', $string);
        $string     = strip_tags($string);
        return $string;
    }

    /**
     * Remove all comments of a string
     * @param string $string
     * @return string $string
     */
    public static function removeComments($string) {
        $string     = preg_replace('#<!--(?:[^<]+|<(?!\!--))*?(--\s*>)#sm', '', (string) $string);
        $string     = preg_replace('!/\*(.*)?\*/!sm', '', $string);
        return $string;
    }

    /**
     * Returns the string $value, removing all but alphabetic characters
     * @param  string $value
     * @return string
     */
    public static function string($string, $allowWhiteSpace=true) {
        $whiteSpace = $allowWhiteSpace ? '\s' : '';
        $pattern = '/[^\p{L}\p{N}' . $whiteSpace . ']/u';
        return preg_replace($pattern, '', (string) $string);
    }


    /**
     * Returns the string $value, removing all but alphabetic characters
     * Found in Zend Framework
     * @param  string $value
     * @return string
     */
    public static function alpha($value, $allowWhiteSpace=true) {
        $whiteSpace = $allowWhiteSpace ? '\s' : '';
        if (!Server::unicodeEnabled()) {
            // POSIX named classes are not supported, use alternative a-zA-Z match
            $pattern = '/[^a-zA-Z' . $whiteSpace . ']/';
        } else {
            //The Alphabet means each language's alphabet.
            $pattern = '/[^\p{L}' . $whiteSpace . ']/u';
        }
        return preg_replace($pattern, '', (string) $value);
    }

    /**
     * strip_tags function, but with attributes
     * @link http://fr.php.net/manual/en/function.strip-tags.php
     * @param string $string
     * @param string $allowtags
     * @param string $allowattributes
     * @example strip_tags_attributes($string,'<strong><em><a>','href,rel');
     */
    private static function _strip_tags_attributes($string,$allowtags=null,$allowattributes=null){
        $string = strip_tags($string,$allowtags);
        if (!is_null($allowattributes)) {
            if(!is_array($allowattributes))
                $allowattributes = explode(",",$allowattributes);
            if(is_array($allowattributes))
                $allowattributes = implode(")(?<!",$allowattributes);
            if (strlen($allowattributes) > 0)
                $allowattributes = "(?<!".$allowattributes.")";
            $string = preg_replace_callback("/<[^>]*>/i",create_function(
                '$matches',
                'return preg_replace("/ [^ =]*'.$allowattributes.'=(\"[^\"]*\"|\'[^\']*\')/i", "", $matches[0]);'
            ),$string);
        }
        return $string;
    }

    /**
     * Sanitize number
     * @param mixed $value
     * @return string
     */
    public static function number($value) {
        if(is_array($value) || is_object($value)) return 0;
        if(Server::unicodeEnabled()) {
            // POSIX named classes are not supported, use alternative 0-9 match
            $pattern = '/[^0-9]/';
        } else if (extension_loaded('mbstring')) {
            // Filter for the value with mbstring
            $pattern = '/[^[:digit:]]/';
        } else {
            // Filter for the value without mbstring
            $pattern = '/[\p{^N}]/';
        }
        $value  = preg_replace($pattern, '', (string) $value);
        return $value;
    }
    
    /**
     * Returns $value without newline control characters
     * @param  string $value
     * @return string
     */
    public static function stripNewLines($string) {
        return str_replace(array("\n", "\r"), '', (string) $string);
    }
    
    /**
		* Permet de garder que des caractères valides
		*
		* @param string $string
		*	@return string $string
		*/
		public static function keepValidChars($toClean){
			
			$toRemplace = array(
							    'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',
							    'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',
							    'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',
							    'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',
							    'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',
							    'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',
							    'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f'
							);
							
			$toClean		 =		 strtr($toClean, $toRemplace);
	    $toClean     =     str_replace('&', 'and', $toClean);
	    $toClean     =     trim(preg_replace('/[^\w\d_ -]/si', '', $toClean));
	    $toClean     =     str_replace('--', '-', $toClean);
	    $toClean     =     str_replace('-', '_', $toClean);
	    $toClean     =     str_replace('.', '_', $toClean);
	    $toClean     =     str_replace(' ', '-', $toClean);
	    $toClean     =     strtolower($toClean);
	   	
	    return $toClean;
		}
		
		public static function keepValidCharsRewriting($toClean){
			
			$toClean = self :: keepValidChars($toClean);
			$toClean     =     str_replace('amp', '', $toClean);
			
	    return $toClean;
		}

    /**
     * Returns $value without accents
     * @param  string $value
     * @return string
     */
    public static function stripAccents($string) {
        $accent='ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËéèêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ';
        $noaccent='AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn';
        $string = strtr($string,$accent,$noaccent);
        return $string;
    }

    /**
     * Sanitize string for shell using
     * @param string $string
     * @return string $string
     */
    public static function shell($string) {
        //$string     = self::stripHexa((string) $string);
        $string     = escapeshellcmd($string);
        return $string;
    }

    /**
     * Remove all hexadecim values
     * @param string $string
     * @return string $string
     */
    public static function stripHexa($string) {
        $string     = (string) $string;
        $tToEscape  = array('\x0A', '\x00', '\XFF');
        $regex  = '!['.implode('|', $tToEscape).']!i';
        $value  = preg_replace($regex, '', $string);
        return $value;
    }


    /**
     * Escape XSS
     * It should be more complicated, but xss total protection doesn't exist
     * @param string $string
     * @return string
     */
    public static function xss($string) {
        $string     = (string) $string;
        $encoding   = Believe_Config::get('encode.charset');
        if(empty($encoding)) $encoding  = iconv_get_encoding('input_encoding');
        if(empty($encoding)) $encoding  = 'ISO-8859-1';
        return htmlentities($string, ENT_QUOTES, $encoding);
    }
    
    public static function yesText($bool){
    	return ($bool == 1) ? '<span class="bold color-green-flash">OUI</span>' : '<span class="bold color-red-flash">NON</span>';
    }
    
    /**
     * Sanitize Url
     * @param string $string
     * @return string $string
     */
    public static function getGoodUrl($string) {
    	$isInternal = false;
        
        if(preg_match('#'.$_SERVER['HTTP_HOST'].'#', $string)){
					$string = urldecode($string);
					$string = basename($string);
					$string = str_replace('#', '', $string);
					$string = str_replace('?', '.php?', $string);
					$isInternal = true;
				}
        return array('url' => $string, 'isInternal' => $isInternal);
    }
    
    /**
     * Sanitize Links
     * @param string $string
     * @return string $string
     */
    public static function getGoodLinks($string) {
    	return str_replace('target="_self"', 'rel="alternate"', $string);
    }
    
    public static function getWords($string,$nbWords){ 
       $tab=explode(" ",$string); 
       $result = '';
       for($i=0;$i < $nbWords;$i++) 
       { 
          $result.=' '.$tab[$i]; 
       }
       return $result; 
    }
    
    /**
     * Remove all tags
     * @param string $string
     * @param boolean $remove_breaks
     * @return string $string
     */
    public static function strip_all_tags($string, $remove_breaks = false) {
			$string = preg_replace( '@<(script|style)[^>]*?>.*?</\\1>@si', '', $string );
			$string = strip_tags($string);
		
			if ( $remove_breaks )
				$string = preg_replace('/[\r\n\t ]+/', ' ', $string);
		
			return trim( $string );
		}
    
    /**
     * Get only NB words what you want.
     * @param string $string
     * @param string $num_words
     * @return string $string
     */
    public static function html_excerpt( $str, $count ) {
			$str = self::strip_all_tags( $str, true );
			$str = mb_substr( $str, 0, $count );
			// remove part of an entity at the end
			$str = preg_replace( '/&[^;\s]{0,6}$/', '', $str );
			return $str;
		}
		
		/**
     * Trim words
     * @param string $text
     * @param integer $num_words
     * @return string $text
     */
		public static function trim_words( $text, $num_words = 55) {
			$original_text = $text;
			$text = self::strip_all_tags( $text );
			$text = trim( preg_replace( "/[\n\r\t ]+/", ' ', $text ), ' ' );
			preg_match_all( '/./u', $text, $words_array );
			$words_array = array_slice( $words_array[0], 0, $num_words + 1 );
			$sep = '';
				
			if ( count( $words_array ) > $num_words ) {
				array_pop( $words_array );
				$text = implode( $sep, $words_array );
			} else {
				$text = implode( $sep, $words_array );
			}
				return $text;
		}
		
		/**
     * Trim words
     * @param string $text
     * @param integer $num_words
     * @return string $text
     */
		public static function trim_word($text) {
			$text = str_replace(' ', '', $text);
			return $text;
		}
		
		public static function unEscapeHtml($unsafe) {
		     $unsafe = str_replace('&lt;', '<', $unsafe);
		     $unsafe = str_replace('&gt;', '>', $unsafe);
		     $unsafe = str_replace('&cent;', '¢', $unsafe);
		     $unsafe = str_replace('&pound;', '£', $unsafe);
		     $unsafe = str_replace('&euro;', '€', $unsafe);
		     $unsafe = str_replace('&yen;', '¥', $unsafe);
		     $unsafe = str_replace('&deg;', '°', $unsafe);
		     $unsafe = str_replace('&frac14;', '¼', $unsafe);
		     $unsafe = str_replace('&OElig;', 'Œ', $unsafe);
		     $unsafe = str_replace('&frac12;', '½', $unsafe);
		     $unsafe = str_replace('&oelig;', 'œ', $unsafe);
		     $unsafe = str_replace('&frac34;', '¾', $unsafe);
		     $unsafe = str_replace('&Yuml;', 'Ÿ', $unsafe);
		     $unsafe = str_replace('&iexcl;', '¡', $unsafe);
		     $unsafe = str_replace('&laquo;', '«', $unsafe);
		     $unsafe = str_replace('&raquo;', '»', $unsafe);
		     $unsafe = str_replace('&iquest;', '¿', $unsafe);
		     $unsafe = str_replace('&Agrave;', 'À', $unsafe);
		     $unsafe = str_replace('&Aacute;', 'Á', $unsafe);
		     $unsafe = str_replace('&Acirc;', 'Â', $unsafe);
		     $unsafe = str_replace('&Atilde;', 'Ã', $unsafe);
		     $unsafe = str_replace('&Auml;', 'Ä', $unsafe);
		     $unsafe = str_replace('&Aring;', 'Å', $unsafe);
		     $unsafe = str_replace('&AElig;', 'Æ', $unsafe);
		     $unsafe = str_replace('&Ccedil;', 'Ç', $unsafe);
		     $unsafe = str_replace('&Egrave;', 'È', $unsafe);
		     $unsafe = str_replace('&Eacute;', 'É', $unsafe);
		     $unsafe = str_replace('&Ecirc;', 'Ê', $unsafe);
		     $unsafe = str_replace('&Euml;', 'Ë', $unsafe);
		     $unsafe = str_replace('&Igrave;', 'Ì', $unsafe);
		     $unsafe = str_replace('&Iacute;', 'Í', $unsafe);
		     $unsafe = str_replace('&Icirc;', 'Î', $unsafe);
		     $unsafe = str_replace('&Iuml;', 'Ï', $unsafe);
		     $unsafe = str_replace('&ETH;', 'Ð', $unsafe);
		     $unsafe = str_replace('&Ntilde;', 'Ñ', $unsafe);
		     $unsafe = str_replace('&Ograve;', 'Ò', $unsafe);
		     $unsafe = str_replace('&Oacute;', 'Ó', $unsafe);
		     $unsafe = str_replace('&Ocirc;', 'Ô', $unsafe);
		     $unsafe = str_replace('&Otilde;', 'Õ', $unsafe);
		     $unsafe = str_replace('&Ouml;', 'Ö', $unsafe);
		     $unsafe = str_replace('&Oslash;', 'Ø', $unsafe);
		     $unsafe = str_replace('&Ugrave;', 'Ù', $unsafe);
		     $unsafe = str_replace('&Uacute;', 'Ú', $unsafe);
		     $unsafe = str_replace('&Ucirc;', 'Û', $unsafe);
		     $unsafe = str_replace('&Uuml;', 'Ü', $unsafe);
		     $unsafe = str_replace('&Yacute;', 'Ý', $unsafe);
		     $unsafe = str_replace('&THORN;', 'Þ', $unsafe);
		     $unsafe = str_replace('&szlig;', 'ß', $unsafe);
		     $unsafe = str_replace('&agrave;', 'à', $unsafe);
		     $unsafe = str_replace('&aacute;', 'á', $unsafe);
		     $unsafe = str_replace('&acirc;', 'â', $unsafe);
		     $unsafe = str_replace('&atilde;', 'ã', $unsafe);
		     $unsafe = str_replace('&auml;', 'ä', $unsafe);
		     $unsafe = str_replace('&aring;', 'å', $unsafe);
		     $unsafe = str_replace('&aelig;', 'æ', $unsafe);
		     $unsafe = str_replace('&ccedil;', 'ç', $unsafe);
		     $unsafe = str_replace('&egrave;', 'è', $unsafe);
		     $unsafe = str_replace('&eacute;', 'é', $unsafe);
		     $unsafe = str_replace('&ecirc;', 'ê', $unsafe);
		     $unsafe = str_replace('&euml;', 'ë', $unsafe);
		     $unsafe = str_replace('&igrave;', 'ì', $unsafe);
		     $unsafe = str_replace('&iacute;', 'í', $unsafe);
		     $unsafe = str_replace('&icirc;', 'î', $unsafe);
		     $unsafe = str_replace('&iuml;', 'ï', $unsafe);
		     $unsafe = str_replace('&eth;', 'ð', $unsafe);
		     $unsafe = str_replace('&ntilde;', 'ñ', $unsafe);
		     $unsafe = str_replace('&ograve;', 'ò', $unsafe);
		     $unsafe = str_replace('&oacute;', 'ó', $unsafe);
		     $unsafe = str_replace('&ocirc;', 'ô', $unsafe);
		     $unsafe = str_replace('&otilde;', 'õ', $unsafe);
		     $unsafe = str_replace('&ouml;', 'ö', $unsafe);
		     $unsafe = str_replace('&oslash;', 'ø', $unsafe);
		     $unsafe = str_replace('&ugrave;', 'ù', $unsafe);
		     $unsafe = str_replace('&uacute;', 'ú', $unsafe);
		     $unsafe = str_replace('&ucirc;', 'û', $unsafe);
		     $unsafe = str_replace('&uuml;', 'ü', $unsafe);
		     $unsafe = str_replace('&yacute;', 'ý', $unsafe);
		     $unsafe = str_replace('&thorn;', 'þ', $unsafe);
		     $unsafe = str_replace('&quot;', '"', $unsafe);
		     $unsafe = str_replace('&apos;', "'", $unsafe);
		     $unsafe = str_replace('&amp;', '&', $unsafe);
		     return $unsafe;
		}
		
    
}
?>