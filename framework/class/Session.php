<?php
/**
 * Session Management
 *
 * @date        march 2011
 * @author      Julien Guion
 * @copyright   Julien Guion
 * @version     1
 */
class Session {

    private static $_tData              = array('common'    => array());
    private static $_initialized        = false;
    private static $_started            = false;
    private static $_defaultSpacename   = 'common';

 
    /**
     * Check if a value has been setted in session
     * @param string $var
     * @param string $spaceName
     * @return boolean
     */   
    public static function exists($var, $spaceName=null) {
    	if(!self::isInitialized()) self::initialize();
        if(is_null($spaceName)) $spaceName  = self::getDefaultSpaceName();
        
        return isset(self::$_tData[$spaceName][$var]);
    }

    /**
     * Getter
     * @param string $var
     * @param string $spaceName
     * @return mixed
     */
    public static function get($var, $spaceName=null, $autoDestroy=false) {
        if(!self::isInitialized()) self::initialize();
        if(is_null($spaceName)) $spaceName  = self::getDefaultSpaceName();
        if(!isset(self::$_tData[$spaceName][$var])) return null;

        $value = self::$_tData[$spaceName][$var];
		
		if(isset(self::$_tData['autoDestroy'][$var]) || $autoDestroy == true)
		{
			unset($_SESSION[$spaceName][$var]);
			unset(self::$_tData[$spaceName][$var]);
			unset(self::$_tData['autoDestroy'][$var]);
		}
		
		return $value;
    }

    /**
     * Setter
     * @param string $var
     * @param mixed $value
     * @param string $spaceName
     */
    public static function set($var, $value, $spaceName=null, $autoDestroy=false) {
        if(!self::isInitialized()) self::initialize();
        if(is_null($spaceName))                 $spaceName  = self::getDefaultSpacename();
        if(!isset(self::$_tData[$spaceName]))   self::$_tData[$spaceName] = array();
        self::$_tData[$spaceName][$var]         = $value;
        if(!isset($_SESSION[$spaceName]))       $_SESSION[$spaceName]   = array();
        $_SESSION[$spaceName][$var]             = $value;

				if($autoDestroy) self::set($var, 1, 'autoDestroy');	
    }
    
    /**
    * Getter - View all session of User
    *
    *
    */
    public static function getAll(){
    	if(!self::isInitialized()) self::initialize();
    	return $_SESSION;
    }
    

    /**
     * Initialize application (load data)
     */
    public static function initialize() {
    	self::start();
      self::$_initialized = true;
      self::$_tData       = isset($_SESSION) ? $_SESSION : array();
    }

    /**
     * Check is application is initialized
     * @return boolean
     */
    public static function isInitialized() {
        return self::$_initialized;
    }

    /**
     * Get the default space name
     * @return string
     */
    public static function getDefaultSpaceName() {
        return self::$_defaultSpacename;
    }

    /**
     * Start the session
     */
    public static function start() {
        if(headers_sent()) throw new Exception_Runtime('An error occured during the Session initialization');
        if(!self::$_started) {
            session_start();
            self::$_started = true;
        }
    }

    /**
     * Setter for the name of the session
     * @param string
     */
    public static function setName($name) {
        $name   = (string) $name;
        session_name($name);
    }

    /**
     * Destroy the current session
     */
    public static function destroy($var) {
    	if(!self::isInitialized()) self::initialize();
      $spaceName  = self::getDefaultSpacename();
      if(!isset(self::$_tData[$spaceName]))   self::$_tData[$spaceName] = array();
			
			unset($_SESSION[$spaceName][$var]);
			unset(self::$_tData[$spaceName][$var]);
    }
    
     /**
     * Destroy the current session
     */
    public static function destroyAll() {
    	if(!self::isInitialized()) self::initialize();
			
			$spaceName  = self::getDefaultSpacename();
      if(!isset(self::$_tData[$spaceName]))   self::$_tData[$spaceName] = array();
			self::$_tData[$spaceName] = array();
			$_SESSION = array();
			
			// Finalement, on détruit la session.
			session_destroy();
    	self::close();
    }
    
    /**
     * Close the current session
     */
    public static function close() {
        session_write_close();
    	self::$_initialized = false;
    	self::$_started = false;
    }
}