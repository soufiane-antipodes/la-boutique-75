<?php
/**
 * @class Trigger
 * @brief Manage event listeners
 * 
 * Usage:
 *      myFunc  = function() { 'PHP is closing'; }
 *      Trigger::bind('shutdown', 'myFunc');
 *
 * Current events :
 *      shutdown (PHP), startScript (Framework), onStartHTML (Framework)
 *
 * @date        february 2012
 * @author      Julien Guion
 * @copyright   agence2web
 * @version     1
 */
abstract class Trigger {

    static private $_ressources    = array();
    static private $_called        = array();
    static private $_initialized    = false;
    
    /**
     * Add a event listener
     * @param string $ressource event listened
     * @param string $callback name of function
     * @return mixed
     */
    public static function bind($ressource, $callback) {

        // ::::: normal events :::::
        if(!isset(self::$_ressources[$ressource])) self::$_ressources[$ressource]   = array();
        self::$_ressources[$ressource][]    = $callback;

        return;
    }

    /**
     * On shutdown
     */
    public static function onShutdown() {
        self::call('shutdown');
    }
    

    /**
     * Delete a event listener
     * @param string $ressource event listened
     */
    public static function unbind($ressource) {
        foreach(self::$_ressources as $key => $callback) {
            if($callback == $ressource) unset(self::$_ressources[$key]);
        }
    }

    /**
     * Call event. This action launch all listerners associated to this event
     * @param string $ressource event
     */
    public static function call($ressource) {

        if(!self::isInitialized()) self::initialize();

        if(isset(self::$_called[$ressource])) {
            self::$_called[$ressource]++;
        } else {
            self::$_called[$ressource] = 0;
        }

        $args   = func_get_args();
        if(isset(self::$_ressources[$ressource])) {
            $ressource  = self::$_ressources[$ressource];
            // ::::: stack :::::
            foreach($ressource as $func) {
                call_user_func_array($func, $args);
            }
        }
    }

    /**
     * Check if trigger was called
     * @param string $ressource
     * @return boolean
     */
    public static function wasCalled($ressource) {
        return isset(self::$_called[$ressource]);
    }

    /**
     * Check if triggers are initialized
     * @return boolean
     */
    public static function isInitialized() {
        return (boolean) self::$_initialized;
    }

    /**
     * Initialize trigger
     */
    public static function initialize() {
        register_shutdown_function(array('Trigger', 'onShutdown'));
        self::$_initialized = true;
    }
}
?>
