<?php

/**
* Class User
*
* Permet de gerer les Utilisateurs qui sont admin
*
* @author Julien Guion
* @version 1.0
*/
Class User{
	
	/**
	* Test si le Client est Loggué
	*
	*	@return boolean
	*/
	public static function isAdminLogged(){
		$result = false;
		
		//On regarde si une session est présente
		if(Session :: exists('Admin')){
			$result = true;
		}else{
			//On regarde si un cookie est présent
			if(Cookie :: exists('Admin') && Cookie :: exists('InfoAdmin') && Cookie :: get('Admin') != '' && Cookie :: get('InfoAdmin') != ''){
				
				$ok = self :: adminLoggin(Cookie::get('Admin'), Cookie::get('InfoAdmin'), true);
				if($ok === true) $result = true;
				
			}
		}
		return $result;
	}
	
	/**
	* Log le Client
	*
	* @param string $email
	*	@param string $password
	*	@return boolean
	*/
	public static function adminLoggin($email, $password, $md5=false){
		$result = false;
		$user = array();
		$md5Condition = 'md5("'.mysql_real_escape_string($password).'")';
		if($md5 == true) $md5Condition = '"'.mysql_real_escape_string($password).'"';
		
		Database :: getLine('select id_user, email, password, firstname, lastname, is_admin from '.BDD.'user where email="'.mysql_real_escape_string($email).'" and password='.$md5Condition, $user);
		
		if(count($user) > 1){
			Session :: set('Admin', $user);
			Cookie :: set('Admin', $user['email']);
			Cookie :: set('InfoAdmin', $user['password']);
			$result = true;
		}
		
		return $result;
	}
	
	/**
	* Deconnecte un utilisateur
	*/
	public static function logout(){
		Cookie :: destroyAll();
		Session :: destroyAll();
	}
	
	//::::::::::::::::::::::::::::::::::::::::: GETTER ::::::::::::::::::::::::::::::::://
	
	/**
	* Recupere le nom complet de l'utilisateur connecté
	*
	*	@return string
	*/
	
	public static function adminGetfullName(){
		$user = Session::get('Admin');
		return $user['firstname'] . ' ' . $user['lastname'];
	}
	
	public static function adminGetIdUser(){
		$user = Session :: get('Admin');
		return $user['idUser'];
	}
	
	public static function isSuperAdmin(){
		$user = Session :: get('Admin');
		return $user['is_admin'] ? true : false;
	}
	
	public static function isAdmin(){
		$user = Session :: get('Admin');
		return ($user['is_admin'] == 1 || $user['is_admin'] == 0) ? true : false;
	}

}