<?php
/**
* Class Static Validation
*
* @copyright Julien Guion
* @version 1
*
*/
Class Validation{
	
	/**
	* Check Email
	*
	*	@param string $email
	*	@return boolean
	*/
	public static function email($email){
		return (filter_var($email, FILTER_VALIDATE_EMAIL)) ? true : false;
	}
	
	/**
	* Check Password
	*
	*	@param string $password
	*	@return boolean
	*/
	public static function password($password){
		return (preg_match('#^[a-zA-Z0-9]{6,12}$#', $password)) ? true : false;
	}
	
	/**
	* Check String
	*
	*	@param string $password
	*	@return boolean
	* @deprecated
	*	@todo Gerer les accents - Cree une function keepValidChars
	*/
	public static function string($string){
		return ($string != '') ? true : false;
	}
	
	/**
	* Check si c'est un entier
	*
	*	@param integer $int
	*	@return boolean
	*/
	public static function int($int){
		return (filter_var($int, FILTER_VALIDATE_INT)) ? true : false;
	}
	
	/**
	* Check si le string est un nombre
	*
	*	@param integer $int
	*	@return boolean
	*/
	public static function integer($string){
		return (preg_match('#^[0-9]+$#', $string)) ? true : false;
	}
	
	/**
	* Check Code Postal
	*
	*	@param integer $zipCode
	*	@return boolean
	*/
	public static function zipCode($zipCode){
		return (preg_match('#^[0-9]{5}$#i', $zipCode)) ? true : false;
	}
	
	/**
	* Check Numero de Telephone
	*
	*	@param integer $phone
	*	@return boolean
	*/
	public static function phone($phone){
		return (preg_match('#^[0-9]{10}$#i', $phone)) ? true : false;
	}
	
	public static function youtube($value){
		$pattern = '#^(?:https?://)?(?:www\.)?(?:youtu\.be/|youtube\.com(?:/embed/|/v/|/watch\?v=|/watch\?.+&v=))([\w-]{11})(?:.+)?$#x';
    preg_match($pattern, $value, $matches);
    return (isset($matches[1])) ? $matches[1] :  false;
  }
	
}