-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Client: 10.0.250.69
-- Généré le: Ven 07 Mars 2014 à 16:15
-- Version du serveur: 5.1.31
-- Version de PHP: 5.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `yanmercoeur_dev`
--

-- --------------------------------------------------------

--
-- Structure de la table `yannmercoeur_accueil_diaporama`
--

CREATE TABLE IF NOT EXISTS `yannmercoeur_accueil_diaporama` (
  `id_accueil_diaporama` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(125) NOT NULL,
  `url` varchar(255) NOT NULL,
  `lang` varchar(125) NOT NULL,
  `id_item` int(11) NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `position` smallint(6) NOT NULL,
  PRIMARY KEY (`id_accueil_diaporama`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `yannmercoeur_accueil_diaporama`
--

INSERT INTO `yannmercoeur_accueil_diaporama` (`id_accueil_diaporama`, `title`, `url`, `lang`, `id_item`, `publish`, `position`) VALUES
(1, 'Slide 1', '', 'fr', 11, 1, 1),
(2, 'Slide 2', 'http://www.podcast.com/', 'fr', 12, 1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `yannmercoeur_item`
--

CREATE TABLE IF NOT EXISTS `yannmercoeur_item` (
  `id_item` smallint(6) NOT NULL AUTO_INCREMENT,
  `value` text NOT NULL,
  `extension` varchar(255) NOT NULL,
  PRIMARY KEY (`id_item`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Contenu de la table `yannmercoeur_item`
--

INSERT INTO `yannmercoeur_item` (`id_item`, `value`, `extension`) VALUES
(3, 'slide1', 'jpg'),
(11, 'IMAGE 1 HEADER', 'jpg'),
(12, 'IMAGE 2 HEADER', 'jpg'),
(16, 'client1', 'jpg'),
(17, 'client1', 'jpg'),
(18, 'client2', 'jpg'),
(19, 'client3', 'jpg'),
(20, 'client4', 'jpg'),
(21, 'client5', 'jpg'),
(22, 'client6', 'jpg'),
(23, 'client7', 'jpg'),
(24, 'client8', 'jpg');

-- --------------------------------------------------------

--
-- Structure de la table `yannmercoeur_options`
--

CREATE TABLE IF NOT EXISTS `yannmercoeur_options` (
  `id_option` int(11) NOT NULL AUTO_INCREMENT,
  `idOptionType` smallint(6) NOT NULL,
  `meta_key` varchar(255) NOT NULL,
  `value` text NOT NULL,
  `description` varchar(255) NOT NULL,
  `lang` varchar(2) NOT NULL,
  `position` int(11) NOT NULL,
  PRIMARY KEY (`id_option`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=131 ;

--
-- Contenu de la table `yannmercoeur_options`
--

INSERT INTO `yannmercoeur_options` (`id_option`, `idOptionType`, `meta_key`, `value`, `description`, `lang`, `position`) VALUES
(1, 2, 'HOME_PODCAST', 'Ecoutez le podcast', 'Titre Podcast', 'fr', 1),
(2, 2, 'HOME_LINK_PODCAST', 'http://www.podcast.com/', 'Lien du podcast', 'fr', 2),
(3, 2, 'HOME_FORMATION_TITLE', 'Formation', 'Titre Formation', 'fr', 3),
(4, 2, 'HOME_FORMATION_DESCRIPTION', 'Yan Mercoeur intervient en tant que formateur en communication pour plusieurs organismes de formation professionnelle ou directement au sein des entreprises. Gr&amp;acirc;ce &amp;agrave; l&amp;rsquo;apport des techniques th&amp;eacute;&amp;acirc;trales, ses interventions permettent &amp;agrave; chacun un travail personnalis&amp;eacute; en associant notions th&amp;eacute;oriques et exercices ludiques.', 'Description Formation', 'fr', 4),
(5, 2, 'HOME_COACHING_TITLE', 'Coaching', 'Titre Coaching', 'fr', 5),
(6, 2, 'HOME_COACHING_DESCRIPTION', 'Yan Mercoeur intervient aupres des entreprises en tant que coach certifi&amp;eacute;. Il s&amp;rsquo;agit d&amp;rsquo;accompagner les personnes et/ou les &amp;eacute;quipes dans l&amp;rsquo;atteinte de leurs objectifs. Yan anime des sessions de coaching d&amp;rsquo;&amp;eacute;quipe ( team building). Il s&amp;rsquo;agit d&amp;rsquo;un travail collectif de coh&amp;eacute;sion. Objectif pour l&amp;rsquo;&amp;eacute;quipe : mieux travailler ensemble, devenir plus performante.', 'Description Coaching', 'fr', 6),
(7, 2, 'HOME_ACTEUR_TITLE', 'Acteur', 'Titre Acteur', 'fr', 7),
(8, 2, 'HOME_ACTEUR_DESCRIPTION', 'Yan Mercoeur est com&amp;eacute;dien professionnel. Il exerce son activit&amp;eacute; essentiellement sur sc&amp;egrave;ne. Il anime r&amp;eacute;guli&amp;egrave;rement des cours de th&amp;eacute;&amp;acirc;tre pour adultes amateurs sous trois formes : Cours &amp;agrave; l&amp;#39;ann&amp;eacute;e, stages th&amp;eacute;matiques de 2 jours, stages d&amp;eacute;couvertes pour adultes ( le dimanche )', 'Description Acteur', 'fr', 8),
(9, 2, 'HOME_CONTACT_TITLE', 'Contact', 'Titre Contact', 'fr', 9),
(10, 3, 'ABOUT_TITLE', 'Qui est Yan ?', 'Titre', 'fr', 1),
(11, 3, 'ABOUT_SUBTITLE', 'Mon approche', 'Sous titre', 'fr', 2),
(12, 3, 'ABOUT_DESCRIPTION', 'Communiquer est au coeur meme de notre vie.&lt;br /&gt;\r\nNous avons tous besoin d&amp;#39;&amp;eacute;changes, de relations, de communication avec les autres.&lt;br /&gt;\r\nDepuis l&amp;#39;&amp;eacute;cole, nous avons appris &amp;agrave; raisonner, &amp;agrave; r&amp;eacute;fl&amp;eacute;chir, mais jamais vraiment &amp;agrave; communiquer.&lt;br /&gt;\r\n&lt;br /&gt;\r\nComment donner envie &amp;agrave; nos interlocuteurs d&amp;#39;engager une relation avec nous ?&lt;br /&gt;\r\n&lt;br /&gt;\r\nComment leur donner envie de d&amp;eacute;velopper des relations harmonieuses et durables ?&lt;br /&gt;\r\n&lt;br /&gt;\r\nComment mieux comprendre les autres dans leur mani&amp;egrave;re d&amp;#39;&amp;ecirc;tre, d&amp;#39;agir, de communiquer ?&lt;br /&gt;\r\n&lt;br /&gt;\r\nYan Mercoeur propose une gamme compl&amp;egrave;te&lt;br /&gt;\r\nde formations autour de ces th&amp;eacute;matiques.&lt;br /&gt;\r\nSon approche originale et vivante s&amp;#39;appuie&lt;br /&gt;\r\nsur des mod&amp;egrave;les de communication&lt;br /&gt;\r\nreconnus, toujours enrichis&lt;br /&gt;\r\nde l&amp;#39;art du th&amp;eacute;&amp;acirc;tre.', 'Description 1', 'fr', 3),
(13, 3, 'ABOUT_DESCRIPTION_2', 'Mon parcours professionnel :', 'Description 2 Titre', 'fr', 4),
(14, 3, 'ABOUT_DESCRIPTION_2_CONTENT', 'Une double expertise&lt;br /&gt;\r\nCom&amp;eacute;dien professionnel en activit&amp;eacute; depuis 12 ans&lt;br /&gt;\r\nFormateur certifi&amp;eacute; en relations humaines depuis 8 ans&lt;br /&gt;\r\nCoach et Enseignant certifi&amp;eacute; en Programmation Neuro&lt;br /&gt;\r\nLinguistique&lt;br /&gt;\r\nFormateur certifi&amp;eacute; au mod&amp;egrave;le Process Com&lt;br /&gt;\r\nPraticien certifi&amp;eacute; en Hypnose Ericksonienne&lt;br /&gt;\r\nForm&amp;eacute; en Syst&amp;eacute;mique', 'Description 2 Contenu', 'fr', 5),
(15, 4, 'FORMATION_TITLE', 'Formations', 'Titre', 'fr', 1),
(16, 4, 'FORMATION_DESCRIPTION', 'YM intervient en tant que consultant-expert en communication et en relations humaines.&lt;br /&gt;\r\nIl collabore depuis des ann&amp;eacute;es avec des organismes de formation professionnelle ;&lt;br /&gt;\r\nil intervient aussi directement au sein des entreprises.&lt;br /&gt;\r\nSes formations sont toujours enrichies de techniques th&amp;eacute;&amp;acirc;trales,&lt;br /&gt;\r\npermettant un travail ludique et personnalis&amp;eacute; pour chaque participant.&lt;br /&gt;\r\nIl aime aussi cr&amp;eacute;er des modules de formations sp&amp;eacute;cifiques pour r&amp;eacute;pondre au mieux &amp;agrave; vos besoins.', 'Description', 'fr', 2),
(17, 4, 'FORMATION_DESCRIPTION_BLOC1', 'PRISE DE PAROLE EN PUBLIC&lt;br /&gt;\r\nAVEC LES TECHNIQUES&lt;br /&gt;\r\nD&amp;#39;ACTEUR&lt;br /&gt;\r\n&lt;br /&gt;\r\nMIEUX COMMUNIQUER EN&lt;br /&gt;\r\nPUBLIC AVEC LES TECHNIQUES&lt;br /&gt;\r\nD&amp;#39;IMPROVISATION&lt;br /&gt;\r\n&lt;br /&gt;\r\nPRISE DE PAROLE ET&lt;br /&gt;\r\nCHARISME AU F&amp;Eacute;MININ', 'Description bloc 1', 'fr', 3),
(18, 4, 'FORMATION_DESCRIPTION_BLOC2', 'ANIMER DES R&amp;Eacute;UNIONS&lt;br /&gt;\r\nVIVANTES ET EFFICACES', 'Description bloc 2', 'fr', 4),
(19, 4, 'FORMATION_DESCRIPTION_BLOC3', 'SE PR&amp;Eacute;PARER AUX&lt;br /&gt;\r\nENTRETIENS&lt;br /&gt;\r\nPROFESSIONNELS', 'Description bloc 3', 'fr', 5),
(20, 4, 'FORMATION_DESCRIPTION_BLOC4', 'SORTIR DES&lt;br /&gt;\r\nCONFLITS AU BUREAU', 'Description bloc 4', 'fr', 6),
(21, 5, 'COACHING_TITLE', 'Coaching', 'Titre', 'fr', 1),
(22, 5, 'COACHING_DESCRIPTION', 'YM intervient pour les entreprises et les particuliers en tant que coach certifi&amp;eacute;.&lt;br /&gt;\r\nIl s&amp;#39;agit d&amp;#39; accompagner une personne ou une &amp;eacute;quipe dans l&amp;#39;atteinte de ses objectifs.', 'Description', 'fr', 2),
(23, 5, 'COACHING_BLOC_1_TEXT_1', 'YAN ANIME DES SESSIONS&lt;br /&gt;\r\nDE COACHING D&amp;rsquo;&amp;Eacute;QUIPE&lt;br /&gt;\r\n(TEAM BUILDING)', 'Colonne 1 - Texte 1', 'fr', 3),
(24, 5, 'COACHING_BLOC_1_TEXT_2', 'Il s&amp;rsquo;agit d&amp;rsquo;un travail collectif de coh&amp;eacute;sion', 'Colonne 1 - Texte 2', 'fr', 4),
(25, 5, 'COACHING_BLOC_1_TEXT_3', 'OBJECTIF POUR L&amp;rsquo;&amp;Eacute;QUIPE', 'Colonne 1 - Texte 3', 'fr', 5),
(26, 5, 'COACHING_BLOC_1_TEXT_4', 'Mieux travailler ensemble&lt;br /&gt;\r\nDevenir plus performant', 'Colonne 1 - Texte 4', 'fr', 6),
(27, 5, 'COACHING_BLOC_2_TEXT_1', 'IL ACCOMPAGNE AUSSI DES&lt;br /&gt;\r\nDIRIGEANTS, MANAGERS, ET&lt;br /&gt;\r\nCOLLABORATEURS', 'Colonne 2 - Texte 1', 'fr', 7),
(28, 5, 'COACHING_BLOC_2_TEXT_2', 'Il s&amp;rsquo;agit d&amp;rsquo;un coaching individuel', 'Colonne 2 - Texte 2', 'fr', 8),
(29, 5, 'COACHING_BLOC_2_TEXT_3', 'OBJECTIF', 'Colonne 2 - Texte 3', 'fr', 9),
(30, 5, 'COACHING_BLOC_2_TEXT_4', 'Am&amp;eacute;lioration des relations professionnelles&lt;br /&gt;\r\nPrise de poste&lt;br /&gt;\r\nCommunication dicile avec&lt;br /&gt;\r\nl&amp;rsquo;entourage professionnel&lt;br /&gt;\r\nPrise de parole en r&amp;eacute;union&lt;br /&gt;\r\nPr&amp;eacute;paration aux entretiens&lt;br /&gt;\r\nR&amp;eacute;alisation de projet professionnel&lt;br /&gt;\r\nAccompagner un manager d&amp;rsquo;&amp;eacute;quipe&lt;br /&gt;\r\nGestion des &amp;eacute;motions au bureau', 'Colonne 2 - Texte 4', 'fr', 10),
(31, 6, 'THEATRES_TITLE', 'Cours de Theatres', 'Titre', 'fr', 1),
(32, 6, 'THEATRES_DESCRIPTION', 'Yan Mercoeur est professeur d&amp;rsquo;art dramatique.&lt;br /&gt;\r\nIl anime des cours de th&amp;eacute;&amp;acirc;tre pour adultes amateurs.', 'Description', 'fr', 2),
(33, 6, 'THEATRES_SUBTITLE', 'IL PROPOSE R&amp;Eacute;GULI&amp;Egrave;REMENT :', 'Soustitre', 'fr', 3),
(34, 7, 'TEMOIGNAGES_TITLE', 'T&amp;eacute;moignages', 'Titre', 'fr', 1),
(35, 7, 'TEMOIGNAGES_DESCRIPTION', 'QUELQUES T&amp;Eacute;MOIGNAGES DES INTERVENTIONS DE YAN&lt;br /&gt;\r\nAUPRES DE PARTICULIERS ET DE PROFESSIONELS', 'Description', 'fr', 2),
(36, 8, 'CONTACT_TITLE', 'Contact', 'Titre', 'fr', 1),
(37, 8, 'CONTACT_SUBTITLE', '&amp;laquo; Si vous souhaitez poser une question ou faire&lt;br /&gt;\r\nun commentaire, n&amp;rsquo;h&amp;eacute;sitez pas &amp;agrave; me contacter &amp;raquo;', 'Soustitre', 'fr', 2),
(38, 8, 'CONTACT_LABEL_SOCIAL', 'Suivez moi', 'Label Social', 'fr', 3),
(39, 8, 'CONTACT_FACEBOOK', 'https://fr-fr.facebook.com/yan.mercoeur', 'Lien facebook', 'fr', 4),
(40, 8, 'CONTACT_TWITTER', 'http://twitter.com/', 'Lien Twitter', 'fr', 5),
(41, 8, 'CONTACT_LABEL_CONTACT', 'Contactez moi', 'Label Contact', 'fr', 6),
(42, 8, 'CONTACT_NAME', 'Nom', 'Nom', 'fr', 7),
(43, 8, 'CONTACT_TEL', 'T&amp;eacute;l&amp;eacute;phone', 'Téléphone', 'fr', 8),
(44, 8, 'COTNACT_MAIL', 'E-mail', 'E-mail', 'fr', 9),
(45, 8, 'COTNACT_LABEL_TITLE', 'Titre', 'Label Titre', 'fr', 10),
(46, 8, 'CONTACT_MESSAGE', 'Message', 'Message', 'fr', 11),
(47, 8, 'CONTACT_BUTTON', 'Envoyer', 'Bouton Envoyer', 'fr', 12),
(48, 1, 'META_TITLE', 'Yan Mercoeur | Formation, coaching, cours de th&amp;eacute;&amp;acirc;tre', 'Meta Title', 'fr', 1),
(49, 1, 'META_DESCRIPTION', 'Communiquer autrement | Mieux travailler et vivre ensemble', 'Meta Description', 'fr', 2),
(50, 1, 'META_KEYWORDS', 'yann, mercoeur, coaching', 'Meta Keywords', 'fr', 3),
(51, 1, 'FOOTER_NAME', 'Mercoeur Formation', 'Pied de page - Nom', 'fr', 4),
(52, 1, 'FOOTER_MAIL', 'mercoeur.yan@gmail.com', 'Pied de page - Email', 'fr', 5),
(53, 1, 'FOOTER_FACEBOOK', 'https://fr-fr.facebook.com/yan.mercoeur', 'Pied de page - Lien Facebook', 'fr', 6),
(54, 1, 'FOOTER_TWITTER', 'http://twitter.com/', 'Pied de page - Lien Twitter', 'fr', 7),
(55, 1, 'MENU_HOME', 'Accueil', 'Menu - Accueil', 'fr', 8),
(56, 1, 'MENU_ABOUT', 'Qui est Yan ?', 'Menu - Qui est Yan ?', 'fr', 9),
(57, 1, 'MENU_FORMATIONS', 'Formations', 'Menu - Formations', 'fr', 10),
(58, 1, 'MENU_COACHING', 'Coaching', 'Menu - Coaching', 'fr', 11),
(59, 1, 'MENU_THEATRES', 'Cours de Th&amp;eacute;atres', 'Menu - Cours de Théatres', 'fr', 12),
(60, 1, 'MENU_TEMOIGNAGES', 'T&amp;eacute;moignages', 'Menu - Témoignages', 'fr', 13),
(61, 1, 'MENU_PARTNERS', 'Partenaires', 'Menu - Partenaires', 'fr', 14),
(62, 1, 'MENU_CONTACT', 'Contact', 'Menu - Contact', 'fr', 15),
(63, 9, 'PARTNERS_TITLE', 'Partenaires', 'Titre', 'fr', 1),
(64, 2, 'HOME_PODCAST', 'Ecoutez les podcasts de Yan', 'Titre Podcast', 'en', 1),
(65, 2, 'HOME_LINK_PODCAST', 'http://www.podcast.com/', 'Lien du podcast', 'en', 2),
(66, 2, 'HOME_FORMATION_TITLE', 'Formation', 'Titre Formation', 'en', 3),
(67, 2, 'HOME_FORMATION_DESCRIPTION', 'Yan Mercoeur intervient en tant que formateur en communication pour plusieurs organismes de formation professionnelle ou directement au sein des entreprises. Gr&amp;acirc;ce &amp;agrave; l&amp;rsquo;apport des techniques th&amp;eacute;&amp;acirc;trales, ses interventions permettent &amp;agrave; chacun un travail personnalis&amp;eacute; en associant notions th&amp;eacute;oriques et exercices ludiques.', 'Description Formation', 'en', 4),
(68, 2, 'HOME_COACHING_TITLE', 'Coaching', 'Titre Coaching', 'en', 5),
(69, 2, 'HOME_COACHING_DESCRIPTION', 'Yan Mercoeur intervient aupres des entreprises en tant que coach certifi&amp;eacute;. Il S&amp;rsquo;agit d&amp;rsquo;accompagner les personnes et/ou les &amp;eacute;quipes dans l&amp;rsquo;atteinte de leurs objectifs. Yan anime des sessions de coaching d&amp;rsquo;&amp;eacute;quipe ( team building). Il s&amp;rsquo;agit d&amp;rsquo;un travail collectif de coh&amp;eacute;sion. Objectif pour l&amp;rsquo;&amp;eacute;quipe : mieux travailler ensemble, devenir plus performante.', 'Description Coaching', 'en', 6),
(70, 2, 'HOME_ACTEUR_TITLE', 'Acteur', 'Titre Acteur', 'en', 7),
(71, 2, 'HOME_ACTEUR_DESCRIPTION', 'Yan Mercoeur est com&amp;eacute;dien professionnel. Il exerce son activit&amp;eacute; essentiellement sur sc&amp;egrave;ne. Il anime r&amp;eacute;guli&amp;egrave;rement des cours de th&amp;eacute;&amp;acirc;tre pour adultes amateurs sous trois formes : Cours &amp;agrave; l&amp;#39;ann&amp;eacute;e, stages th&amp;eacute;matiques de 2 jours, stages d&amp;eacute;couvertes pour adultes ( le dimanche )', 'Description Acteur', 'en', 8),
(72, 2, 'HOME_CONTACT_TITLE', 'Contact', 'Titre Contact', 'en', 9),
(73, 3, 'ABOUT_TITLE', 'Qui est Yan ?', 'Titre', 'en', 1),
(74, 3, 'ABOUT_SUBTITLE', 'Mon approche', 'Sous titre', 'en', 2),
(75, 3, 'ABOUT_DESCRIPTION', 'Communiquer est au coeur meme de notre vie.&lt;br /&gt;\r\nNous avons tous besoin d&amp;#39;&amp;eacute;changes, de relations, de communication avec les autres.&lt;br /&gt;\r\nDepuis l&amp;#39;&amp;eacute;cole, nous avons appris &amp;agrave; raisonner, &amp;agrave; r&amp;eacute;fl&amp;eacute;chir, mais jamais vraiment &amp;agrave; communiquer.&lt;br /&gt;\r\n&lt;br /&gt;\r\nComment donner envie &amp;agrave; nos interlocuteurs d&amp;#39;engager une relation avec nous ?&lt;br /&gt;\r\n&lt;br /&gt;\r\nComment leur donner envie de d&amp;eacute;velopper des relations harmonieuses et durables ?&lt;br /&gt;\r\n&lt;br /&gt;\r\nComment mieux comprendre les autres dans leur mani&amp;egrave;re d&amp;#39;&amp;ecirc;tre, d&amp;#39;agir, de communiquer ?&lt;br /&gt;\r\n&lt;br /&gt;\r\nYan Mercoeur propose une gamme compl&amp;egrave;te&lt;br /&gt;\r\nde formations autour de ces th&amp;eacute;matiques.&lt;br /&gt;\r\nSon approche originale et vivante s&amp;#39;appuie&lt;br /&gt;\r\nsur des mod&amp;egrave;les de communication&lt;br /&gt;\r\nreconnus, toujours enrichis&lt;br /&gt;\r\nde l&amp;#39;art du th&amp;eacute;&amp;acirc;tre.', 'Description 1', 'en', 3),
(76, 3, 'ABOUT_DESCRIPTION_2', 'Mon parcours professionnel :', 'Description 2 Titre', 'en', 4),
(77, 3, 'ABOUT_DESCRIPTION_2_CONTENT', 'Une double expertise&lt;br /&gt;\r\nCom&amp;eacute;dien professionnel en activit&amp;eacute; depuis 12 ans&lt;br /&gt;\r\nFormateur certifi&amp;eacute; en relations humaines depuis 8 ans&lt;br /&gt;\r\nCoach et Enseignant certifi&amp;eacute; en Programmation Neuro&lt;br /&gt;\r\nLinguistique&lt;br /&gt;\r\nFormateur certifi&amp;eacute; au mod&amp;egrave;le Process Com&lt;br /&gt;\r\nPraticien certifi&amp;eacute; en Hypnose Ericksonienne&lt;br /&gt;\r\nForm&amp;eacute; en Syst&amp;eacute;mique', 'Description 2 Contenu', 'en', 5),
(78, 4, 'FORMATION_TITLE', 'Formations', 'Titre', 'en', 1),
(79, 4, 'FORMATION_DESCRIPTION', 'YM intervient en tant que consultant-expert en communication et en relations humaines.&lt;br /&gt;\r\nIl collabore depuis des ann&amp;eacute;es avec des organismes de formation professionnelle ;&lt;br /&gt;\r\nil intervient aussi directement au sein des entreprises.&lt;br /&gt;\r\nSes formations sont toujours enrichies de techniques th&amp;eacute;&amp;acirc;trales,&lt;br /&gt;\r\npermettant un travail ludique et personnalis&amp;eacute; pour chaque participant.&lt;br /&gt;\r\nIl aime aussi cr&amp;eacute;er des modules de formations sp&amp;eacute;cifiques pour r&amp;eacute;pondre au mieux &amp;agrave; vos besoins.', 'Description', 'en', 2),
(80, 4, 'FORMATION_DESCRIPTION_BLOC1', 'PRISE DE PAROLE EN PUBLIC&lt;br /&gt;\r\nAVEC LES TECHNIQUES&lt;br /&gt;\r\nD&amp;#39;ACTEUR&lt;br /&gt;\r\n&lt;br /&gt;\r\nMIEUX COMMUNIQUER EN&lt;br /&gt;\r\nPUBLIC AVEC LES TECHNIQUES&lt;br /&gt;\r\nD&amp;#39;IMPROVISATION&lt;br /&gt;\r\n&lt;br /&gt;\r\nPRISE DE PAROLE ET&lt;br /&gt;\r\nCHARISME AU F&amp;Eacute;MININ', 'Description bloc 1', 'en', 3),
(81, 4, 'FORMATION_DESCRIPTION_BLOC2', 'ANIMER DES R&amp;Eacute;UNIONS&lt;br /&gt;\r\nVIVANTES ET EFFICACES', 'Description bloc 2', 'en', 4),
(82, 4, 'FORMATION_DESCRIPTION_BLOC3', 'SE PR&amp;Eacute;PARER AUX&lt;br /&gt;\r\nENTRETIENS&lt;br /&gt;\r\nPROFESSIONNELS', 'Description bloc 3', 'en', 5),
(83, 4, 'FORMATION_DESCRIPTION_BLOC4', 'SORTIR DES&lt;br /&gt;\r\nCONFLITS AU BUREAU', 'Description bloc 4', 'en', 6),
(84, 5, 'COACHING_TITLE', 'Coaching', 'Titre', 'en', 1),
(85, 5, 'COACHING_DESCRIPTION', 'YM intervient pour les entreprises et les particuliers en tant que coach certifi&amp;eacute;.&lt;br /&gt;\r\nIl s&amp;#39;agit d&amp;#39; accompagner une personne ou une &amp;eacute;quipe dans l&amp;#39;atteinte de ses objectifs.', 'Description', 'en', 2),
(86, 5, 'COACHING_BLOC_1_TEXT_1', 'YAN ANIME DES SESSIONS&lt;br /&gt;\r\nDE COACHING D&amp;rsquo;&amp;Eacute;QUIPE&lt;br /&gt;\r\n(TEAM BUILDING)', 'Colonne 1 - Texte 1', 'en', 3),
(87, 5, 'COACHING_BLOC_1_TEXT_2', 'Il s&amp;rsquo;agit d&amp;rsquo;un travail collectif de coh&amp;eacute;sion', 'Colonne 1 - Texte 2', 'en', 4),
(88, 5, 'COACHING_BLOC_1_TEXT_3', 'OBJECTIF POUR L&amp;rsquo;&amp;Eacute;QUIPE', 'Colonne 1 - Texte 3', 'en', 5),
(89, 5, 'COACHING_BLOC_1_TEXT_4', 'Mieux travailler ensemble&lt;br /&gt;\r\nDevenir plus performant', 'Colonne 1 - Texte 4', 'en', 6),
(90, 5, 'COACHING_BLOC_2_TEXT_1', 'IL ACCOMPAGNE AUSSI DES&lt;br /&gt;\r\nDIRIGEANTS, MANAGERS, ET&lt;br /&gt;\r\nCOLLABORATEURS', 'Colonne 2 - Texte 1', 'en', 7),
(91, 5, 'COACHING_BLOC_2_TEXT_2', 'Il s&amp;rsquo;agit d&amp;rsquo;un coaching individuel', 'Colonne 2 - Texte 2', 'en', 8),
(92, 5, 'COACHING_BLOC_2_TEXT_3', 'OBJECTIF', 'Colonne 2 - Texte 3', 'en', 9),
(93, 5, 'COACHING_BLOC_2_TEXT_4', 'Am&amp;eacute;lioration des relations professionnelles&lt;br /&gt;\r\nPrise de poste&lt;br /&gt;\r\nCommunication dicile avec&lt;br /&gt;\r\nl&amp;rsquo;entourage professionnel&lt;br /&gt;\r\nPrise de parole en r&amp;eacute;union&lt;br /&gt;\r\nPr&amp;eacute;paration aux entretiens&lt;br /&gt;\r\nR&amp;eacute;alisation de projet professionnel&lt;br /&gt;\r\nAccompagner un manager d&amp;rsquo;&amp;eacute;quipe&lt;br /&gt;\r\nGestion des &amp;eacute;motions au bureau', 'Colonne 2 - Texte 4', 'en', 10),
(94, 6, 'THEATRES_TITLE', 'Cours de Theatres', 'Titre', 'en', 1),
(95, 6, 'THEATRES_DESCRIPTION', 'Yan Mercoeur est professeur d&amp;rsquo;art dramatique.&lt;br /&gt;\r\nIl anime des cours de th&amp;eacute;&amp;acirc;tre pour adultes amateurs.', 'Description', 'en', 2),
(96, 6, 'THEATRES_SUBTITLE', 'IL PROPOSE R&amp;Eacute;GULI&amp;Egrave;REMENT :', 'Soustitre', 'en', 3),
(97, 7, 'TEMOIGNAGES_TITLE', 'T&amp;eacute;moignages', 'Titre', 'en', 1),
(98, 7, 'TEMOIGNAGES_DESCRIPTION', 'QUELQUES T&amp;Eacute;MOIGNAGES DES INTERVENTIONS DE YAN&lt;br /&gt;\r\nAUPRES DE PARTICULIERS ET DE PROFESSIONELS', 'Description', 'en', 2),
(99, 8, 'CONTACT_TITLE', 'Contact', 'Titre', 'en', 1),
(100, 8, 'CONTACT_SUBTITLE', '&amp;laquo; Si vous souhaitez poser une question ou faire&lt;br /&gt;\r\nun commentaire, n&amp;rsquo;h&amp;eacute;sitez pas &amp;agrave; me contacter &amp;raquo;', 'Soustitre', 'en', 2),
(101, 8, 'CONTACT_LABEL_SOCIAL', 'Suivez moi', 'Label Social', 'en', 3),
(102, 8, 'CONTACT_FACEBOOK', 'http://facebook.com/', 'Lien facebook', 'en', 4),
(103, 8, 'CONTACT_TWITTER', 'http://twitter.com/', 'Lien Twitter', 'en', 5),
(104, 8, 'CONTACT_LABEL_CONTACT', 'Contact', 'Label Contact', 'en', 6),
(105, 8, 'CONTACT_NAME', 'Nom', 'Nom', 'en', 7),
(106, 8, 'CONTACT_TEL', 'T&amp;eacute;l&amp;eacute;phone', 'Téléphone', 'en', 8),
(107, 8, 'COTNACT_MAIL', 'E-mail', 'E-mail', 'en', 9),
(108, 8, 'COTNACT_LABEL_TITLE', 'Titre', 'Label Titre', 'en', 10),
(109, 8, 'CONTACT_MESSAGE', 'Message', 'Message', 'en', 11),
(110, 8, 'CONTACT_BUTTON', 'Envoyer', 'Bouton Envoyer', 'en', 12),
(111, 1, 'META_TITLE', 'Yann Mercoeur', 'Meta Title', 'en', 1),
(112, 1, 'META_DESCRIPTION', 'Communiquer autrement | Mieux travailler et vivre ensemble', 'Meta Description', 'en', 2),
(113, 1, 'META_KEYWORDS', 'yann, mercoeur, coaching', 'Meta Keywords', 'en', 3),
(114, 1, 'FOOTER_NAME', 'Mercoeur Formation', 'Pied de page - Nom', 'en', 4),
(115, 1, 'FOOTER_MAIL', 'mercoeur.yan@gmail.com', 'Pied de page - Email', 'en', 5),
(116, 1, 'FOOTER_FACEBOOK', 'http://facebook.com/', 'Pied de page - Lien Facebook', 'en', 6),
(117, 1, 'FOOTER_TWITTER', 'http://twitter.com/', 'Pied de page - Lien Twitter', 'en', 7),
(118, 1, 'MENU_HOME', 'Home', 'Menu - Accueil', 'en', 8),
(119, 1, 'MENU_ABOUT', 'Qui est Yan ?', 'Menu - Qui est Yan ?', 'en', 9),
(120, 1, 'MENU_FORMATIONS', 'Formations', 'Menu - Formations', 'en', 10),
(121, 1, 'MENU_COACHING', 'Coaching', 'Menu - Coaching', 'en', 11),
(122, 1, 'MENU_THEATRES', 'Cours de Th&amp;eacute;atres', 'Menu - Cours de Théatres', 'en', 12),
(123, 1, 'MENU_TEMOIGNAGES', 'T&amp;eacute;moignages', 'Menu - Témoignages', 'en', 13),
(124, 1, 'MENU_PARTNERS', 'Partenaires', 'Menu - Partenaires', 'en', 14),
(125, 1, 'MENU_CONTACT', 'Contact', 'Menu - Contact', 'en', 15),
(126, 9, 'PARTNERS_TITLE', 'Partenaires', 'Titre', 'en', 1),
(127, 7, 'TEMOIGNAGES_LABEL_DEMAND', 'Demande', 'Label Demande', 'fr', 3),
(128, 7, 'TEMOIGNAGES_LABEL_METHOD', 'Methode', 'Label Méthode', 'fr', 4),
(129, 7, 'TEMOIGNAGES_LABEL_DEMAND', 'Demande', 'Label Demande', 'en', 5),
(130, 7, 'TEMOIGNAGES_LABEL_METHOD', 'Methode', 'Label Méthode', 'en', 6);

-- --------------------------------------------------------

--
-- Structure de la table `yannmercoeur_options_type`
--

CREATE TABLE IF NOT EXISTS `yannmercoeur_options_type` (
  `idOptionType` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`idOptionType`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `yannmercoeur_options_type`
--

INSERT INTO `yannmercoeur_options_type` (`idOptionType`, `name`) VALUES
(1, 'Textes généraux'),
(2, 'Accueil'),
(3, 'Qui est Yann ?'),
(4, 'Formations'),
(5, 'Coaching'),
(6, 'Cours de théâtres'),
(7, 'Témoignages'),
(8, 'Contact'),
(9, 'Partenaires');

-- --------------------------------------------------------

--
-- Structure de la table `yannmercoeur_partners`
--

CREATE TABLE IF NOT EXISTS `yannmercoeur_partners` (
  `id_partners` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `link` varchar(255) NOT NULL,
  `lang` varchar(125) NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `position` smallint(6) NOT NULL,
  PRIMARY KEY (`id_partners`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `yannmercoeur_partners`
--

INSERT INTO `yannmercoeur_partners` (`id_partners`, `title`, `description`, `link`, `lang`, `publish`, `position`) VALUES
(1, 'FORMATION&lt;br /&gt;\r\nEN PNL', 'Vous cherchez une formation&lt;br /&gt;\r\nen PNL', 'www.ressourcesetstrategies.com', 'fr', 1, 1),
(2, 'PRISE DE PAROLE ET&lt;br /&gt;\r\nCHARISME', 'Vous cherchez une formation&lt;br /&gt;\r\nPrise de Parole et Charisme&lt;br /&gt;\r\nentre femmes', 'www.femmes-et-managers.com', 'fr', 1, 2),
(3, 'CABINET DE&lt;br /&gt;\r\nRECRUTEMENT', 'Vous cherchez un cabinet&lt;br /&gt;\r\nde recrutement, du coaching', 'www.rcrconseil.com', 'fr', 1, 3),
(4, 'PRISE DE PAROLE ET&lt;br /&gt;\r\nCHARISME', 'Vous cherchez une formation&lt;br /&gt;\r\nen communication, du conseil&lt;br /&gt;\r\nen images', 'www.laboiteauximages.com', 'fr', 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `yannmercoeur_podcast`
--

CREATE TABLE IF NOT EXISTS `yannmercoeur_podcast` (
  `id_podcast` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `lang` varchar(125) NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `position` smallint(6) NOT NULL,
  `creationDate` datetime NOT NULL,
  PRIMARY KEY (`id_podcast`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

--
-- Contenu de la table `yannmercoeur_podcast`
--

INSERT INTO `yannmercoeur_podcast` (`id_podcast`, `title`, `link`, `lang`, `publish`, `position`, `creationDate`) VALUES
(6, '11.03.2013 - Le travail en Open Space', 'http://www.francebleu.fr/societe/open-space-travail/les-experts/l-open-space-au-travail-un-vrai-fleau', 'fr', 0, 26, '2014-01-10 18:31:04'),
(7, '18.03.2013 - Les rapports intergenerations au boulot', 'http://www.francebleu.fr/info-service-horoscope-pronostic-bon-plan/les-experts/expert-52', 'fr', 0, 25, '2014-01-10 18:34:23'),
(8, '25.03.2013 - Se préparer à un entretien d’embauche', 'http://www.francebleu.fr/info-service-horoscope-pronostic-bon-plan/les-experts/expert-58', 'fr', 0, 24, '2014-01-10 18:35:30'),
(9, '01.04.2013 - Le télétravail', 'http://www.francebleu.fr/info-service-horoscope-pronostic-bon-plan/les-experts/expert-64', 'fr', 0, 23, '2014-01-10 18:36:29'),
(10, '08.04.2013 - Je ne supporte plus mon boss, que faire !?', 'http://www.francebleu.fr/emissions/les-experts-1', 'fr', 0, 22, '2014-01-10 18:37:05'),
(11, '15.04.2013 - Retrouver sa motivation au bureau', 'http://www.francebleu.fr/info-service-horoscope-pronostic-bon-plan/les-experts/expert-71', 'fr', 0, 21, '2014-01-10 18:37:57'),
(12, '22.04.2013 - Gérer ses contrariétés au bureau', 'http://www.francebleu.fr/info-service-horoscope-pronostic-bon-plan/les-experts/expert-76', 'fr', 0, 20, '2014-01-10 18:39:21'),
(13, '06.05.2013 - Relations tendues avec mes collègues', 'http://www.francebleu.fr/info-service-horoscope-pronostic-bon-plan/les-experts/expert-84', 'fr', 0, 19, '2014-01-10 18:40:12'),
(14, '13.05.2013 - Perfectionniste au bureau', 'http://www.francebleu.fr/info-service-horoscope-pronostic-bon-plan/les-experts/expert-90', 'fr', 0, 18, '2014-01-10 18:40:56'),
(15, '20.05.2013 - Mieux gérer son temps au bureau', 'http://www.francebleu.fr/info-service-horoscope-pronostic-bon-plan/les-experts/expert-97', 'fr', 0, 17, '2014-01-10 18:41:35'),
(16, '27.05.2013 - Prendre la parole en public', 'http://www.francebleu.fr/info-service-horoscope-pronostic-bon-plan/les-experts/expert-102', 'fr', 0, 16, '2014-01-10 18:42:05'),
(17, '03.06.2013 - La tenue vestimentaire', 'http://www.francebleu.fr/info-service-horoscope-pronostic-bon-plan/les-experts/expert-108', 'fr', 0, 15, '2014-01-10 18:42:33'),
(18, '10.06.2013 - Repérer les signes du Burn Out', 'http://www.francebleu.fr/info-service-horoscope-pronostic-bon-plan/les-experts/expert-112', 'fr', 0, 14, '2014-01-10 18:43:10'),
(19, '17.06.2013 - Limiter le blues du dimanche soir', 'http://www.francebleu.fr/info-service-horoscope-pronostic-bon-plan/les-experts/expert-118', 'fr', 0, 13, '2014-01-10 18:43:45'),
(20, '02.11.2013 - Nouveau job : comment bien prendre ses marques ?', 'http://www.francebleu.fr/societe/les-experts/expert-2', 'fr', 0, 12, '2014-01-10 18:44:27'),
(21, '09.11.2013 - Oser faire une demande à sa hiérarchie', 'http://www.francebleu.fr/%5Bfield_diffusion_themes-term-raw%5D/les-experts/expert', 'fr', 0, 11, '2014-01-10 18:45:00'),
(22, '16.09.2013 - Politesse et courtoisie au bureau', 'http://www.francebleu.fr/societe/les-experts/expert-8', 'fr', 0, 10, '2014-01-10 18:45:34'),
(23, '23.09.2013 - Comment formuler et recevoir les critiques', 'http://www.francebleu.fr/societe/les-experts/expert-13', 'fr', 0, 9, '2014-01-10 18:46:06'),
(24, '30.09.2013 - Je ne me sens pas à la hauteur dans mon job', 'http://www.francebleu.fr/societe/les-experts/expert-18', 'fr', 0, 8, '2014-01-10 18:46:35'),
(25, '21.10.2013 - Préparer et réussir son entretien d’embauche', 'http://www.francebleu.fr/societe/les-experts/expert-33', 'fr', 0, 7, '2014-01-10 18:47:05'),
(26, '28.10.2013 - Le retard au bureau', 'http://www.francebleu.fr/societe/les-experts/expert-38', 'fr', 0, 6, '2014-01-10 18:47:38'),
(27, '04.11.2013 - Travailler en famille', 'http://www.francebleu.fr/societe/les-experts/expert-43', 'fr', 0, 5, '2014-01-10 18:48:30'),
(28, '9.12.2013 - La procrastination : comment ne pas repousser au lendemain nos tâches ?', 'http://www.francebleu.fr/societe/les-experts/expert-68', 'fr', 1, 4, '2014-01-16 19:27:00'),
(29, '16.12.2013 - Prendre un coach professionnel : avantages ? interet ?', 'http://www.francebleu.fr/societe/les-experts/expert-73', 'fr', 1, 3, '2014-01-16 19:27:50'),
(30, '6.01.2014 - Les bonnes résolutions : comment les tenir cette année ?', 'http://www.francebleu.fr/societe/les-experts/expert-88', 'fr', 1, 2, '2014-01-16 19:28:31'),
(31, '13.01.2014 - Les pots en entreprise : allier convivialité et esprit professionnel', 'http://www.francebleu.fr/societe/les-experts/expert-93', 'fr', 1, 1, '2014-01-16 19:29:16');

-- --------------------------------------------------------

--
-- Structure de la table `yannmercoeur_temoignages_diaporama`
--

CREATE TABLE IF NOT EXISTS `yannmercoeur_temoignages_diaporama` (
  `id_temoignages_diaporama` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `demand` text NOT NULL,
  `method` text NOT NULL,
  `lang` varchar(125) NOT NULL,
  `id_item` int(11) NOT NULL,
  `publish` tinyint(1) NOT NULL DEFAULT '0',
  `position` smallint(6) NOT NULL,
  PRIMARY KEY (`id_temoignages_diaporama`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Contenu de la table `yannmercoeur_temoignages_diaporama`
--

INSERT INTO `yannmercoeur_temoignages_diaporama` (`id_temoignages_diaporama`, `title`, `description`, `demand`, `method`, `lang`, `id_item`, `publish`, `position`) VALUES
(1, 'KARINE&lt;br /&gt;\r\nCOMMERCIALE , PARIS', '&amp;laquo; Le juste &amp;eacute;quilibre entre l&amp;rsquo;explication de la th&amp;eacute;orie et la mise en pratique, l&amp;rsquo;approche, la souplesse p&amp;eacute;dagogique de Yan, m&amp;rsquo;ont&amp;nbsp;permis d&amp;rsquo;utiliser, d&amp;egrave;s le lendemain, ce que j&amp;rsquo;avais appris.&lt;br /&gt;\r\nEn r&amp;eacute;sum&amp;eacute; cette formation m&amp;rsquo;a mis&amp;nbsp;en capacit&amp;eacute; de communiquer en conscience &amp;raquo;', 'RETROUVER SA CONFIANCE', 'UTILISATION DES TECHNIQUES TH&amp;Eacute;ATRALES&amp;nbsp;POUR D&amp;Eacute;PASSER SES LIMITES.', 'fr', 3, 1, 1),
(2, 'Elodie&amp;nbsp;', 'Si je devais mettre un seul qualificatif &amp;agrave; cette formation de Yan , je mettrai : EXCELLENTE.&lt;br /&gt;\r\n100% des techniques &amp;amp; id&amp;eacute;es qui ont &amp;eacute;t&amp;eacute; abord&amp;eacute;es durant cette formation sont imm&amp;eacute;diatement exploitables pour nous permettre d&amp;rsquo;&amp;ecirc;tre meilleur en prise de Parole en Public.&lt;br /&gt;\r\nMes &amp;nbsp;pr&amp;eacute;sentations et autres communications orales en public/Client&amp;egrave;le devraient &amp;ecirc;tre bien plus &amp;lsquo;professionnelles&amp;rsquo; dans le futur.', 'az', 'az', 'fr', 16, 0, 2),
(3, 'ELODIE&lt;br /&gt;\r\nRESPONSABLE COMMERCIALE , PARIS', '&amp;laquo; Si je devais mettre un seul qualificatif &amp;agrave; cette formation de Yan , je mettrai : EXCELLENTE.&lt;br /&gt;\r\n100% des techniques &amp;amp; id&amp;eacute;es qui ont &amp;eacute;t&amp;eacute; abord&amp;eacute;es durant cette formation sont mm&amp;eacute;diatement exploitables pour nous permettre d&amp;rsquo;&amp;ecirc;tre meilleur en prise de Parole en Public.&lt;br /&gt;\r\nMes pr&amp;eacute;sentations et autres communications orales en public/Client&amp;egrave;le devraient &amp;ecirc;tre bien plus &amp;lsquo;professionnelles&amp;rsquo; dans le futur. &amp;raquo;', 'GAGNER EN AISANCE &amp;Agrave; L&amp;#39;ORAL / G&amp;Eacute;RER SON TRAC', 'UTILISATION DES TECHNIQUES TH&amp;Eacute;&amp;Acirc;TRALES POUR SE PR&amp;Eacute;PARER ET S&amp;#39;ENTRAINER&lt;br /&gt;\r\nJEUX DE R&amp;Ocirc;LE ET TECHNIQUES DE PNL', 'fr', 17, 1, 3),
(4, 'RICHARD&lt;br /&gt;\r\nRESPONSABLE TECHNIQUE GRANDS COMPTES , PARIS', '&amp;laquo; C&amp;#39;est meilleure formation que j&amp;#39;ai faite :&lt;br /&gt;\r\n- Yan est un excellent formateur et animateur&lt;br /&gt;\r\n- Les &amp;eacute;l&amp;eacute;ments appris sont utilisables pour les prises de parole en public, en animation de r&amp;eacute;union, et simple dialogue.&lt;br /&gt;\r\n- Les &amp;eacute;l&amp;eacute;ments transmis sont complets (posture, respiration, d&amp;eacute;bit, ma&amp;icirc;trise de l&amp;#39;espace, ...)&lt;br /&gt;\r\n- Les b&amp;eacute;n&amp;eacute;fices sont visibles des la 1er heure de formation et tout au long de la formation. &amp;raquo;', 'D&amp;Eacute;VELLOPER SON IMPACT ET SON CHARISME EN PRISE DE PAROLE EN PUBLIC', 'TRAVAIL SUR LE PROJET DE VIE PROFESSIONNEL&lt;br /&gt;\r\nTECHNIQUES D&amp;#39;IMPACT ET D&amp;#39;IMPROVISATION D&amp;#39;ACTEUR - JEUX DE R&amp;Ocirc;LES', 'fr', 18, 1, 4),
(5, 'NATHALIE&lt;br /&gt;\r\nRESPONSABLE RH , PARIS', '&amp;laquo;&amp;nbsp;Merci &amp;nbsp;pour cette formation de grande qualit&amp;eacute;, dont nous pourrons conserver de nombreux b&amp;eacute;n&amp;eacute;fices pour notre activit&amp;eacute;.&lt;br /&gt;\r\nUn intervenant talentueux, passionn&amp;eacute;, qui a su nous communiquer son enthousiasme et les &amp;laquo; ficelles du m&amp;eacute;tier &amp;raquo; pour am&amp;eacute;liorer notre prestance en public. &amp;raquo;', 'SAVOIR R&amp;Eacute;AGIR &amp;Agrave; TOUTES LES OBJECTIONS EN R&amp;Eacute;UNION OU EN RENDEZ-VOUS&lt;br /&gt;\r\nSAVOIR DIRE NON', 'TRAVAIL SUR LE R&amp;Ocirc;LE PROFESSIONNEL, L&amp;#39;&amp;Eacute;COUTE, ET LES BLOCAGES MENTAUX AVEC LES OUTILS DE LA PNL&lt;br /&gt;\r\nRENFORCEMENT DE L&amp;#39;ASSERTIVIT&amp;Eacute; AVEC LES JEUX D&amp;#39;ACTEUR&lt;br /&gt;\r\nD&amp;Eacute;COUVERTE DU MOD&amp;Egrave;LE PROCESS COM - TRAVAIL SUR LES DRIVERS', 'fr', 19, 1, 5),
(6, 'MARIE HAUDE&lt;br /&gt;\r\nCONSULTANTE FORMATRICE , BREST', '&amp;laquo;&amp;nbsp;Le juste &amp;eacute;quilibre entre l&amp;rsquo;explication de la th&amp;eacute;orie et la mise en pratique,&amp;nbsp;l&amp;rsquo;approche, la souplesse p&amp;eacute;dagogique de Yan, m&amp;rsquo;ont permis d&amp;rsquo;utiliser, d&amp;egrave;s le lendemain, ce que j&amp;rsquo;avais appris.&lt;br /&gt;\r\nEn r&amp;eacute;sum&amp;eacute; cette formation m&amp;rsquo;a mis en capacit&amp;eacute; de communiquer en conscience ;o) &amp;raquo;', 'MIEUX COMMUNIQUER DANS LE CADRE PROFESSIONNEL&lt;br /&gt;\r\nG&amp;Eacute;RER SON STRESS &amp;Agrave; L&amp;#39;ORAL', 'D&amp;Eacute;CODAGE DES JEUX PSYCHOLOGIQUES AVEC L&amp;#39;ANALYSE TRANSACTIONNELLE.&lt;br /&gt;\r\nINSTALLER ET MAINTENIR DES RELATIONS DE QUALIT&amp;Eacute; AVEC LA PNL&lt;br /&gt;\r\nUTILISATION DU MOD&amp;Egrave;LE PROCESS COM', 'fr', 20, 1, 6),
(7, 'HUGUES&lt;br /&gt;\r\nDENTISTE , CHAMB&amp;Eacute;RY', '&amp;laquo;&amp;nbsp;Tr&amp;egrave;s bonne formation. Mes attentes ont &amp;eacute;t&amp;eacute; prises en compte et trait&amp;eacute;es de mani&amp;egrave;re constructive. &amp;raquo;', 'INSTALLER UN MODE RELATIONNEL CONVIVIAL ET ADAPT&amp;Eacute; &amp;Agrave; CHAQUE PATIENT&lt;br /&gt;\r\nMIEUX G&amp;Eacute;RER SON TEMPS AVEC LES PATIENTS', 'UTILISATION PRATIQUE DU MOD&amp;Egrave;LE PROCESS COM&lt;br /&gt;\r\nD&amp;Eacute;CODAGE DES SITUATIONS COMPLEXES AVEC L&amp;#39;APPROCHE SYST&amp;Eacute;MIQUE&lt;br /&gt;\r\nD&amp;Eacute;COUVERTE DES PROCESSUS MENTAUX LI&amp;Eacute;S AU TEMPS EN PNL ET EN AT', 'fr', 21, 1, 7),
(8, 'EVA&lt;br /&gt;\r\nCONSEILL&amp;Egrave;RE CLIENT&amp;Egrave;LE , NIORT', '&amp;laquo; Formation tr&amp;egrave;s vivante et ludique. J&amp;#39;ai beaucoup appr&amp;eacute;ci&amp;eacute; les jeux de r&amp;ocirc;les tr&amp;egrave;s instructifs et vari&amp;eacute;s. &amp;raquo;', 'ACCOMPAGNER ET MIEUX MANAGER UNE EQUIPE DE CONSEILLERS &amp;Eacute;L&amp;Eacute;PHONIQUES', 'TECHNIQUES DE COMMUNICATION DE LA PNL&lt;br /&gt;\r\nOUTILS DE MOTIVATION ISSUS DE LA PNL&lt;br /&gt;\r\nTRAVAIL SUR LES CROYANCES LI&amp;Eacute;ES AU MANAGEMENT&lt;br /&gt;\r\nUTILISATION DE PCM POUR LES STYLES DE MANAGEMENT', 'fr', 22, 1, 8),
(9, 'LAMYA&lt;br /&gt;\r\nRESPONSABLE P&amp;Eacute;DAGOGIQUE , LEVALLOIS', '&amp;laquo; C&amp;rsquo;est vraiment une des meilleures formations que j&amp;rsquo;ai eues.&lt;br /&gt;\r\nTout y est : la m&amp;eacute;thode p&amp;eacute;dagogique impliquant chacun, le contenu qui va bien au-del&amp;agrave; de la prise de parole classique, les qualit&amp;eacute;s d&amp;rsquo;orateur, de formateur mais aussi humaines de Yan et son exp&amp;eacute;rience ont rendu cette formation est super enrichissante. &amp;raquo;', 'MIEUX G&amp;Eacute;RER SES &amp;Eacute;MOTIONS DANS LE TRAVAIL D&amp;#39;ENSEIGNEMENT&lt;br /&gt;\r\nREDONNER MOTIVATION ET PLAISIR AUX &amp;Eacute;L&amp;Egrave;VES', 'UTILISATION DU CONCEPT DES POSITIONS DE VIE EN AT&lt;br /&gt;\r\nG&amp;Eacute;RER SES &amp;Eacute;MOTIONS AVEC L&amp;#39;INTELLIGENCE &amp;Eacute;MOTIONNELLE&lt;br /&gt;\r\nLES &amp;Eacute;TAPES DE L&amp;#39;APPRENTISSAGE ET LA VIE D&amp;#39;UN GROUPE&lt;br /&gt;\r\nOUTILS DE MOTIVATION ISSUS DE LA PNL ET DU MOD&amp;Egrave;LE PROCESS COM&lt;br /&gt;\r\n&amp;nbsp;', 'fr', 23, 1, 9),
(10, 'MICH&amp;Egrave;LE&lt;br /&gt;\r\nRESPONSABLE JURIDIQUE , &amp;nbsp;BOULOGNE', '&amp;laquo; Yan est un excellent animateur avec une impressionnante capacit&amp;eacute; de facilitateur.&amp;nbsp;&lt;br /&gt;\r\nIl est Percutant et bienveillant &amp;nbsp;Sa posture th&amp;eacute;atrale permet d&amp;#39;aborder les sujets individuels sans jugement ni tabou &amp;shy; &amp;nbsp;Tr&amp;egrave;s bon balance th&amp;eacute;orie&amp;shy; / pratique durant le stage&lt;br /&gt;\r\nExcellente ma&amp;icirc;trise du sujet enseign&amp;eacute;. &amp;raquo;', 'RETROUVER CONFIANCE EN SOI A L&amp;#39;ORAL&lt;br /&gt;\r\nACCOMPAGNEMENT SUR UNE PRISE DE POSTE', 'TRAVAIL SUR LES CROYANCES LIMITANTES AVEC LA PNL&lt;br /&gt;\r\nUTILISATION DES TECHNIQUES D&amp;#39;ANCRAGE&lt;br /&gt;\r\nD&amp;Eacute;COUVERTE DE LA COURBE DU CHANGEMENT ET DES R&amp;Eacute;SISTANCES', 'fr', 24, 1, 10);

-- --------------------------------------------------------

--
-- Structure de la table `yannmercoeur_user`
--

CREATE TABLE IF NOT EXISTS `yannmercoeur_user` (
  `id_user` smallint(6) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_user`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Contenu de la table `yannmercoeur_user`
--

INSERT INTO `yannmercoeur_user` (`id_user`, `email`, `password`, `firstname`, `lastname`, `is_admin`) VALUES
(1, 'julien@agence2web.com', '404b494fbc2b241200f9936f33cf564c', 'Julien', 'Guion', 1),
(5, 'antipodesmultimedia@gmail.com', 'f3d6b499dfd56a0f371f21f6f64aff45', 'Admin', 'Antipodes', 0),
(6, 'contact@yanmercoeur.com', 'b1cb5ef3d3fe1e4c330b2d57bd112608', 'Yan', 'Mercoeur', 2);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
