<?php
require_once('.loader.php');

Database :: verifyConnexion();

$error = null;
$success = null;

//require(PATH_TRAITEMENTS . 'index.php');

Page :: setIndexMenu(1);
Page :: setTitle($globals['TITLE_SITE']);
Page :: setMetaDescription($globals['META_DESC_DEFAULT']);

Trigger :: call('onStartHTML');
?>
<!-- ******************************************************* HOME ********************************** -->	
<!-- theme slider -->
<div class="homepage-slider-wrap">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="homepage-slider">
                    <ul class="slides">
                        <li style="background-image: url('<?=PATH_HTTP_IMG_THEME?>homepage-slider01.jpg');">
                            <div class="overlay"></div>
                        </li>
                        <li style="background-image: url('<?=PATH_HTTP_IMG_THEME?>homepage-slider02.jpg');">
                            <div class="overlay"></div>
                        </li>
                        <li style="background-image: url('<?=PATH_HTTP_IMG_THEME?>homepage-slider03.jpg');">
                            <div class="overlay"></div>
                        </li>
                        <li style="background-image: url('<?=PATH_HTTP_IMG_THEME?>homepage-slider04.jpg');">
                            <div class="overlay"></div>
                        </li>
                        <li style="background-image: url('<?=PATH_HTTP_IMG_THEME?>homepage-slider05.jpg');">
                            <div class="overlay"></div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="homepage-quote">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2">
                <div class="text"><?=$globals['HOMEPAGE_QUOTE']?></div>
            </div>
        </div>
    </div>
</section>
<section class="hp-tabs">
	<div class="title-bar"><?=$globals['DISCOVER_SELECTION']?></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="grid-products-wrapper">
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product01.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <?php /*<div class="price">515,00$</div>*/ ?>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.php" class="view-product">
                                    <div class="text">
										<span class="product-view"><?=$globals['VIEW_PRODUCT']?></span>
										<span class="product-name">Fauteuil Roche Bobois</span>
                                    </div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product02.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.php" class="view-product">                                    <div class="text">
										<span class="product-view"><?=$globals['VIEW_PRODUCT']?></span>
										<span class="product-name">Fauteuil Roche Bobois</span>
                                    </div>
<div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product03.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.php" class="view-product">                                    <div class="text">
										<span class="product-view"><?=$globals['VIEW_PRODUCT']?></span>
										<span class="product-name">Fauteuil Roche Bobois</span>
                                    </div>
<div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product04.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.php" class="view-product">                                    <div class="text">
										<span class="product-view"><?=$globals['VIEW_PRODUCT']?></span>
										<span class="product-name">Fauteuil Roche Bobois</span>
                                    </div>
<div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product05.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.php" class="view-product">                                    <div class="text">
										<span class="product-view"><?=$globals['VIEW_PRODUCT']?></span>
										<span class="product-name">Fauteuil Roche Bobois</span>
                                    </div>
<div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product06.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.php" class="view-product">                                    <div class="text">
										<span class="product-view"><?=$globals['VIEW_PRODUCT']?></span>
										<span class="product-name">Fauteuil Roche Bobois</span>
                                    </div>
<div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product07.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.php" class="view-product">                                    <div class="text">
										<span class="product-view"><?=$globals['VIEW_PRODUCT']?></span>
										<span class="product-name">Fauteuil Roche Bobois</span>
                                    </div>
<div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product08.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.php" class="view-product">                                    <div class="text">
										<span class="product-view"><?=$globals['VIEW_PRODUCT']?></span>
										<span class="product-name">Fauteuil Roche Bobois</span>
                                    </div>
<div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product09.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.php" class="view-product">                                    <div class="text">
										<span class="product-view"><?=$globals['VIEW_PRODUCT']?></span>
										<span class="product-name">Fauteuil Roche Bobois</span>
                                    </div>
<div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product10.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.php" class="view-product">                                    <div class="text">
										<span class="product-view"><?=$globals['VIEW_PRODUCT']?></span>
										<span class="product-name">Fauteuil Roche Bobois</span>
                                    </div>
<div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product11.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.php" class="view-product">                                    <div class="text">
										<span class="product-view"><?=$globals['VIEW_PRODUCT']?></span>
										<span class="product-name">Fauteuil Roche Bobois</span>
                                    </div>
<div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product12.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.php" class="view-product">                                    <div class="text">
										<span class="product-view"><?=$globals['VIEW_PRODUCT']?></span>
										<span class="product-name">Fauteuil Roche Bobois</span>
                                    </div>
<div class="background"></div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div><!-- //grid-products-wrapper -->
            </div>
        </div><!-- //row -->           
        <div class="row">
            <div>
                <div class="col-xs-3 col-xs-push-9">
                	<a href="<?=getActualLanguage()?>/nouveautes" class="bt-white"><?=$globals['VIEW_NEW_PRODUCTS']?></a>
                </div>
            </div>
        </div>
            
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-sm-12">
      		<div class="section-line"></div>
        </div>
    </div>
</div>
<section class="hp-section">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <a href="#">
                	<img src="<?=PATH_HTTP_IMG_GLOBAL?>pub.jpg" alt=""/>
                </a>
            </div>
            <div class="col-sm-3">
                <a href="#">
                	<img src="<?=PATH_HTTP_IMG_GLOBAL?>pub-brut.jpg" alt=""/>
                </a>
            </div>
        </div>
    </div>
</section>
<?
	HTML_Script :: addScript('
	');
?>