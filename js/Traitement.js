$(document).ready(function(){
	/*
		HOME PAGE : ROYAL SLIDER
	*/
	$(".royalSlider").royalSlider({
        keyboardNavEnabled  : true,
        imageScaleMode 		: 'fill',
        controlNavigation	: 'none',
        autoScaleSliderHeight: 400,
        loop				: true,
        loopRewind			: true,
        autoPlay: {
    		enabled: true,
    		pauseOnHover: true
    	},
    	transitionType		: 'move'
    });




    /*
        ACCORDION
    */
    if ( $('.accordion').length !== 0) $('.accordion').accordion();


    /*
        Menu Responsive
    */
    //$('body').html('<span>' + document.body.clientWidth + '</span>');


/*    setTimeout(function(){
        $sNameIframe = $('.entete iframe').attr('name');
        alert($sNameIframe);
        $sNameIframe.$('a.pluginShareButtonLink').css({ backgroundColor:'red' });
    }, 3000);*/

    function activateSlideBar()
    {
        $.slidebars();
         // Slidebars Submenus
        $('.sb-toggle-submenu').off('click').on('click', function() 
        {
            $submenu = $(this).parent().children('.sb-submenu');
            $li = $(this).parent('li');
            $(this).add($submenu).toggleClass('sb-submenu-active');
            
            if ($submenu.hasClass('sb-submenu-active')) 
            {
                $li.removeClass('element');
                $submenu.stop().slideDown(200);
            } 
            else 
            {
                $li.addClass('element');
                $submenu.stop().slideUp(200);
            }
        });
    }

    setTimeout(function(){activateSlideBar();}, 200);

    $(document).on('click', '.sb-slidebar .element-parent', function(e)
    {
        e.preventDefault();
        $eParent = $(this);

        $eParent.toggleClass('element-parent-active');

        $aClasses = $(this).attr('class').split(' ');
        $sClass = $aClasses[2];

        $_idParent = $sClass.split('-');
        $idParent = $_idParent[2];

        // Enfants existants
        if ( $('.element-child-' + $idParent).length > 0 )
        {
            $('.element-child-' + $idParent + ':not(.element-child-active)').stop().slideDown();
            $('.element-child-active.element-child-' + $idParent).stop().slideUp();

            $('.element-child-' + $idParent).toggleClass('element-child-active');
        }
    });

   /*
        Menu Responsive : Affectation de l'élement parent actif
   */
    if ( $('li.element-child.current').length > 0 )
    {
        $aClasses = $('li.element-child.current').attr('class').split(' ');
        $sClass = $aClasses[2];
        $_idParent = $sClass.split('-');
        $idParent = $_idParent[2];

        $('li.element-parent-' + $idParent).addClass('current');
    }
    
    

    function getBaseURL ($sString) 
    {
        // Si le paramètre n'est pas défini on lui donne une valeur par défaut
        $sString = typeof $sString !== 'undefined' ? $sString : "";

         if (!window.location.origin)
            window.location.origin = window.location.protocol+"//"+window.location.host; 
        return window.location.origin + '/' + $sString;
    }

    /*
        BLOG : INFINITE SCROLL
    */
    if ( $('.blog .articles').length !== 0 )
    {
        $('.blog .articles').infinitescroll({
            loading:
            {
                msgText:"",
                finishedMsg:"",
                img: getBaseURL('img/global/ajax-loader.gif'),
                speed: 'slow'
            },
            state: {
                currPage: 1
            },
            nextSelector:".paging-navigation a.next_element",
            navSelector:".paging-navigation",
            itemSelector:".single_article",
            contentSelector:".blog .articles",
            debug:false,
            path: function(){ return $('a.next_element').attr('href'); },
            behavior:"",
            callback:""
        }, function(newElements, data, url) {
            // Capture de l'URL courant
            var $sUrl = $('a.next_element').attr('href');
            // Capture de la page courante
            $iPageNumber = $sUrl.substring( $sUrl.lastIndexOf("/") + 1 );
            //console.log('iPageNumber = ' + $iPageNumber);
            // Définition de la page suivante
            $iNextPage = (parseInt($iPageNumber)+1);
            //console.log('iNextPage = ' + $iNextPage);
            // Construction de l'URL pour la page suivante
            $sUrl = $sUrl.substring(0, $sUrl.lastIndexOf("/") + 1 ) + $iNextPage;
            //console.log('sUrl2 = ' + $sUrl);
            // Mise en place de l'URL
            $('a.next_element').attr('href', $sUrl);
            $('a.next_element').text($iNextPage);
            if ( $iNextPage <= 0 ) $('.blog .articles').infinitescroll('pause');
        });
    }

    

    /*
        INPUT FILE
    */
    $(document).on('change', 'input[type="file"]', function(e){
        var $sValue = $(this).val();
        $(this).parents('.form_line').children('.field_path').val($sValue);
    });
    
    // Animation des boutons de socials
    /*$(document).on('mouseenter', '.actus-socials .socials li, .actus-socials .socials li a, .pageinterne .socials li, .pageinterne .socials li a', function(e){
        // Il s'agit d'une balise a
        if ( $(this).hasClass('block') )
        {
            $(this).parent('li').stop().animate({ 'border-color': "#d5655d" });
            $(this).stop().css({ 
                color: "#d5655d",
                'background-position': '50% 0%'
            });
        }
        // Sinon c'est une balise li
        else
        {
            $(this).stop().animate({ 'border-color': "#d5655d" });
            $(this).children('a').stop().css({ 
                color: "#d5655d", 
                'background-position': '50% 0%'
            });
        }
    });
    $(document).on('mouseout', '.actus-socials .socials li, .actus-socials .socials li a, .pageinterne .socials li, .pageinterne .socials li a', function(e){
        // Il s'agit d'une balise a
        if ( $(this).hasClass('block') )
        {
            $(this).parent('li').stop().animate({ 'border-color': "#c4c3c3" });
            $(this).stop().css({ 
                color: "#c4c3c3",
                'background-position': '50% 100%'
            });
        }
        // Sinon c'est une balise li
        else
        {
            $(this).stop().animate({ 'border-color': "#c4c3c3" });
            $(this).children('a').stop().css({ 
                color: "#c4c3c3",
                'background-position': '50% 100%'
            });
        }
    });*/
    $(document).on('click', '.actus-socials .socials li a, .pageinterne .socials li a', function(e){
        if ( $(this).hasClass('block') ) e.preventDefault();
    });
    // En cliquant sur le li d'une social
    $(document).on('click', '.actus-socials .socials li, .pageinterne .socials li', function(e){
        if ( $(this).hasClass('block') ) e.preventDefault();
        window.open($(this).children('a').attr('href'));
    });



	
	$('#newsletter').submit(function(){
		console.log($(this));
		var result = true;
		
		var newsletter = $(this).find('#newsletter-email').val();
		if(newsletter == ''){ $(this).find('#newsletter-email').addClass('error'); result = false; }
		
		if(result === true){
		
		//Creation de la variable POST
		var postSend = 'action=newsletter&newsletter='+newsletter;
		
			$.ajax({
			   type: 'POST',
			   url: baseUri + 'ajax.php',
			   data: postSend,
			   success: function(msg){
			   	if(msg==1){
			   		$('#newsletter')[0].reset();
			   		$.magnificPopup.open({
						  items: {
						    src: '<div class="white-popup">Votre email a été enregistrer</div>',
						    type: 'inline'
						  }
						});
			   	}
			   	else{
			   		var temp = msg.split('*|*');
			   		$.magnificPopup.open({
						  items: {
						    src: '<div class="white-popup">'+temp[1]+'</div>',
						    type: 'inline'
						  }
						});
			   	}
			   	
			   }
			 }); 
		}
		
		return false;
	});
	
	//Validation des formulaires
	/*jQuery.validator.setDefaults({
		debug: true,
		success: "valid"
	});
	*/
	/*
	$('.formulaire_type').validate({
	 rules: {
			cv: {
				required: false
			},
			motivation: {
				required: false
			}
		}
	});
	*/
	
});