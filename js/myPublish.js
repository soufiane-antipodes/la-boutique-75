/**
* Modules Publish/Unpublish
*
*
*
*/

(function($) 
{
	
	$.fn.myPublish = function(options) 
	{

		var defaults = 
		{
			table : '',
			nameId : '',
			idRelation : 0
		};

		var opts = $.extend({}, defaults, options);
		var plugin = this;


		$('.publish').click(function()
		{
			var idValue = 0;
			// Si il posséde une relation, on publie/dépublie toute ses relations, SINON on ne prend rien que son ID
			idValue = ( $(this).attr('rel').indexOf('publish-') >= 0 ) ? $(this).attr('rel') : this.id;
			console.log(idValue);
			idValue = idValue.replace('publish-', '');
			var statusValue = $(this).is(':checked');

			if(statusValue == 1) statusValue = 1;
			else statusValue = 0;

			$.ajax(
			{
				type: 'GET',
				url: 'ajax.php',
				data: 'action=publish&tableName=' + opts.table + '&idName=' + opts.nameId + '&status=' + statusValue + '&id=' + idValue,
				success: function(msg) { console.log(msg); }
			}); 

		});

		$('.publishHomePage').click(function()
		{
			var idValue = this.id;
			idValue = idValue.replace('publishHomePage-', '');
			var statusValue = $(this).is(':checked');

			if(statusValue == 1) statusValue = 1;
			else statusValue = 0;

			$.ajax(
			{
				type: 'GET',
				url: 'ajax.php',
				data: 'action=publishHomePage&tableName='+opts.table+'&idName='+opts.nameId+'&id='+idValue+'&status='+statusValue,
				success: function(msg){ console.log(msg); }
			}); 

		});

		return this;
	};

})(jQuery);