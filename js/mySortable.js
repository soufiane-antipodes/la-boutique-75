/**
* Modules Sortable position
*
*
*
*/

(function($) 
{

    $.fn.mySortable = function(options) 
    {
    	var defaults = 
        {
    		table : '',
    		nameId : '',
    		handle : '.move',
            multiply: 1
    	};
        var opts = $.extend({}, defaults, options);
        var plugin = this;

        /**
        * Drag and Drop
        */
        $(this).sortable(
        {	
    		revert : true, 
    		cursor : 'move', 
    		handle : opts.handle,
    	    opacity : 0.8,
    	    update : function(event, ui)
            {
        		postToSend = opts.table+'*|*'+opts.nameId+'*|*';
        		$(this).find('.entry').each(function()
                {
                    var idName = this.id;
                    idName = idName.replace('myEntry-', '');
                    postToSend += idName+'*|*';
                });
                $.ajax(
                {
    			    type: 'POST',
    			    url: 'ajax.php',
    			    data: 'action=updatePosition&value=' + escape(postToSend) + '&multiply=' + opts.multiply
    			}).done(function(data){
                    console.log(data);
                });
    	    }
    	});
        return this;
    };

})(jQuery);