<? $sNomPage = str_replace('_', '-', Sanitize :: keepValidChars(Request :: getField('nom')) ); ?>

<? require(PATH_TRAITEMENTS . 'aside.php'); ?>

<aside>
	<? if ( $aSubPages ) : ?>
		<nav>
			<ul>
				<? foreach ($aSubPages as $iKey => $sValue) : ?>
					<li <?= ($iKey === 0) ? 'class="first"' : null ; ?> >
						<? 
							$sUrl = $sValue['id'] . '-' . Sanitize :: keepValidCharsRewriting($sValue['name']); 
						   	$sUrl = str_replace('_', '-', $sUrl);
						?>
						<a href="<?= getActualLanguage() . '/page/' . $sUrl ?>" class="box-sizing <?= ( $sUrl === $sNomPage ) ? 'current' : null ?>"><?= $sValue['name'] ?></a>
					</li>
				<? endforeach ?>
			</ul>
		</nav>
	<? endif ?>
	<?php if(isset($aParentPage['id_item']) && $aParentPage['id_item'] > 0 || isset($aPage['id_item']) && $aPage['id_item'] > 0) : ?>
		<?php if(isset($aParentPage)) : ?>
			<img class="imagetop" src="/../timthumb.php?src=<?= PATH_HTTP_IMG_PAGES . Sanitize :: keepValidChars($aParentPage['value']) . '-' . $aParentPage['id_item'] . '.' . $aParentPage['extension']?>&amp;zc=1&amp;w=370&amp;h=370&amp;q=100" alt="Demander un devis" />
		<?php else : ?>
			<img class="imagetop" src="/../timthumb.php?src=<?= PATH_HTTP_IMG_PAGES . Sanitize :: keepValidChars($aPage['value']) . '-' . $aPage['id_item'] . '.' . $aPage['extension']?>&amp;zc=1&amp;w=370&amp;h=370&amp;q=100" alt="Demander un devis" />
		<?php endif; ?>
		<div class="devis box-sizing">
			<h2><?=$globals['ASIDE_DEVIS_TITLE']?></h2>
			<a href="<?= getActualLanguage() . '/devis' ?>" class="arrow_right"><?=$globals['ASIDE_DEVIS_LINK']?></a>
		</div>
	<?php endif; ?>
	<div class="socials box-sizing">
		<ul>
			<li class="facebook"><a href="<?=$globals['FB']?>" class="block" target="_blank">Facebook</a></li>
			<li class="twitter"><a href="<?=$globals['TW']?>" class="block" target="_blank">Twitter</a></li>
			<li class="youtube"><a href="<?=$globals['YT']?>" class="block" target="_blank">Youtube</a></li>
			<li class="linkedin"><a href="<?=$globals['LD']?>" class="block" target="_blank">Linked In</a></li>
		</ul>
	</div>
	<article class="last_actu box-sizing">
		<h2><?=$globals['ASIDE_LAST_ACT']?></h2>
		<a href="<?= getActualLanguage() . '/blog/' . $aArticle['id_article'] . '-' . Sanitize :: keepValidCharsRewriting($aArticle['title']) ?>" >
			<img src="/../timthumb.php?src=<?= PATH_HTTP_IMG_ARTICLES . Sanitize :: keepValidChars($aArticle['value']) . '-' . $aArticle['id_item'] . '.' . $aArticle['extension'] ?>&amp;zc=1&amp;w=370&amp;h=200&amp;q=100&amp;zc=2" alt="<?= $aArticle['title'] ?>" >
		</a>
		<h3><?= $aArticle['title'] ?></h3>
		<em><?=$globals['ACT_DATE']?> <?= date('d/m/Y', strtotime($aArticle['date_creation']) ) ?></em>
		<?
			// Si la description dépasse le maximum de caractères par extrait, on lui rajoute 3 petits points
			$sDescription = htmlspecialchars_decode($aArticle['description']);
			$sDescription = ( strlen($sDescription) > 140 ) ? Sanitize :: html_excerpt($sDescription, 140) . '...' : $sDescription;
		?>
		<p><?= $sDescription ?></p>
		<a href="<?= getActualLanguage() . '/blog/' . $aArticle['id_article'] . '-' . Sanitize :: keepValidCharsRewriting($aArticle['title']) ?>" class="arrow_right"><?=$globals['ACT_MORE']?></a>
	</article>
</aside>