<?php
	global $globals;
?>	
<div class="container footer-line"></div>
<footer class="main-footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="footer-widget">
                    <h2><?=$globals['SEARCH_TITLE']?></h2>
                    <div class="footer-search">
                        <form action="">
                            <div class="input-group">
                                <input type="text" class="form-control" name="s">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="footer-widget">
                    <h2>Products</h2>
                    <div class="footer-menu">
                        <ul>
                            <li><a href="#">Armchairs & Sofas</a></li>
                            <li><a href="#">Office chairs</a></li>
                            <li><a href="#">Beds</a></li>
                            <li><a href="#">Living room Storage</a></li>
                            <li><a href="#">TV & media furniture</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="footer-widget">
                    <h2><?=$globals['FOOTER_NEWSLETTER2']?></h2>
                    <div class="footer-newsletter">
                        <form action="">
                            <div class="input-group">
                                <input type="text" class="form-control" name="s" placeholder="<?=$globals['FOOTER_MAIL']?>">
                                <span class="input-group-btn">
                                    <button class="btn btn-default" type="submit"><i class="fa fa-newsletter">OK</i></button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <h2><?=$globals['FOOTER_SOCIALS']?></h2>
                    <ul class="social-icons">
                        <li><a href="#" class="social-icon twitter"></a></li>
                        <li><a href="#" class="social-icon instagram"></a></li>
                        <li><a href="#" class="social-icon googleplus"></a></li>
                        <li><a href="#" class="social-icon pinterest"></a></li>
                        <li><a href="#" class="social-icon linkedin"></a></li>
                        <li><a href="#" class="social-icon facebook"></a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="final-line">
                </div>
            </div>
        </div>
    </div>
</footer>
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="login-form">
                    <h3>Let's get started</h3>
                    <form role="form" method="post">
                        <div class="form-group">
                            <label for="login-username">Username or email</label>
                            <input type="text" class="form-control" id="login-username" name="username">
                        </div>
                        <div class="form-group">
                            <label for="login-password">Password</label>
                            <input type="password" class="form-control" id="login-password" name="password">
                        </div>
                        <div class="form-group">
                            <a class="forgotten" href="#">Forgot password?</a>
                            <button type="submit" class="btn btn-default">Login</button>
                        </div>
                    </form>
                    <a class="fb-login" href="#">Login with Facebook</a>
                </div>
                <hr/>
                <div class="register-form">
                    <h3>New customer</h3>
                    <form role="form" method="post">
                        <div class="form-group">
                            <label for="register-username">Username</label>
                            <input type="text" class="form-control" id="register-username" name="username">
                        </div>
                        <div class="form-group">
                            <label for="register-email">Email</label>
                            <input type="email" class="form-control" id="register-email" name="email">
                        </div>
                        <div class="form-group">
                            <label for="register-password">Password</label>
                            <input type="text" class="form-control" id="register-password" name="password">
                        </div>
                        <div class="form-group">
                            <label for="register-password-confirm">Password</label>
                            <input type="text" class="form-control" id="register-password-confirm" name="password-confirm">
                        </div>
                        <div class="form-group">
                            <button type="submit" class="btn btn-default">Register</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<?php /*		
			<footer class="full-float box-sizing">
				<div class="centered">
					<div class="footer_left footer_part">
						<h5><?=$globals['FOOTER_NEWSLETTER']?></h5>
						<p><?=$globals['FOOTER_NEWSLETTER2']?></p>
						<form method="POST" action="<?=$_SERVER['REQUEST_URI']?>" id="newsletter" class="formulaire_type">
							<input type="email" id="newsletter-email" class="newsletter_input box-sizing outline removeAppearence required" placeholder="<?=$globals['FOOTER_MAIL']?>" name="newsletter">
							<input type="submit" value="OK" class="removeAppearence" >
						</form>
					</div>
					<div class="footer_middle footer_part">
						<h5><?=$globals['FOOTER_QUESTIONNAIRE']?></h5>
						<p><a <?=(Page :: getIndexMenu() == 90) ? 'class="current"' : null;?> href="<?= getActualLanguage() . '/questionnaire-satisfaction' ?>"><?=$globals['FOOTER_SATISFACTION']?></a></p>
						<p><a <?=(Page :: getIndexMenu() == 80) ? 'class="current"' : null;?> href="<?= getActualLanguage() . '/recrutement' ?>"><?=$globals['FOOTER_RECRUTEMENT']?></a></p>
					</div>
					<div class="footer_right footer_part">
						<h5><?=$globals['FOOTER_CP']?></h5>
						<p><a <?=(Page :: getIndexMenu() == 70) ? 'class="current"' : null;?> href="<?= getActualLanguage() . '/mentions-legales' ?>"><?=$globals['FOOTER_ML']?></a></p>
						<p><a href="http://www.agence2web.com" target="_blank"><?=$globals['FOOTER_A2W']?> Agence2Web</a></p>
					</div>
				</div>
				
			</footer>
		</div> <!--  Fermeture du container principal -->
		<div class="sb-slidebar sb-left">
		  <? include(PATH_LOAD . 'menu_responsive.php'); ?>
		</div>
*/ ?>	
	<!-- Auto-Generated Scripts -->
	<?=HTML_Script::displayAll();?>
	
	<?php /*                	       
	<script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-51179673-1', 'dvai.fr');
		ga('send', 'pageview');
	</script>
	*/ ?>
	
	</body>
</html>
<? Trigger::call('onEndParseHtml'); ?>