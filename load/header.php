<?php 
	global $globals;
?>

<!DOCTYPE html>
<html lang="<?=Session :: get('langToUse')?>">
<head>
	<? Trigger::call('onBeforeParseHeader');?>
  	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  	<title><?= Page :: getTitle() ?> </title>
	<meta name="description" content="<?= Page :: getMetaDescription() ?>">
	
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="viewport" content="width=device-width">
  
 	<meta property="og:title" content="<?= Page :: getTitle() ?>" >
	<meta property="og:description" content="<?= Page :: getMetaFbDescription() ?>" >
	<meta property="og:image" content="<?=Page :: getMetaImage()?>" >
	<meta property="og:url" content="<?=$_SERVER['REQUEST_URI']?>" >
	
	<!--
	<link rel="alternate" href="<?=buildLangURL('fr')?>" hreflang="fr" />
	<link rel="alternate" href="<?=buildLangURL('en')?>" hreflang="en" />
	<?php if($globals['LANG_ES'] == 'On') : ?><link rel="alternate" href="<?=buildLangURL('sp')?>" hreflang="sp" /><?php endif; ?>
	<?php if($globals['LANG_DE'] == 'On') : ?><link rel="alternate" href="<?=buildLangURL('de')?>" hreflang="de" /><?php endif; ?>
	-->
	
 	<link rel="shortcut icon" href="<?=PATH_HTTP_IMG_FAVICON?>favicon.png?<?=date('YmdHis', filemtime(PATH_IMG_FAVICON.'favicon.png'))?>" />
	
	<!-- Stylesheets -->
    <link rel="stylesheet" href="<?=PATH_HTTP_CSS?>bootstrap.min.css">
    <link rel="stylesheet" href="<?=PATH_HTTP_CSS?>font-awesome.min.css">
    <link rel="stylesheet" href="<?=PATH_HTTP_CSS?>flexslider.css">
    <link rel="stylesheet" href="<?=PATH_HTTP_CSS?>jquery.selectBoxIt.css">
    <link rel="stylesheet" href="<?=PATH_HTTP_CSS?>main.css">
    
    <script src="<?=PATH_HTTP_JS_THEME?>modernizr-2.6.2-respond-1.1.0.min.js"></script>
    
	<script src="<?=PATH_HTTP_PLUGINS_JQUERY?>jquery-1.11.0.min.js" type="text/javascript"></script>
	<script src="<?=PATH_HTTP_PLUGINS_JQUERYUI?>jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>

  <!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
	<script type="text/javascript">
		baseUri = '<?=PATH_HTTP?>';
	</script>
	
  <?php
  		/*
	    HTML_CSS :: addFile(PATH_HTTP_PLUGINS_ROYALSLIDER . 'royalslider/royalslider.css');
		HTML_CSS :: addFile(PATH_HTTP_PLUGINS_ROYALSLIDER . 'royalslider/skins/default/rs-default.css?'.date('YmdHis'));
		HTML_CSS :: addFile(PATH_HTTP_PLUGINS_RESPONSIVEMENU . 'style.css?'.date('YmdHis'));
		*/
		
		HTML_Script :: addFile(PATH_HTTP_JS_THEME.'bootstrap.min.js'); 
		HTML_Script :: addFile(PATH_HTTP_JS_THEME.'jquery.isotope.min.js'); 
		HTML_Script :: addFile(PATH_HTTP_JS_THEME.'jquery.raty.min.js'); 
		HTML_Script :: addFile(PATH_HTTP_JS_THEME.'jquery.flexslider.js'); 
		HTML_Script :: addFile(PATH_HTTP_JS_THEME.'jquery.mousewheel.js'); 
		HTML_Script :: addFile(PATH_HTTP_JS_THEME.'jquery.mCustomScrollbar.js'); 
		HTML_Script :: addFile(PATH_HTTP_JS_THEME.'jquery.selectBoxIt.min.js'); 
		HTML_Script :: addFile(PATH_HTTP_JS.'main.js'); 
		
		/*
		HTML_Script :: addFile(PATH_HTTP_PLUGINS_GMAP3.'gmap3.min.js'); 
		HTML_Script :: addFile(PATH_HTTP_PLUGINS_SLIDEBARS.'slidebars.min.js'); 
		HTML_Script :: addFile(PATH_HTTP_PLUGINS_ACCORDION.'accordion.js'); 
		HTML_Script :: addFile(PATH_HTTP_PLUGINS_ROYALSLIDER.'royalslider/jquery.royalslider.min.js'); 
		HTML_Script :: addFile(PATH_HTTP_PLUGINS_TEXTFIT.'textFit.js');
		HTML_Script :: addFile(PATH_HTTP_PLUGINS_VALIDFORM.'jquery.validate.js');
		*/
		
		//Popup
		/*
	    HTML_CSS :: addFile(PATH_HTTP_PLUGINS_MAGNIFICPOPUP . 'dist/magnific-popup.css');
	    HTML_Script :: addFile(PATH_HTTP_PLUGINS_MAGNIFICPOPUP . 'dist/jquery.magnific-popup.min.js');

		HTML_Script :: addFile(PATH_HTTP_JS . 'Traitement.js');
		*/
		
		$device = new DetectMobile();
		$isAndroid = $device->DetectAndroid();
		if($isAndroid) Page :: setClassBody('android');
	?>
  
	<? Trigger::call('onAfterParseHeader');?>
</head>
<body class="<?=Page :: getClassBody()?>">
	<!-- Theme header -->
    <div class="navbar navbar-default">
      <div class="container">
          <div class="row">
              <div class="navbar-header col-sm-6">
                  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                  </button>
                  <a class="navbar-brand" href="/">
                      <span class="inner">
                          <img src="<?=PATH_HTTP_IMG_THEME?>logo.png" alt="LA BOUTIQUE 75"/>
                      </span>
                  </a>
              </div>
              <div class="col-sm-6 right-side">
                  <div class="navbar-collapse collapse">
                      <div class="row secondary-navigation-wrap">
							<?php
                            $langClass = '';
							$langSeparator = ' | ';
                            switch (Session :: get('langToUse')) { 
                                case 'fr':  $langClass = 'langFR-on';  break;
                                case 'en':  $langClass = 'langEN-on';  break;
                            }
                            ?>
                            <div class="lang-menu" role="menu">
                            	
                                <?= ($langClass=='langFR-on') ? '<span class="flag fr current">FR</span>' 
                                                              : '<a class="flag fr" href="'. buildLangURL('fr') .'">FR</a>'?>
                                <?=$langSeparator?>
                                <?= ($langClass=='langEN-on') ? '<span class="flag en current">EN</span>' 
                                                              : '<a class="flag en" href="'. buildLangURL('en') .'">EN</a>'?>
                            </div>
                          <ul class="nav navbar-nav secondary-navigation">
                              <li><a href="#">Wish List (0)</a></li>
                          </ul>
                          <address>
                              122 Rue des Rosiers <br />
                              93400 Saint-Ouen <br />
                              FRANCE
                              <br /><br />
                              T. 01.42.48.52.63 <br />
                              M. 06.68.61.68.21 <br />
                              laboutique75@gmail.com
                          </address>
                      </div>
                      <div class="row">
                          <div class="col-sm-12 main-navigation">
							<?php include(PATH_LOAD . 'menu.php'); ?>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    </div>
    <!-- //Theme header -->
		