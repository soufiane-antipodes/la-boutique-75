<?php 

	global $globals;

	$link = '';
	
if(preg_match('#/en/#', $_SERVER['REQUEST_URI'])){
	$link = str_replace('/en/', '/', $_SERVER['REQUEST_URI']);
	$urlCanonical = PATH_HTTP;
	$otherLanguage = 'fr';
	$urlCurrentCanonical = PATH_HTTP.'en/';
}
else{
	$link = PATH_HTTP.'en/';
	$urlCanonical = PATH_HTTP.'en/';
	$otherLanguage = 'en';
	$urlCurrentCanonical = PATH_HTTP;
}

?>


<!DOCTYPE html>
<html lang="<?=Session :: get('langToUse')?>">
<head>
	<? Trigger::call('onBeforeParseHeader');?>
  <meta charset="utf-8" />
  <title><?=$globals['META_TITLE']?></title>
	<meta name="description" content="<?=$globals['META_DESCRIPTION']?>">
	<meta name="keywords" content="<?=$globals['META_KEYWORDS']?>">
	<meta name="robots" content="all">
	
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  
 	<meta property="og:title" content="<?=$globals['META_TITLE']?>" >
	<meta property="og:description" content="<?=$globals['META_DESCRIPTION']?>" >
	<meta property="og:image" content="<?=PATH_HTTP?>timthumb.php?src=<?=PATH_HTTP_IMG_GLOBAL?>logo-small.jpg&amp;w=200&amp;h=200&amp;zc=2&amp;q=100" >
	<meta property="og:url" content="<?=$urlCurrentCanonical?>" >
	
	<?php /* ?><link rel="canonical" href="<?=$urlCurrentCanonical?>" /><?php */ ?>
	<?php /* ?><link rel="alternate" hreflang="<?=$otherLanguage?>" href="<?=$urlCanonical?>" /><?php */ ?>
  
  <link rel="shortcut icon" href="<?=PATH_HTTP_IMG_FAVICON?>favicon.ico?<?=date('YmdHis', filemtime(PATH_IMG_FAVICON.'favicon.ico'))?>" />
	
	<!-- Stylesheets -->
	<link rel="stylesheet" href="<?=PATH_HTTP_CSS?>globals.css" />
	<link rel="stylesheet" href="<?=PATH_HTTP_CSS?>style.css?<?=date('YmdHis', filemtime(PATH_CSS.'style.css'))?>" />

  <script src="<?=PATH_HTTP_PLUGINS_JQUERY?>jquery-1.7.2.min.js" type="text/javascript"></script>
  
  
  
  <!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
	
  <?php
  	
	  HTML_CSS :: addFile(PATH_HTTP_PLUGINS_ROYALSLIDER . 'royalslider/royalslider.css');
		HTML_CSS :: addFile(PATH_HTTP_PLUGINS_ROYALSLIDER . 'royalslider/skins/default/rs-default.css?'.date('YmdHis'));
		
		HTML_CSS :: addFile(PATH_HTTP_PLUGINS_RESPONSIVEMENU . 'style.css?'.date('YmdHis'));
		
		HTML_Script :: addFile(PATH_HTTP_PLUGINS_RESPONSIVEMENU.'script.js'); 
		HTML_Script :: addFile(PATH_HTTP_PLUGINS_ROYALSLIDER.'royalslider/jquery.royalslider.min.js'); 
		HTML_Script :: addFile(PATH_HTTP_PLUGINS_STICKY.'jquery.sticky.js'); 
		HTML_Script :: addFile(PATH_HTTP_PLUGINS_ONEPAGE.'jquery.scrollTo.js'); 
		HTML_Script :: addFile(PATH_HTTP_PLUGINS_ONEPAGE.'jquery.nav.js'); 
		HTML_Script :: addFile(PATH_HTTP_PLUGINS_PARALLAX.'deploy/parallax.js');
		
		$device = new DetectMobile();
		$isAndroid = $device->DetectAndroid();
		if($isAndroid) Page :: setClassBody('android');
	?>
  
	<? Trigger::call('onAfterParseHeader');?>
</head>
<body class="<?=Page :: getClassBody()?>">
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/fr_FR/all.js#xfbml=1&appId=447198321964840";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>
	
	<div class="topBackground" id="header"></div><!-- .topBackground -->

		<?php /*
		<!-- **************************************
				 ***************** LANGUAGE
				 ********************************** -->
		<div class="wrapLanguage" style="">
			<div class="wrapper">
				<div class="language <?=(Session :: get('langToUse') == 'fr') ? 'langFR' : 'langEN'?>">
					<ul class="inLine">
						<?=(Session :: get('langToUse') == 'en') ? '<li class="current">EN</li>' : '<li><a href="'.$link.'">EN</a></li>'?>
						<li>|</li>
						<?=(Session :: get('langToUse') == 'fr') ? '<li class="current">FR</li>' : '<li><a href="'.$link.'">FR</a></li>'?>
					</ul>
					<div class="clear"></div><!-- .clear -->
				</div><!-- .language -->
			</div><!-- .wrapper -->
		</div><!-- .wrapLanguage -->
		
		*/ ?>
		
		<div class="wrapLogo">
			<div class="wrapper">
				<div class="logo">
					<a href="<?=PATH_HTTP?>" class="logo-bigger"><img src="<?=PATH_HTTP_IMG_GLOBAL?>logo.png" alt="Logo Yann Mercoeur" /></a>
					<a href="<?=PATH_HTTP?>" class="logo-small"><img src="<?=PATH_HTTP_IMG_GLOBAL?>logo-small.jpg" alt="Logo Yann Mercoeur" /></a>
				</div><!-- .logo -->
			</div><!-- .wrapper -->
		</div><!-- .wrapLogo -->
		
			
		<!-- **************************************
				 ***************** MENU
				 ********************************** -->
			<?php include(PATH_LOAD . 'menu.php'); ?>