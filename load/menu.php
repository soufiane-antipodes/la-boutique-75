<?php
	global $globals;	

	//require(PATH_TRAITEMENTS . 'menu.php');

	//$sNomPage = str_replace('_', '-', Sanitize :: keepValidChars(Request :: getField('nom')) );	


?>
<ul class="nav navbar-nav navbar-right">
    <li class="first<?=(Page :: getIndexMenu() == 1) ? '  on' : null;?>"><a href="<?=getActualLanguage()?>/"><?=$globals['MENU_HOME']?></a></li>
    <li<?=(Page :: getIndexMenu() == 2) ? ' class="on"' : null;?>><a href="<?=getActualLanguage()?>/nouveautes"><?=$globals['MENU_NEW']?></a></li>
    <li<?=(Page :: getIndexMenu() == 3) ? ' class="on"' : null;?>><a href="<?=getActualLanguage()?>/collection"><?=$globals['MENU_COLLECTION']?></a></li>
    <!--
    <li>
    <a href="#" id="navbarDrop1" class="dropdown-toggle" data-toggle="dropdown">Products</a>
    <ul class="dropdown-menu" role="menu" aria-labelledby="navbarDrop1">
        <li><a href="products-list.php">Chairs</a></li>
        <li><a href="products-list.php">Bedroom Storage</a></li>
        <li><a href="products-list.php">Beds</a></li>
        <li>
            <a href="products-list.php">Decorations</a>
            <ul>
                <li><a href="products-list.php">Outdoor</a></li>
                <li>
                    <a href="products-list.php">Indoor</a>
                    <ul>
                        <li><a href="products-list.php">Bedroom Decorations</a></li>
                        <li><a href="products-list.php">Kitchen Decorations</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <li><a href="products-list.php">Outdoor Furniture</a></li>
    </ul>
    </li>
    -->
    <li<?=(Page :: getIndexMenu() == 4) ? ' class="on"' : null;?>><a href="<?=getActualLanguage()?>/a-propos"><?=$globals['MENU_ABOUT']?></a></li>
    <li<?=(Page :: getIndexMenu() == 5) ? ' class="on"' : null;?>><a href="<?=getActualLanguage()?>/contact"><?=$globals['MENU_CONTACT']?></a></li>
</ul>