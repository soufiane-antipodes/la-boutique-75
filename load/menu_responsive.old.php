<?php
	global $globals;	

	require(PATH_TRAITEMENTS . 'menu_responsive.php');

	$sNomPage = str_replace('_', '-', Sanitize :: keepValidChars(Request :: getField('nom')) );	
?>



<nav>
	<div class="logo">DVAI - Découpe, Fonds &amp; produits aluminium inox</div>
	<ul class="inline menuLeft">
		<li class="first element <?=(Page :: getIndexMenu() == 1) ? 'current' : null;?>"><a href="<?= getActualLanguage() . '/' ?>" >Accueil</a></li>
		<? foreach ($aMenus as $iKey => $sValue) : ?>
			<? if ( count($sValue) > 1 ) : ?>
				<li class="element">
					<a href="#" class="sb-toggle-submenu"><?= $sValue[0]['name'] ?><span class="sb-caret"></span></a>
					<ul class="sb-submenu sb-submenu-active">
						<? foreach ($sValue as $iSubKey => $sSubValue) : if ( $iSubKey === 0 ) continue; ?>
							<? 
								$sUrl = $sSubValue['id'] . '-' . Sanitize :: keepValidCharsRewriting($sSubValue['name']); 
								$sUrl = str_replace('_', '-', $sUrl);
							?>
							<li class="element <?= ( $sUrl === $sNomPage ) ? 'current' : null ?>"><a href="<?= getActualLanguage() . '/page/' . $sUrl ?>"><?= $sSubValue['name'] ?></a></li>
						<? endforeach ?>
					</ul>
				</li>
			<? else : ?>
				<? $sUrl = $sValue[0]['id'] . '-' . Sanitize :: keepValidCharsRewriting($sValue[0]['name']); ?>
				<li class="element <?= ( $sUrl === $sNomPage ) ? 'current' : null ?>"><a href="<?= getActualLanguage() . '/page/' . $sValue[0]['id'] . '-' . Sanitize :: keepValidCharsRewriting($sValue[0]['name']) ?>"><?= $sValue[0]['name'] ?></a></li>
			<? endif ?>
		<? endforeach ?>
	</ul>
	<ul class="inline menuMiddle">
		<li class="element"><a href="<?= getActualLanguage() . '/devis' ?>" class="box-sizing block">Demande de devis</a></li>
	</ul>
	<ul class="inline menuRight">
		<li class="element <?= ( Page :: getIndexMenu() == 50 ) ? 'current' : null; ?>"><a href="<?= getActualLanguage() . '/blog' ?>">Blog</a></li>
		<li class="last element <?= ( Page :: getIndexMenu() == 60 ) ? 'current' : null; ?>"><a href="<?= getActualLanguage() . '/contact' ?> ">Contact</a></li>
	</ul>
</nav>
