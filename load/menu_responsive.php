<?php
	global $globals;	

	require(PATH_TRAITEMENTS . 'menu_responsive.php');

	$sNomPage = str_replace('_', '-', Sanitize :: keepValidChars(Request :: getField('nom')) );	

?>



<nav>
	<div class="logo">DVAI - Découpe, Fonds &amp; produits aluminium inox</div>
	<ul class="menuLeft">
		<li class="first element <?=(Page :: getIndexMenu() == 1) ? 'current' : null;?>"><a href="<?= getActualLanguage() . '/' ?>" ><?=$globals['MENU_HOME']?></a></li>
		<? foreach ($aMenus as $iKey => $sValue) : ?>
			<? if ( count($sValue) > 1 ) : ?>
				<li class="element element-parent element-parent-<?= $sValue[0]['id'] ?>"><a href="#" class="sb-toggle-submenu"><?= $sValue[0]['name'] ?></a></li>
					<? foreach ($sValue as $iSubKey => $sSubValue) : if ( $iSubKey === 0 ) continue; ?>
						<li class="element element-child element-child-<?= $sValue[0]['id'] ?> <?= ( buildUpURL($sSubValue['id'], $sSubValue['name']) === $sNomPage ) ? 'current' : null ?>">
							<a href="<?= getActualLanguage() . buildUpURL($sSubValue['id'], $sSubValue['name'], 'page') ?>"><?= $sSubValue['name'] ?></a>
						</li>
					<? endforeach ?>
			<? else : ?>
				<li class="element <?= ( buildUpURL($sValue[0]['id'], $sValue[0]['name']) === $sNomPage ) ? 'current' : null ?>">
					<a href="<?= getActualLanguage() . buildUpURL($sValue[0]['id'], $sValue[0]['name'], 'page') ?>"><?= $sValue[0]['name'] ?></a>
				</li>
			<? endif ?>
		<? endforeach ?>
	</ul>
	<ul class="menuMiddle">
		<li class="element"><a href="<?= getActualLanguage() . '/devis' ?>" class="box-sizing block"><?=$globals['MENU_DEVIS']?></a></li>
	</ul>
	<ul class="menuRight">
		<li class="element <?=( Page :: getIndexMenu() == 50 ) ? 'current' : null;?>"><a href="<?= getActualLanguage() . '/blog' ?>"><?=$globals['MENU_BLOG']?></a></li>
		<li class="last element <?=( Page :: getIndexMenu() == 60 ) ? 'current' : null;?>"><a href="<?= getActualLanguage() . '/contact' ?>"><?=$globals['MENU_CONTACT']?></a></li>
	</ul>
</nav>
