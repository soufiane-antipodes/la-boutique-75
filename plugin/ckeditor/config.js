/**
 * @license Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.stylesSet.add( 'my_styles', [
    // Inline styles
    { name: 'Fond rouge', element: 'div', styles: { 'background-color': '#D5655D', 'padding' : '10px', 'color' : '#fff' } },
    { name: 'Titre rouge', element: 'h2', styles: { 'color': '#D5655D' } }
] );

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	
	config.toolbar = 'FullOption';
 
	config.toolbar_FullOption =
	[
		{ name: 'document', items : ['Bold','Italic','Underline','Subscript','Superscript','TextColor','-','PasteFromWord','-','Undo','Redo','-','RemoveFormat', 'Link','Unlink', 'Image', 'Format', '-', 'Styles', 'Source'] },
	];
	
	config.toolbar_BasicOption =
	[
		{ name: 'document', items : ['Bold','Italic','Underline','Subscript','Superscript','TextColor','-', 'PasteFromWord', 'Source' ] },
	];
	
	config.stylesSet = 'my_styles';
	
	// Brazil colors only.
	config.colorButton_colors = '000000,C60159,8F8F8F';
	
	// Not recommended.
	config.enterMode = CKEDITOR.ENTER_BR;
	config.forcePasteAsPlainText = true;
	
	config.filebrowserBrowseUrl = '/plugin/kcfinder/browse.php?type=files';
	config.filebrowserImageBrowseUrl = '/plugin/kcfinder/browse.php?type=images';
	config.filebrowserFlashBrowseUrl = '/plugin/kcfinder/browse.php?type=flash';
	config.filebrowserUploadUrl = '/plugin/kcfinder/upload.php?type=files';
	config.filebrowserImageUploadUrl = '/plugin/kcfinder/upload.php?type=images';
	config.filebrowserFlashUploadUrl = '/plugin/kcfinder/upload.php?type=flash';
};
