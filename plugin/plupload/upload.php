<?php
/**
 * upload.php
 *
 * Copyright 2009, Moxiecode Systems AB
 * Released under GPL License.
 *
 * License: http://www.plupload.com/license
 * Contributing: http://www.plupload.com/contributing
 */

require_once('../../.loader.php');

$information = array('id' => 'id_artist_picture', 'table' => 'artists_picture');

$fields = array();
$fields['lang'] = array('name' => 'Langue','value' => Session :: get('langAdminToUse'), 'type' => 'string', 'isMandatory' => true);
$fields['id_artist'] = array('name' => 'Valeur','value' => Request :: getInt('idParent'), 'type' => 'string', 'isMandatory' => true);

$file = $_FILES['file'];
$file['isMandatory'] = true;
$file['dest'] = PATH_IMG_ARTISTS;
$file['extension'] = array('.jpg', '.JPG', '.png');

$thumbnails = array(
										'big' => array('width' => 480, 'height' => null, 'source' => PATH_IMG_ARTISTS, 'dest' => PATH_IMG_ARTISTS . 'zoom/', 'maxHeight' => 480),
										'normal' => array('width' => 140, 'height' => 140, 'source' => PATH_IMG_ARTISTS . 'zoom/', 'dest' => PATH_IMG_ARTISTS)
										);

$position = array('position' => 'end', 'condition' => 'lang="'.Session :: get('langAdminToUse').'"');

$result = Manage :: add($information['table'], $file, $fields, $thumbnails, $position, $error);
if($result == true) $success = 'Image ajout�e';
?>

