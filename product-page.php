<?php
require_once('.loader.php');

Database :: verifyConnexion();

$error = null;
$success = null;

//Page :: setIndexMenu(1);
Page :: setTitle('Nom du produit | ' . TITLE_BY_DEFAULT);
Page :: setMetaDescription($globals['META_DESC_DEFAULT']);

Trigger :: call('onStartHTML');
?>
<section class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="product-description">
                   <div class="product-slider">
                        <ul class="slides">
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product01.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product02.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product03.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product04.jpg" />
                                </figure>
                            </li>
                        </ul>
                    </div>
                   <div class="name">Svelik</div>
                   <div class="description">Queen and King beds</div>
                   <div class="info-block">
                       <h4>Combination</h4>
                       <div class="info">
                           Frame: 11-454<br/>
                           Seat: 16-280<br/>
                           Bed dimensions: 200x190<br/>
                       </div>
                   </div>
                    <div class="info-block">
                        <h4>Colors</h4>
                        <div class="info">
                            <ul>
                                <li>
                                    <span class="inner" style="background: #9b8d7c"/>
                                </li>
                                <li>
                                    <span class="inner" style="background: #993333"/>
                                </li>
                                <li>
                                    <span class="inner" style="background: #336666"/>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="price-wrapper">
                        <div class="price">
                            2500,00$
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-9">
                <div class="product-presentation-tabs">
                    <div class="tab-content">
                        <div class="tab-pane active text-editor" id="product-info">
                            <p>
                                Mauris aliquam a ipsum id lacinia. Suspendisse neque ipsum, condimentum id lacinia non, luctus in dui. Praesent eu aliquam massa. Mauris blandit euismod lorem a suscipit. Phasellus a mauris diam. In posuere ipsum at tellus luctus pretium. 
                            </p>
                            <ul>
                                <li>Vestibulum ante ipsum primis in faucibus</li>
                                <li>Nunc a feugiat neque, porta adipiscing urna</li>
                                <li>Integer ut congue nunc, quis imperdiet risus</li>
                                <li>Etiam magna neque, rhoncus ac purus et</li>
                                <li>Cum sociis natoque penatibus et magnis dis</li>
                                <li>Aliquam dignissim felis leo, quis condimentum</li>
                            </ul>
                        </div>
                        <div class="tab-pane text-editor" id="product-details">
                            <p>
                                Vivamus suscipit, quam eu tempor hendrerit, metus nisl porttitor ante, sit amet congue nisl ipsum quis urna. Duis non nisl vitae risus fermentum malesuada. Ut tincidunt sagittis nulla in blandit.
                            </p>
                            <ul>
                                <li>Pellentesque viverra ligula ullamcorper, eleifend</li>
                                <li>Nam vitae augue ante. Aenean tellus purus</li>
                                <li>Vestibulum laoreet arcu turpis, vel condimentum sapien</li>
                                <li>Nullam semper tempus odio, vel vehicula nunc. </li>
                                <li>Maecenas vitae arcu nec dui rhoncus varius.</li>
                                <li>Nunc posuere, dolor ut porttitor imperdiet</li>
                            </ul>
                        </div>
                        <div class="tab-pane text-editor" id="delivery">
                            <p>
                                Suspendisse eleifend sollicitudin ligula, et vehicula diam faucibus ac. Fusce risus enim, convallis vel vulputate adipiscing, vehicula sit amet augue. Duis pretium lacus eu libero pretium pulvinar.
                            </p>
                            <ul>
                                <li>Suspendisse neque ipsum, condimentum id lacinia non, luctus in dui.</li>
                                <li>Maecenas et dolor non lorem condimentum rutrum</li>
                                <li>Cum sociis natoque penatibus et magnis dis parturient montes</li>
                                <li>Nulla nibh est, gravida a elit non, bibendum sodales neque</li>
                                <li>Donec volutpat posuere orci ac tincidunt</li>
                                <li>Sed at pulvinar nisi, vel sollicitudin quam</li>
                            </ul>
                        </div>
                        <div class="tab-pane text-editor" id="rate">
                            <p>
                                Proin pretium purus nec eros consectetur tempor. Maecenas congue libero vehicula est iaculis, eget molestie ante bibendum. Cras eu pellentesque purus, at condimentum nisi. 
                            </p>
                            <ul>
                                <li>Vestibulum vel diam ac justo fermentum placerat.</li>
                                <li>Nunc a feugiat neque, porta adipiscing urna</li>
                                <li>Etiam magna neque, rhoncus ac purus et</li>
                                <li>Phasellus vestibulum hendrerit lectus nec malesuada</li>
                                <li>Cum sociis natoque penatibus et magnis dis parturient</li>
                                <li>Maecenas vitae arcu nec dui rhoncus varius</li>
                            </ul>
                        </div>
                    </div>
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#product-info" data-toggle="tab">Product Information</a></li>
                        <li><a href="#product-details" data-toggle="tab">Product Details</a></li>
                        <li><a href="#delivery" data-toggle="tab">Delivery and returns</a></li>
                        <li><a href="#rate" data-toggle="tab">Rate and share</a></li>
                    </ul>
                </div>
            </div>
            <aside class="col-sm-3">
                <div class="widget sketch">
                    <figure>
                        <img src="<?=PATH_HTTP_IMG_THEME?>sketch.png" alt=""/>
                    </figure>
                </div>
            </aside>
            <div class="col-sm-9">
                <div class="feedback">
                    <h3>Feedback</h3>
                    <ul>
                        <li class="comment">
                            <div class="likes-wrapper">
                                <div class="like"></div>
                            </div>
                            <figure>
                                <a href="#">
                                    <img src="<?=PATH_HTTP_IMG_THEME?>face1.jpg" alt=""/>
                                </a>
                            </figure>
                            <div class="user">
                                <a href="#">Jean Willes</a>
                                says:
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean suscpit metus quis leo cod sit amet viverra turpis tincidunt.
                            </div>
                            <div class="date">September 20th, 2013</div>
                        </li>
                        <li class="comment">
                            <div class="likes-wrapper">
                                <div class="like"></div>
                            </div>
                            <figure>
                                <a href="#">
                                    <img src="<?=PATH_HTTP_IMG_THEME?>face2.jpg" alt=""/>
                                </a>
                            </figure>
                            <div class="user">
                                <a href="#">Madeleine Sherwood</a>
                                says:
                            </div>
                            <div class="content">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sed nunc sit
                                amet nunc auctor viverra non quis libero. Vestibulum non ligulari. Nunc quis nisi dapibus,
                                facilisis purus varius, placerat eros. Nulla sit amet convallis libero. Integer posuere egestas
                                aliquam. Donec justo felis, scelerisque sed pulvinar vitae, posuere elementum lacus.
                                Nullam sodales tincidunt diam vitae tincidunt. Cras ut tortor nullaonec mauris
                            </div>
                            <div class="date">September 20th, 2013</div>
                        </li>
                        <li class="comment">
                            <div class="likes-wrapper">
                                <div class="dislike"></div>
                            </div>
                            <figure>
                                <a href="#">
                                    <img src="<?=PATH_HTTP_IMG_THEME?>face3.jpg" alt=""/>
                                </a>
                            </figure>
                            <div class="user">
                                <a href="#">Ronnie Spector</a>
                                says:
                            </div>
                            <div class="content">
                                Vestibulum non ligulari. Nunc quis nisi dapibus, facilisis purus variu,
                                placerat eros. Nulla sit amet convallis liberonteger posuere egestas
                            </div>
                            <div class="date">September 20th, 2013</div>
                        </li>
                        <li class="comment">
                            <div class="likes-wrapper">
                                <div class="like"></div>
                            </div>
                            <figure>
                                <a href="#">
                                    <img src="<?=PATH_HTTP_IMG_THEME?>face4.jpg" alt=""/>
                                </a>
                            </figure>
                            <div class="user">
                                <a href="#">Stella Stevens</a>
                                says:
                            </div>
                            <div class="content">
                                Nam in varius ligula, id elementum dolor. Nullam tempor est est,
                                sit amet tristique justo lacinia sed. Mauris faucibus, magna et
                                aliquam convallis, nibh lectus convallis felis, a feugiat massa nunc ut metus.
                                Morbi eleifend suscipit leo id iaculis.
                            </div>
                            <div class="date">September 20th, 2013</div>
                        </li>
                    </ul>
                </div>
            </div>
            <aside class="col-sm-3">
                <div class="widget leave-feedback">
                    <h3>Leave feedback</h3>
                    <form role="form">
                        <textarea name="feedback" class="form-control"></textarea>
                        <input id="likes" type="hidden" name="likes" />
                        <a href="#" class="like"></a>
                        <a href="#" class="dislike"></a>
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                </div>
                <div class="widget small-text">
                    apply for designer’s
                    Professional Review
                </div>
                <div class="widget big-text">
                    Get
                    furniture
                    that your
                    wife will
                    enjoy
                </div>
            </aside>
            <div class="col-sm-12">
                <div class="recommended-products grid-products-wrapper">
                    <h3>Recommended</h3>
                    <div class="row">
                            <div class="col-sm-3">
                                <div class="grid-product first">
                                    <figure>
                                        <a href="">
                                            <img src="<?=PATH_HTTP_IMG_THEME?>single-product01.jpg" alt=""/>
                                        </a>
                                    </figure>
                                    <div class="overlay">
                                        <div class="price">105,00$</div>
                                        <div class="background"></div>
                                    </div>
                                    <a href="product-page.html" class="view-product">
                                        <div class="text">View product</div>
                                        <div class="background"></div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="grid-product">
                                    <figure>
                                        <a href="">
                                            <img src="<?=PATH_HTTP_IMG_THEME?>single-product02.jpg" alt=""/>
                                        </a>
                                    </figure>
                                    <div class="overlay">
                                        <div class="price">705,00$</div>
                                        <div class="background"></div>
                                    </div>
                                    <a href="product-page.html" class="view-product">
                                        <div class="text">View product</div>
                                        <div class="background"></div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="grid-product">
                                    <figure>
                                        <a href="">
                                            <img src="<?=PATH_HTTP_IMG_THEME?>single-product03.jpg" alt=""/>
                                        </a>
                                    </figure>
                                    <div class="overlay">
                                        <div class="price">2450,00$</div>
                                        <div class="background"></div>
                                    </div>
                                    <a href="product-page.html" class="view-product">
                                        <div class="text">View product</div>
                                        <div class="background"></div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="grid-product">
                                    <figure>
                                        <a href="">
                                            <img src="<?=PATH_HTTP_IMG_THEME?>single-product04.jpg" alt=""/>
                                        </a>
                                    </figure>
                                    <div class="overlay">
                                        <div class="price">515,00$</div>
                                        <div class="background"></div>
                                    </div>
                                    <a href="product-page.html" class="view-product">
                                        <div class="text">View product</div>
                                        <div class="background"></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</section>
    