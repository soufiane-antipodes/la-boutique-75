<?php
require_once('.loader.php');

Database :: verifyConnexion();

$error = null;
$success = null;

Page :: setIndexMenu(3);
Page :: setTitle($globals['MENU_COLLECTION'] . ' | ' . TITLE_BY_DEFAULT);

Trigger :: call('onStartHTML');
?>
<section class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="products-header">
                    <div class="category-menu">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab1" data-toggle="tab"><span class="inner"><img src="img/tab-img01.png" alt=""/></span></a></li>
                        <li><a href="#tab2" data-toggle="tab"><span class="inner"><img src="img/tab-img02.png" alt=""/></span></a></li>
                        <li><a href="#tab3" data-toggle="tab"><span class="inner"><img src="img/tab-img03.png" alt=""/></span></a></li>
                        <li><a href="#tab4" data-toggle="tab"><span class="inner"><img src="img/tab-img04.png" alt=""/></span></a></li>
                        <li><a href="#tab5" data-toggle="tab"><span class="inner"><img src="img/tab-img05.png" alt=""/></span></a></li>
                        <li>
                        	<a href="#tab6" data-toggle="tab">
                            	<span class="category-label">Moderne &amp; Design</span>
                            	<span class="inner">
                        			<div style="width: 100%; margin: 0 auto;">
                                        <object>
                                            <embed src="<?=PATH_HTTP_SVG?>cat-modndesign.svg" style="width: 100%; max-width: 40px; height: auto;" />
                                        </object>
                                    </div>
                                </span>
                            </a>
                        </li>
                    </ul>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="grid-products-wrapper">
                    <div class="row">
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product01.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">515,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product02.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">2500,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product03.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">150,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product04.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">2035,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product05.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">515,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product06.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">3255,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product07.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">400,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product08.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">5400,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product09.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">515,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product10.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">1200,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product11.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">104000,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product12.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">3500,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product01.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">516005,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product02.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">300,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product03.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">515,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product04.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">515,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product05.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">515,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product06.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">515,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>

                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product07.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">515,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                        <div class="col-sm-3 col-xs-6">
                            <div class="grid-product">
                                <figure>
                                    <a href="product-page.html">
                                        <img src="<?=PATH_HTTP_IMG_THEME?>home_product08.jpg" alt=""/>
                                    </a>
                                </figure>
                                <div class="overlay">
                                    <div class="price">515,00$</div>
                                    <div class="background"></div>
                                </div>
                                <a href="product-page.html" class="view-product">
                                    <div class="text">View product</div>
                                    <div class="background"></div>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 pagination-wrapper">
                        <div class="pagination-inner">
                            <span class="title">Page:</span>
                            <ul class="pagination">
                                <li><a href="#" class="active">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><span class="points">...</span></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
    