<?php
require_once('.loader.php');

Database :: verifyConnexion();

$error = null;
$success = null;

Page :: setIndexMenu(2);
Page :: setTitle($globals['MENU_NEW'] .' | '. TITLE_BY_DEFAULT);

Trigger :: call('onStartHTML');
?>
<section class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="products-header">
                    <div class="category-menu">
                        <span>153 items from</span>
                        <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                All
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="#">Armchair & sofas</a></li>
                                <li><a href="#">Office chairs</a></li>
                                <li><a href="#">Beds</a></li>
                                <li><a href="#">Living room storage</a></li>
                                <li><a href="#">Tv & media furniture</a></li>
                            </ul>
                        </div>
                        <span>categories</span>
                    </div>
                    <div class="view-menu">
                        <a href="#" class="grid-view">
                            <i class="fa fa-th"></i>
                        </a>
                        <a href="#" class="list-view active">
                            <i class="fa fa-list-ul"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-12">
                <article class="product-description">
                    <div class="product-slider">
                        <ul class="slides">
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product01.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product02.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product03.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product04.jpg" />
                                </figure>
                            </li>
                        </ul>
                    </div>
                    <div class="name">Svelik</div>
                    <div class="description">Queen and King beds</div>
                    <div class="info-block">
                        <h4>Combination</h4>
                        <div class="info">
                            Frame: 11-454<br/>
                            Seat: 16-280<br/>
                            Bed dimensions: 200x190<br/>
                        </div>
                    </div>
                    <div class="info-block">
                        <h4>Colors</h4>
                        <div class="info">
                            <ul>
                                <li>
                                    <span class="inner" style="background: #9b8d7c"/>
                                </li>
                                <li>
                                    <span class="inner" style="background: #993333"/>
                                </li>
                                <li>
                                    <span class="inner" style="background: #336666"/>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="price-wrapper">
                        <a href="#">
                            <span class="value">2500,00$</span>
                            <span class="on-hover">View product</span>
                        </a>
                    </div>
                </article>
                <article class="product-description">
                    <div class="product-slider">
                        <ul class="slides">
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product02.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product03.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product04.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product01.jpg" />
                                </figure>
                            </li>
                        </ul>
                    </div>
                    <div class="name">ADRASTEIA</div>
                    <div class="description">Queen and King beds</div>
                    <div class="info-block">
                        <h4>Combination</h4>
                        <div class="info">
                            Frame: 11-454<br/>
                            Seat: 16-280<br/>
                            Bed dimensions: 200x190<br/>
                        </div>
                    </div>
                    <div class="info-block">
                        <h4>Colors</h4>
                        <div class="info">
                            <ul>
                                <li>
                                    <span class="inner" style="background: #7a4930"/>
                                </li>
                                <li>
                                    <span class="inner" style="background: #909090"/>
                                </li>
                                <li>
                            </ul>
                        </div>
                    </div>
                    <div class="price-wrapper">
                        <a href="#">
                            <span class="value">3750,00$</span>
                            <span class="on-hover">View product</span>
                        </a>
                    </div>
                </article>
                <article class="product-description">
                    <div class="product-slider">
                        <ul class="slides">
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product03.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product01.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product04.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product02.jpg" />
                                </figure>
                            </li>
                        </ul>
                    </div>
                    <div class="name">Svelik</div>
                    <div class="description">Queen and King beds</div>
                    <div class="info-block">
                        <h4>Combination</h4>
                        <div class="info">
                            Frame: 11-454<br/>
                            Seat: 16-280<br/>
                            Bed dimensions: 200x190<br/>
                        </div>
                    </div>
                    <div class="info-block">
                        <h4>Colors</h4>
                        <div class="info">
                            <ul>
                                <li>
                                    <span class="inner" style="background: #9b8d7c"/>
                                </li>
                                <li>
                                    <span class="inner" style="background: #993333"/>
                                </li>
                                <li>
                                    <span class="inner" style="background: #336666"/>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="price-wrapper">
                        <a href="#">
                            <span class="value">2500,00$</span>
                            <span class="on-hover">View product</span>
                        </a>
                    </div>
                </article>
                <article class="product-description">
                    <div class="product-slider">
                        <ul class="slides">
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product04.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product02.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product01.jpg" />
                                </figure>
                            </li>
                            <li>
                                <figure>
                                    <img src="<?=PATH_HTTP_IMG_THEME?>single-product03.jpg" />
                                </figure>
                            </li>
                        </ul>
                    </div>
                    <div class="name">ADRASTEIA</div>
                    <div class="description">Queen and King beds</div>
                    <div class="info-block">
                        <h4>Combination</h4>
                        <div class="info">
                            Frame: 11-454<br/>
                            Seat: 16-280<br/>
                            Bed dimensions: 200x190<br/>
                        </div>
                    </div>
                    <div class="info-block">
                        <h4>Colors</h4>
                        <div class="info">
                            <ul>
                                <li>
                                    <span class="inner" style="background: #9b8d7c"/>
                                </li>
                                <li>
                                    <span class="inner" style="background: #993333"/>
                                </li>
                                <li>
                                    <span class="inner" style="background: #336666"/>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="price-wrapper">
                        <a href="#">
                            <span class="value">3750,00$</span>
                            <span class="on-hover">View product</span>
                        </a>
                    </div>
                </article>
            </div>
            <div class="col-sm-12 pagination-wrapper">
                <div class="pagination-inner">
                    <span class="title">Page:</span>
                    <ul class="pagination">
                        <li><a href="#" class="active">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><span class="points">...</span></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
    