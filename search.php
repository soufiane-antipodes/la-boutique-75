<?php
require_once('.loader.php');

require(PATH_TRAITEMENTS . 'recherche.php');

Page :: setTitle($globals['SEARCH_TITLE'].' | DVAI');
Page :: setMetaDescription($globals['META_DESC_SEARCH']);
 Page :: setClassBody('double');
Trigger :: call('onStartHTML');

?>
<div class="actualpage full-float">
	<div class="centered titleBox">
		<div class="leftAside">&nbsp;</div>
		<div class="rightAside">
			<h1><b><?=$globals['SEARCH_TITLE']?></b></h1>
		</div>
	</div>
</div>

<section class="pageinterne recherche">
	<div class="centered">
		<?php include(PATH_LOAD . 'aside.php'); ?>
		<div class="content box-sizing">
			<h2><?= ( $iCount < 1 ) ? $globals['SEARCH_NO'] : (( $iCount == 1 ) ? '1 '.$globals['SEARCH_ONE'] : $iCount . ' '.$globals['SEARCH_MORE']) ?></h2>
			<? if ( count($aRecherche['pages']) !== 0 || count($aRecherche['blog']) !== 0 ) : ?>
				<ul>
					<? foreach ($aRecherche['pages'] as $iKey => $sValue) : ?>
						<li><a href="<?= getActualLanguage() . buildUpURL($sValue['id'], $sValue['name'], 'page') ?>"><?= $sValue['name'] ?></a></li>
					<? endforeach ?>
					<? foreach ($aRecherche['blog'] as $iKey => $sValue) : ?>
						<li><a href="<?= getActualLanguage() . '/blog/' . $sValue['id_article'].'-'.Sanitize :: keepValidCharsRewriting($sValue['title']) ?>"><?= $sValue['title'] ?></a></li>
					<? endforeach ?>
				</ul>
			<? endif ?>
			<?= htmlspecialchars_decode($aPage['content']); ?>
		</div>
	</div>
</section>