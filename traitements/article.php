<?php 

	$sLang = mysql_real_escape_string(Session :: get('langToUse'));

	// Recuperation des variables GET/POST
	$iTypeArticle 	= Sanitize :: keepValidChars(Request :: getField('type'));
	$iID 			= Sanitize :: keepValidChars(Request :: getInt('id'));
	
	/*
		Traitement d'un article quelconque
	*/
	$aActualActualite = array();
	$aPrevID = array();
	$aNextID = array();
	if ($iTypeArticle === "blog")
	{
		// Fetch de l'article concerné avec le suivant et le précédant
		$sQuery = 'SELECT * FROM '.BDD.'actualites S INNER JOIN '.BDD.'item I ON I.id_item = S.id_item_zoom WHERE S.lang="'. $sLang . '" AND publish = 1 AND id = ' . $iID;
		$aOptions = array('type' => 'getOrderedTable', 'orderArrayBy' => 'id');
		$aActualActualite = fetchElementByLang($sQuery, $aOptions);
		
		$position = Database :: get('SELECT position FROM '.BDD.'actualites S INNER JOIN '.BDD.'item I ON I.id_item = S.id_item WHERE S.lang="'. $sLang . '" AND publish = 1 AND id = ' . $iID);
		
		if($position > 0){
			$sQuery = 'SELECT id, id_article, title FROM '.BDD.'actualites S INNER JOIN '.BDD.'item I ON I.id_item = S.id_item WHERE S.lang="'. $sLang . '" AND publish = 1 AND position < ' . $position . ' ORDER BY position DESC LIMIT 0,1';
			$aOptions = array('type' => 'getTable');
			$aPrevID = fetchElementByLang($sQuery, $aOptions);
			
			$sQuery = 'SELECT id, id_article, title FROM '.BDD.'actualites S INNER JOIN '.BDD.'item I ON I.id_item = S.id_item WHERE S.lang="'. $sLang . '" AND publish = 1 AND position > ' . $position . ' ORDER BY position LIMIT 0,1';
			$aOptions = array('type' => 'getTable');
			$aNextID = fetchElementByLang($sQuery, $aOptions);
		}
		
	}

	// Si aucune actualité n'est retournée, ou si l'actualité en question n'existe pas, on retourne à la page précédante
	/*if ( count($aActualActualite) === 0 ) 
	{
		header('Location: /blog');
	}*/

 ?>