<?php 

	$sLang = mysql_real_escape_string(Session :: get('langToUse'));
	
	//Création de la pagination
	$page = 1;
	$limit = 3;
	
	if(Request :: hasRequest('GET') && Request :: getField('page') != null){
		$page = Request :: getInt('page');
	}
	
	$nbActualites = Database :: countOf(BDD.'actualites', 'lang = "' . $sLang . '" AND publish = 1');
	$nbPages = ceil($nbActualites / $limit);
	
	$offset = ($page - 1) * $limit;

	$aActualites = array();
	$aOptions = array('type' => 'getOrderedTable', 'orderArrayBy' => 'id_article');

	if ( $page > 1 )
	{
		$sQuery = 'SELECT * FROM '.BDD.'actualites S INNER JOIN '.BDD.'item I ON I.id_item = S.id_item WHERE S.lang = "' . $sLang . '" AND publish = 1 ORDER BY position LIMIT '.$offset.','.$limit;
		$aActualites = fetchElementByLang($sQuery, $aOptions);
	}
	else
	{
		$sQuery = 'SELECT * FROM '.BDD.'actualites S INNER JOIN '.BDD.'item I ON I.id_item = S.id_item WHERE S.lang = "' . $sLang . '" AND publish = 1 ORDER BY position LIMIT 0,3';
		$aActualites = fetchElementByLang($sQuery, $aOptions);
	}
	

?>