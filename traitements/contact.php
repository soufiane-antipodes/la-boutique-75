<?php 
	
	$error = null;
	$success = null;
	
	if ( Request :: hasRequest('POST') )
	{
		// A traiter pour l'envoi d'emails
		$field = Request :: getData();
		
		if(empty($field['societe']) || empty($field['nom']) || empty($field['code_postal']) || empty($field['ville']) || empty($field['telephone']) || empty($field['email']) || empty($field['demande']))
			$error .= $globals['CON_ERROR'].'<br />';
		
		if($error === null)
		{
			$result = Validation :: email($field['email']);
			if(!$result) $error .= $globals['CON_ERR_MAIL'].'<br />';
		}
		
		if($error === null)
		{
			$html = '
						<div style="text-align : center;margin : 0 0 20px 0;"><img src="'.PATH_HTTP_IMG_GLOBAL.'logo_principal.png" alt="Logo DVAI" /></div>
							Bonjour, <br /><br />
						
						Une personne essaie de prendre contact avec vous... Répondez lui rapidement.<br />
						---------------------------------------------------------<br /><br />
						
						Coordonnées : <br /><br />
						

						Nom : '.$field['nom'].'<br />
						Societe : '.$field['societe'].'<br />
						Adresse : '.$field['adresse'].'<br />
						Code Postal : '.$field['code_postal'].'<br />
						Ville : '.$field['ville'].'<br />
						Telephone : '.$field['telephone'].'<br />
						Email : '.$field['email'].'<br />
						<br />
						Message : '.$field['demande'].'<br />
						
						--<br />
						Cordialement.<br />
						  L\'équipe DVAI.
						';
			
			$email = new Email();
			$result = $email	-> setTo(EMAIL_ADMIN.EMAIL_TEST)
												-> setFrom(EMAIL_FROM)
												-> setBody($html)
												-> setSubject('[DVAI] - Contact Site internet')
												-> send();

			if($result)
			{
				$success .= $globals['CON_OK'];
				Request :: clear();
				unset($field);
				
				HTML_Script :: addScript('
				
					<!-- Google Code for Page Merci Formulaire Conversion Page --> 
					<script type="text/javascript">
						/* <![CDATA[ */
						var google_conversion_id = 968127141;
						var google_conversion_language = "en";
						var google_conversion_format = "3";
						var google_conversion_color = "ffffff";
						var google_conversion_label = "LMniCJu36BIQpeXRzQM"; var google_remarketing_only = false;
						/* ]]> */
					</script>
					
					<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
					
					<noscript>
						<div style="display:inline;">
							<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/968127141/?label=LMniCJu36BIQpeXRzQM&amp;guid=ON&amp;script=0" />
						</div>
					</noscript>
				
				', false, true);
				
			}
		}

	}

?>