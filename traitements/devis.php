<?php 
	
	$error = null;
	$success = null;
	$bError = false;

	
	if ( Request :: hasRequest('POST') )
	{
		// A traiter pour l'envoi d'emails
		$field = Request :: getData();
		
		if(empty($field['pays']) || empty($field['societe']) || empty($field['nom']) || empty($field['code_postal']) || empty($field['ville']) || empty($field['telephone']) || empty($field['email']) || empty($field['demande']))
			$error .= $globals['DDEVIS_ERROR'].'<br />';
		
		if($error === null)
		{
			$result = Validation :: email($field['email']);
			if(!$result) $error .= $globals['CON_ERR_MAIL'].'<br />';
		}
		
		if($_FILES['devisJoint']['size'] > 0){
			
			Picture :: setExtensions(array('.doc', '.DOC', '.DOCX', '.docx', '.pdf', '.PDF', '.jpeg', '.JPEG', '.jpg', '.JPG', '.png', '.PNG', '.gif', '.GIF', '.xlsx', '.xls'));
			$error = null;
			
			$sName = 'devis-' . Sanitize :: keepValidChars($field['societe']) . date('-d-m-Y-H-i-s');
			Picture :: uploadFile($_FILES['devisJoint'], PATH_IMG_UPLOAD . 'devis/', $sName, $error, array());

			$sFile = $_FILES['devisJoint']['name'];
			$sExtension = pathinfo($sFile, PATHINFO_EXTENSION);

			if ( !$bError ) $attachment = PATH_HTTP_IMG_UPLOAD . 'devis/' . $sName . '.' . strtolower($sExtension);
			
		}
		
		if($error === null)
		{
			$html = '
						<div style="text-align : center;margin : 0 0 20px 0;"><img src="'.PATH_HTTP_IMG_GLOBAL.'logo_principal.png" alt="Logo DVAI" /></div>
							Bonjour, <br /><br />
						
						Une personne a fait une demande de devis... Répondez lui rapidement.<br />
						---------------------------------------------------------<br /><br />
						
						Coordonnées : <br /><br />
						

						Nom : '.$field['nom'].'<br />
						Societe : '.$field['societe'].'<br />
						Adresse : '.$field['adresse'].'<br />
						Code Postal : '.$field['code_postal'].'<br />
						Ville : '.$field['ville'].'<br />
						Pays	:	'.$field['pays'].'<br/>
						Telephone : '.$field['telephone'].'<br />
						Email : '.$field['email'].'<br />
						<br/>
						Message : '.$field['demande'].'<br />';
						
						if(isset($attachment) && !empty($attachment)) $html .= 'Fichier : <a href="'.$attachment.'" target="_blank">' . $attachment.'</a><br /><br />	'; 
						
						
	$html .= '
						
						--<br />
						Cordialement.<br />
						  L\'équipe DVAI.
						';
			
			$email = new Email();
			$email	-> setTo(EMAIL_ADMIN.EMAIL_TEST)
							-> setFrom(EMAIL_FROM)
							-> setBody($html);
			
			if($error === null && !$bError)
	    	$bResult = $email-> setSubject('[DVAI] - Demande de devis Site internet')->send();
			
			if($bResult)
			{
				$success .= $globals['DDEVIS_OK'];
				Request :: clear();
				unset($field);
				
				HTML_Script :: addScript('
				
					<!-- Google Code for Page Merci Formulaire Conversion Page --> 
					<script type="text/javascript">
						/* <![CDATA[ */
						var google_conversion_id = 968127141;
						var google_conversion_language = "en";
						var google_conversion_format = "3";
						var google_conversion_color = "ffffff";
						var google_conversion_label = "LMniCJu36BIQpeXRzQM"; var google_remarketing_only = false;
						/* ]]> */
					</script>
					
					<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
					
					<noscript>
						<div style="display:inline;">
							<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/968127141/?label=LMniCJu36BIQpeXRzQM&amp;guid=ON&amp;script=0" />
						</div>
					</noscript>
				
				', false, true);
			}
		}
	}

?>