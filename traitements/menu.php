<?php 

	$sLang = mysql_real_escape_string(Session :: get('langToUse'));

	// Fetch des pages
	$sQuery = 'SELECT id, name, position, idSubPage FROM ' . BDD . 'pages WHERE lang = "' . $sLang . '" AND idSubPage = 0 ORDER BY position ASC';
	$aOptions = array('type' => 'getOrderedTable', 'orderArrayBy' => 'id');
	$aPagesRes = fetchElementByLang($sQuery, $aOptions);

	$sQuery = 'SELECT id, idSubPage, name FROM ' . BDD . 'pages WHERE lang = "' . $sLang . '" AND idSubPage != 0 ORDER BY position ASC';
	$aOptions = array('type' => 'getOrderedTable', 'orderArrayBy' => 'id');
	$aSubPagesRes = fetchElementByLang($sQuery, $aOptions);
	
	$aMenus = mergeArrayMenus($aPagesRes, $aSubPagesRes);
	$aMenus = getMenuFamily($aMenus);
	
	# Flo - défintion de la variable
	$sPageParenteActive = array();

	// Pour trouver la page parente active quand c'est une sous page qui est appelée
	if ( Request :: fieldExists('nom') )
	{
		$aData = Request :: getData();

		$sNomPage = $aData['nom'];

		$_iID =  explode('-', $sNomPage);
		$iID =  (int) $_iID[0];

		// Fetch de la page parente
		$sQuery = 'SELECT name FROM ' . BDD . 'pages WHERE lang = "' . $sLang . '" AND id = (SELECT idSubPage FROM ' . BDD . 'pages WHERE id = "' . $iID . '")';
		
		//$aOptions = array('type' => 'getLine');
		//$aPageParenteActive = fetchElementByLang($sQuery, $aOptions);
		
		$aPageParenteActive = array();
		Database :: getLine($sQuery, $aPageParenteActive);

		$sPageParenteActive = ( isset($aPageParenteActive['name']) ) ? $aPageParenteActive['name'] : 'parent';
	}


?>