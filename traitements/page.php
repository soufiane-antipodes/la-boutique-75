<?php 

	$sLang = mysql_real_escape_string(Session :: get('langToUse'));

	// Recuperation des variables GET/POST
	$sNom = Sanitize :: keepValidChars(Request :: getField('nom'));
	// Capture de l'ID à partir du nom
	$iID = substr($sNom, 0, strpos($sNom, '_'));
	


	// Page courante
	$sQuery = 'SELECT P.*, I.value, I.extension FROM '.BDD.'pages P left join '.BDD.'item I on I.id_item=P.id_item WHERE P.lang = "' . $sLang . '" AND P.id = "' . $iID . '"';
	$aOptions = array('type' => 'getLine');
	$aPage = fetchElementByLang($sQuery, $aOptions);
	

	// Sous pages
	$aSubPages = array();
	if($aPage['idSubPage'] > 0){
		$sQuery = 'SELECT * FROM '.BDD.'pages WHERE lang = "' . $sLang . '" AND idSubPage = "' . $iID . '" ORDER BY position';
		$aOptions = array('type' => 'getTable');
		$aSubPages = fetchElementByLang($sQuery, $aOptions);
	}
	
	// Si aucune page n'est reçue
	if ( count($aPage) === 0 && count($aSubPages) === 0 ) 
	{
		header('Location: /');
	}
	
	if($aPage['idSubPage'] > 0){
		$sQuery = 'SELECT name FROM '.BDD.'pages WHERE lang = "' . $sLang . '" AND id = "' . $aPage['idSubPage'].'"';
		$aOptions = array('type' => 'getLine');
		$aMenuParent = fetchElementByLang($sQuery, $aOptions);
	}
	
	$sMenuParent = ( !isset($aMenuParent['name']) ) ? $aPage['name'] : $aMenuParent['name'];
	

	// Si c'est une sous page qui est demandée, on rappatrie les sous pages liés
	if ( $aPage['idSubPage'] > 0)
	{
		$sQuery = 'SELECT * FROM '.BDD.'pages WHERE lang = "' . $sLang . '" AND idSubPage = ' . $aPage['idSubPage'] . ' ORDER BY position';
		$aOptions = array('type' => 'getTable');
		$aSubPages = fetchElementByLang($sQuery, $aOptions);
		
		// Page parente
		$sQuery = 'SELECT P.*, I.value, I.extension FROM '.BDD.'pages P left join '.BDD.'item I on I.id_item=P.id_item WHERE P.lang = "' . $sLang . '" AND P.id = ' . $aPage['idSubPage'];
		$aOptions = array('type' => 'getLine');
		$aParentPage = fetchElementByLang($sQuery, $aOptions);
		
	}
	
 ?>