<?php
	
	$sLang = mysql_real_escape_string(Session :: get('langToUse'));
	$error = $globals['SAT_ERR'];
	$success = $globals['SAT_OK'];
	$bError = false;
	$bSuccess = false;
	
	Database :: verifyConnexion();
	
	if ( Request :: hasRequest('POST') )
	{
		
		// A traiter pour l'envoi d'emails
		$aData = Request :: getData();
		$sQuery = '';
		foreach ($aData as $iKey => $sValue)
		{
			if ( $iKey === 'lang' ) continue;
			if(!in_array($iKey, array('email', 'societe', 'fonction', 'prenom', 'nom')))
				if ( empty( $_POST[$iKey] )) $bError = true;
		}
		
		if ( !$bError )
		{
			
			//$request = 'INSERT INTO ' . BDD . 'questionnaire_reponses VALUES(NULL, ' . substr($sQuery, 0, -1) . ', NOW())';
			
			$html = '
							<div style="text-align : center;margin : 0 0 20px 0;"><img src="'.PATH_HTTP_IMG_GLOBAL.'logo_principal.png" alt="Logo DVAI" /></div>
							Bonjour, <br /><br />
							
							Une personne vous a répondue à l\'enquête de satisfaction<br />
							---------------------------------------------------------<br /><br />
							
							Informations : <br /><br />
							
							';
							
							foreach ($aData as $iKey => $sValue)
								{
									if ( $iKey === 'lang' ) continue;
									$html .= ucfirst($iKey) . ' : ' . $sValue . '<br />';
								}
							
							$html .='<br />
							
							
							--<br />
							Cordialement.<br />
							  L\'équipe DVAI.
							';
			
			$email = new Email();
			$result = $email	-> setTo(EMAIL_ADMIN.EMAIL_TEST)
												-> setFrom(EMAIL_FROM)
												-> setSubject('[DVAI] - Enquete de satisfaction')
												-> setBody($html)
												-> send();
			
			if($result){
				$bSuccess = true;
			}
		}
	}

	//Retourne tableau ordonné
	$sQuery = 'SELECT s.title as subject, q.title FROM '.BDD.'questionnaire q INNER JOIN '.BDD.'questionnaire_subjects s ON q.idSubject = s.id WHERE s.lang = "' . $sLang . '" and q.lang="'.$sLang.'" ORDER BY s.position, q.position ';
	$aOptions = array('type' => 'getTable');
	
	$aQuestionnaire = fetchElementByLang($sQuery, $aOptions);
	$aQuestionnaire = regroupArrayBy($aQuestionnaire, 'subject', 'title');
	
?>