<?php 
	$sLang = mysql_real_escape_string(Session :: get('langToUse'));
	$iCount = 0;
	if ( Request :: fieldExists('motcle') )
	{
		$sMotCle = Request :: getField('motcle');
		$aMotCle = explode(' ', $sMotCle);
		$aFields = array();
		array_push($aFields, 'content');
		array_push($aFields, 'name');
		$sQuery = '';

		// Boucle sur les mots clés
		foreach ($aMotCle as $_iKey => $_sValue) 
		{
			// Boucle sur les champs de recherche
			foreach ($aFields as $iKey => $sValue) 
			{
				$sQuery .= ' ' . $sValue . ' LIKE "%' . $_sValue . '%" OR';
			}
		}

		// Suppression du dernier OR
		$sQuery = substr($sQuery, 0, -3);
		// Suppression de l'espace en premier
		$sQuery = substr($sQuery, 1);
		

		$aRecherche = array();

		$_sQuery = 'SELECT * FROM '.BDD.'pages WHERE (' . $sQuery . ') AND lang = "' . $sLang .'"';
		$aOptions = array('type' => 'getTable');
		$aRecherche['pages'] = fetchElementByLang($_sQuery, $aOptions);

		$sQuery = str_replace('content', 'description', $sQuery);
		$sQuery = str_replace('name', 'title', $sQuery);

		$_sQuery = 'SELECT * FROM '.BDD.'actualites WHERE publish = 1 AND (' . $sQuery . ') AND lang = "' . $sLang . '"';
		$aOptions = array('type' => 'getTable');
		$aRecherche['blog'] = fetchElementByLang($_sQuery, $aOptions);

		$iCount = ( count($aRecherche['pages']) === 0 && count($aRecherche['blog']) === 0 ) ? 0 : count($aRecherche['pages']) + count($aRecherche['blog']);
	}


?>