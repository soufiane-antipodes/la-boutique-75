<?php
	$sLang = mysql_real_escape_string(Session :: get('langToUse'));
	
	$error = null;
	$success = null;
	
	
	if ( Request :: hasRequest('POST') )
	{
		// A traiter pour l'envoi d'emails
		$aData = Request :: getData();
		$bError = false;
		$sQuery = '';
		foreach ($aData as $iKey => $sValue)
		{
			if ( $iKey === 'lang' ) continue;
			if ( empty( $_POST[$iKey] ) ){ $bError = true;$error .= $globals['CON_ERROR'].'<br />';}
		}
		
		if ( !$bError )
		{
			foreach ($aData as $iKey => $sValue)
			{
				if ( $iKey === 'lang' ) continue;
				$sQuery .= "'" . $sValue . "',";
			}
			
			if($error === null)
			{
				$result = Validation :: email($aData['email']);
				if(!$result) $error .= $globals['CON_ERR_MAIL'].'<br />';
			}
			
			if($error === null){

				$html = '
							<div style="text-align : center;margin : 0 0 20px 0;"><img src="'.PATH_HTTP_IMG_GLOBAL.'logo_principal.png" alt="Logo DVAI" /></div>
								Bonjour, <br /><br />
							
							Une personne vous a contactée via le formulaire de recrutement... Répondez lui rapidement.<br />
							---------------------------------------------------------<br /><br />
							
							Coordonnées : <br /><br />
							
							Civilite : 				'.$aData['civilite'].				'<br />
							Nom : 					'.$aData['nom'].					'<br />
							Prenom : 				'.$aData['prenom'].					'<br />
							Adresse : 				'.$aData['adresse'].				'<br />
							Code Postal : 			'.$aData['code_postal'].			'<br />
							Ville : 				'.$aData['ville'].					'<br />
							Telephone : 			'.$aData['telephone'].				'<br />
							Email : 				'.$aData['email'].					'<br />
							--<br />
							Cordialement.<br />
							  L\'équipe DVAI.
							';
				
				$oEmail	= new Email();
				$oEmail	-> setTo(EMAIL_ADMIN.EMAIL_TEST)
								-> setFrom(EMAIL_FROM)
								-> setBody($html);
	
				Picture :: setExtensions(array('.doc', '.DOC', '.DOCX', '.docx', '.pdf', '.PDF'));
				$error = null;
				
				$sName = 'motivation-' . date('d-m-Y--H-i-s');
				Picture :: uploadFile($_FILES['motivation'], PATH_IMG_UPLOAD, $sName, $error, array());
	
				$sFile = $_FILES['motivation']['name'];
				$sExtension = pathinfo($sFile, PATHINFO_EXTENSION);
	
				if ( !$bError ) $oEmail->addAttachments(PATH_HTTP_IMG_UPLOAD . $sName . '.' . $sExtension);
				
				
				$sName = 'cv-' . date('d-m-Y--H-i-s');
				Picture :: uploadFile($_FILES['cv'], PATH_IMG_UPLOAD, $sName, $error, array());
	
				$sFile = $_FILES['cv']['name'];
				$sExtension = pathinfo($sFile, PATHINFO_EXTENSION);
	
				if ( !$bError ) $oEmail->addAttachments(PATH_HTTP_IMG_UPLOAD . $sName . '.' . $sExtension);
	
				$oEmail->addAttachments(PATH_HTTP_IMG_UPLOAD . $sName . '.' . strtolower($sExtension));
				
				
				if($error === null && !$bError)
		    	$bResult = $oEmail->setSubject('[DVAI] - Recrutement')->send();
			    
				if($bResult)
				{
					$success .= $globals['REC_OK'];
					Request :: clear();
					unset($field);
				}
			}
			
		}
	}

	//Retourne tableau ordonné
	$sQuery = 'SELECT * FROM '.BDD.'recrutement WHERE lang = "' . $sLang . '" ORDER BY position';
	$aOptions = array('type' => 'getOrderedTable', 'orderArrayBy' => 'id');
	$aPostes = fetchElementByLang($sQuery, $aOptions);
?>