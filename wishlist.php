<?php
require_once('.loader.php');

Database :: verifyConnexion();

$error = null;
$success = null;

//Page :: setIndexMenu(1);
Page :: setTitle('Wishlist | ' . TITLE_BY_DEFAULT);

Trigger :: call('onStartHTML');
?>
<section class="main-container">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1">
                <div class="write-comments">
                    <h3>Write comment</h3>
                    <form role="form">
                        <div class="col-sm-6">
                            <label for="name">Your Name*</label>
                            <input type="text" class="form-control" id="name" name="name">
                            <label for="email">Your Email*</label>
                            <input type="email" class="form-control" id="email" name="email">
                            <label for="url">URL</label>
                            <input type="text" class="form-control" id="url" name="url">
                        </div>
                        <div class="col-sm-6">
                            <textarea class="form-control" name="message"></textarea>
                            <button type="submit" class="btn btn-default">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
    